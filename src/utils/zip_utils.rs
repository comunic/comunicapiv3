//! # ZIP utilities
//!
//! @author Pierre Hubert

/// Check out whether a given file is a valid ZIP archive or not
pub fn is_valid_zip(file: &[u8]) -> bool {
    let cursor = std::io::Cursor::new(file);

    match zip::ZipArchive::new(cursor) {
        Ok(_) => true,
        Err(e) => {
            eprintln!("Failed to read ZIP archive! {:#?}", e);
            false
        }
    }
}
