use std::collections::HashSet;

use crate::controllers::user_ws_controller::WsSession;
use crate::data::conversation::ConvID;
use crate::data::post::PostID;
use crate::data::user::UserID;
use crate::data::user_token::UserAccessToken;

#[derive(Clone, Debug)]
pub struct ActiveCall {
    pub conv_id: ConvID,
    pub ready: bool,
}

/// This structure contains information about an active connection
#[derive(Clone, Debug)]
pub struct UserWsConnection {
    pub user_token: UserAccessToken,
    pub remote_ip: String,
    pub session: actix::Addr<WsSession>,
    pub incognito: bool,
    pub conversations: HashSet<ConvID>,
    pub posts: HashSet<PostID>,
    pub active_call: Option<ActiveCall>,
}

impl UserWsConnection {
    pub fn user_id(&self) -> &UserID {
        &self.user_token.user_id
    }

    /// Check out whether a connection is being used to make a call in a specific conversation or not
    pub fn is_having_call_with_conversation(&self, conv_id: &ConvID) -> bool {
        if let Some(call_info) = &self.active_call {
            return &call_info.conv_id == conv_id;
        }

        false
    }
}
