//! # New conversation message
//!
//! @author Pierre Hubert

use crate::data::conversation::ConvID;
use crate::data::conversation_message::{ConversationMessageFile, ConversationServerMessageType};
use crate::data::user::UserID;

/// Information about a new conversation message
pub struct NewConversationMessage {
    pub user_id: Option<UserID>,
    pub conv_id: ConvID,
    pub message: Option<String>,
    pub file: Option<ConversationMessageFile>,
    pub server_message: Option<ConversationServerMessageType>,
}

impl NewConversationMessage {
    /// Create a new server message
    pub fn new_server_message(conv_id: ConvID, message: ConversationServerMessageType) -> Self {
        Self {
            user_id: None,
            conv_id,
            message: None,
            file: None,
            server_message: Some(message),
        }
    }
}
