//! # Surveys controller
//!
//! @author Pierre Hubert

use crate::api_data::survey_api::SurveyAPI;
use crate::constants::MAXIMUM_NUMBER_SURVEY_CHOICES;
use crate::data::base_request_handler::BaseRequestHandler;
use crate::data::error::ResultBoxError;
use crate::data::http_request_handler::HttpRequestHandler;
use crate::data::post::PostAccessLevel;
use crate::helpers::survey_helper;
use crate::routes::RequestResult;

impl HttpRequestHandler {
    /// Get the ID of a survey associated to a post whose ID was specified in the request
    fn post_survey_id_from_post_id(
        &mut self,
        name: &str,
        min_level: PostAccessLevel,
    ) -> ResultBoxError<u64> {
        let post = self.post_post_with_access(name, min_level)?;
        survey_helper::get_id(post.id)
    }
}

/// Get information about a single survey
pub async fn get_info_single(r: &mut HttpRequestHandler) -> RequestResult {
    let post = r.post_post_with_access("postID", PostAccessLevel::BASIC_ACCESS)?;
    let survey = r.ok_or_not_found(
        survey_helper::get_info(post.id),
        "This post does not seems to have any survey...",
    )?;

    r.set_response(SurveyAPI::new(&survey, r.user_id_opt())?)
}

/// Respond to a survey
pub async fn send_response(r: &mut HttpRequestHandler) -> RequestResult {
    let survey_id = r.post_survey_id_from_post_id("postID", PostAccessLevel::BASIC_ACCESS)?;
    let choice_id = r.post_u64("choiceID")?;

    survey_helper::cancel_response(r.user_id_ref()?, survey_id)?;

    if !survey_helper::choice_exists(survey_id, choice_id)? {
        r.not_found("Choice not found for this survey!".to_string())?;
    }

    survey_helper::send_response(r.user_id_ref()?, survey_id, choice_id)?;

    r.success("Choice saved!")
}

/// Cancel a response to a survey
pub async fn cancel_response(r: &mut HttpRequestHandler) -> RequestResult {
    let survey_id = r.post_survey_id_from_post_id("postID", PostAccessLevel::BASIC_ACCESS)?;

    survey_helper::cancel_response(r.user_id_ref()?, survey_id)?;

    r.success("Response cancelled")
}

/// Create a new choice for a survey
pub async fn create_new_choice(r: &mut HttpRequestHandler) -> RequestResult {
    let post = r.post_post_with_access("postID", PostAccessLevel::BASIC_ACCESS)?;
    let new_choice = r.post_string("choice")?;

    let survey = survey_helper::get_info(post.id)?;
    if !survey.allow_new_choices {
        r.forbidden("It is not possible to create new choices for this survey!".to_string())?;
    }

    // Check for similar choices
    if survey
        .choices
        .iter()
        .find(|c| c.name.to_lowercase().eq(&new_choice.to_lowercase()))
        .is_some()
    {
        r.forbidden("This choice already exists!".to_string())?;
    }

    survey_helper::create_choice(survey.id, &new_choice)?;

    // Auto-block creation of new choices if limit is reached
    if survey.choices.len() + 1 >= MAXIMUM_NUMBER_SURVEY_CHOICES {
        survey_helper::block_new_choices_creation(survey.id)?;
    }

    r.success("Choice created")
}

/// Block the creation of new choices
pub async fn block_new_choices_creation(r: &mut HttpRequestHandler) -> RequestResult {
    let survey_id = r.post_survey_id_from_post_id("postID", PostAccessLevel::FULL_ACCESS)?;

    survey_helper::block_new_choices_creation(survey_id)?;

    r.success("")
}
