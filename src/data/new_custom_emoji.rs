//! # New custom emoji
//!
//! @author Pierre Hubert

use crate::data::user::UserID;

pub struct NewCustomEmoji {
    pub user_id: UserID,
    pub shortcut: String,
    pub path: String,
}
