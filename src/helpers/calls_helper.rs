//! # Calls helper
//!
//! @author Pierre Hubert

use crate::controllers::rtc_relay_controller;
use crate::data::config::conf;
use crate::data::conversation::Conversation;

/// Check out whether a conversation can make a call or not
pub fn can_have_call(conv: &Conversation) -> bool {
    if !conf().is_rtc_relay_enabled() {
        return false;
    }

    if let Some(conf) = &conf().rtc_relay {
        return conv.members.len() > 1
            && conf.max_users_per_calls >= conv.members.len() as u64
            && rtc_relay_controller::is_connected();
    }

    false
}

/// Check out whether a conversation is allowed to make video calls or not
pub fn can_have_video_calls(conv: &Conversation) -> bool {
    if let Some(conf) = &conf().rtc_relay {
        return can_have_call(conv)
            && conf.allow_video
            && conf.max_users_per_video_calls >= conv.members.len() as u64;
    }

    false
}
