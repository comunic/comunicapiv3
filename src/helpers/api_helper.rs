use crate::constants::database_tables_names::CLIENTS_TABLE;
use crate::data::api_client::APIClient;
use crate::data::error::{Res, ResultBoxError};
use crate::helpers::database;
use crate::helpers::database::{InsertQuery, QueryInfo};

/// API helper
///
/// @author Pierre Hubert

/// Get the entire list of clients
pub fn get_clients() -> Res<Vec<APIClient>> {
    QueryInfo::new(CLIENTS_TABLE).exec(db_to_client)
}

/// Register a new client
pub fn register_client(client: &APIClient) -> Res<u64> {
    let mut query = InsertQuery::new(CLIENTS_TABLE)
        .add_str("name", &client.name)
        .add_opt_str("domain", client.domain.as_ref())
        .add_opt_str("comment", client.comment.as_ref())
        .add_opt_str(
            "firebase_project_name",
            client.firebase_project_name.as_ref(),
        )
        .add_opt_str(
            "firebase_service_account_file",
            client.firebase_service_account_file.as_ref(),
        );

    if client.default_expiration_time > 0 {
        query = query.add_u64("default_expiration_time", client.default_expiration_time);
    }

    query.insert_expect_result()
}

/// Get information about a client, based on its token
pub fn get_client(name: &str) -> ResultBoxError<APIClient> {
    QueryInfo::new(CLIENTS_TABLE)
        .cond("name", name)
        .query_row(db_to_client)
}

/// Get information about a client, based on its origin
pub fn get_by_origin(name: &str) -> ResultBoxError<APIClient> {
    QueryInfo::new(CLIENTS_TABLE)
        .cond("domain", name)
        .query_row(db_to_client)
}

/// Get information about a client, based on its ID
pub fn get_by_id(id: u64) -> Res<APIClient> {
    QueryInfo::new(CLIENTS_TABLE)
        .cond_u64("id", id)
        .query_row(db_to_client)
}

fn db_to_client(res: &database::RowResult) -> Res<APIClient> {
    Ok(APIClient {
        id: res.get_u64("id")?,
        name: res.get_str("name")?,
        domain: res.get_optional_str("domain")?,
        comment: res.get_optional_str("comment")?,
        default_expiration_time: res.get_u64("default_expiration_time")?,
        firebase_project_name: res.get_optional_str("firebase_project_name")?,
        firebase_service_account_file: res.get_optional_str("firebase_service_account_file")?,
    })
}
