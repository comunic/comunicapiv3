//! # Get security questions results
//!
//! @author Pierre Hubert
use serde::Serialize;

use crate::data::user::User;

#[derive(Serialize)]
pub struct ResGetSecurityQuestions {
    questions: Vec<String>,
}

impl ResGetSecurityQuestions {
    pub fn new(user: &User) -> ResGetSecurityQuestions {
        let questions = vec![
            user.security_question_1.clone().unwrap_or_default(),
            user.security_question_2.clone().unwrap_or_default(),
        ];

        ResGetSecurityQuestions { questions }
    }
}
