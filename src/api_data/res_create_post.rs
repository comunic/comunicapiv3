//! # Create a post result
//!
//! @author Pierre Hubert
use serde::Serialize;

#[derive(Serialize)]
#[allow(non_snake_case)]
pub struct ResCreatePost {
    success: String,
    postID: u64,
}

impl ResCreatePost {
    /// Initialize a new structure
    pub fn new(post_id: u64) -> ResCreatePost {
        ResCreatePost {
            success: "The post has been created!".to_string(),
            postID: post_id,
        }
    }
}
