//! # General settings API
//!
//! @author Pierre Hubert

use serde::Serialize;

use crate::data::user::User;

#[derive(Serialize)]
#[allow(non_snake_case)]
pub struct GeneralSettingsAPI {
    id: u64,
    email: String,
    firstName: String,
    lastName: String,
    is_public: bool,
    is_open: bool,
    allow_comments: bool,
    allow_posts_from_friends: bool,
    allow_comunic_mails: bool,
    public_friends_list: bool,
    public_email: bool,
    virtual_directory: String,
    personnal_website: String,
    publicNote: String,
    location: Option<String>,
}

impl GeneralSettingsAPI {
    pub fn new(user: &User) -> GeneralSettingsAPI {
        GeneralSettingsAPI {
            id: user.id.id(),
            email: user.email.clone(),
            firstName: user.first_name.clone(),
            lastName: user.last_name.clone(),
            is_public: user.is_page_public(),
            is_open: user.is_page_open(),
            allow_comments: !user.block_comments_on_his_page,
            allow_posts_from_friends: user.allow_posts_from_friends,
            allow_comunic_mails: user.allow_mails,
            public_friends_list: user.public_friends_list,
            public_email: user.is_email_public,
            virtual_directory: user.virtual_directory.clone().unwrap_or_default(),
            personnal_website: user.personal_website.clone().unwrap_or_default(),
            publicNote: user.public_note.clone().unwrap_or_default(),
            location: user.location.clone(),
        }
    }
}
