//! # Comments controller
//!
//! @author Pierre Hubert

use crate::api_data::comment_api::CommentAPI;
use crate::api_data::res_create_comment::ResCreateComment;
use crate::constants::PATH_COMMENTS_IMAGES;
use crate::controllers::user_ws_controller;
use crate::data::base_request_handler::BaseRequestHandler;
use crate::data::comment::Comment;
use crate::data::error::Res;
use crate::data::http_request_handler::HttpRequestHandler;
use crate::data::notification::NotifEventType;
use crate::data::post::PostAccessLevel;
use crate::data::user_ws_message::UserWsMessage;
use crate::helpers::events_helper::Event;
use crate::helpers::{comments_helper, events_helper, notifications_helper, posts_helper};
use crate::routes::RequestResult;
use crate::utils::date_utils::time;
use crate::utils::string_utils::remove_html_nodes;

/// Create a new comment
pub async fn create(r: &mut HttpRequestHandler) -> RequestResult {
    let post = r.post_post_with_access("postID", PostAccessLevel::BASIC_ACCESS)?;

    if !posts_helper::allow_comments_on_post(&post)? {
        r.forbidden("You can not create comments on this post!".to_string())?;
    }

    let (content, image) = if r.has_file("image") {
        (
            remove_html_nodes(&r.post_string_opt("content", 0, false)?),
            Some(r.save_post_image("image", PATH_COMMENTS_IMAGES, 700, 700)?),
        )
    } else {
        (r.post_content("content", 2, true)?, None)
    };

    let comment = Comment {
        id: 0,
        time_sent: time(),
        user_id: r.user_id()?,
        post_id: post.id,
        content,
        image_path: image,
    };

    let comment_id = comments_helper::create(&comment).await?;

    // Create notifications
    notifications_helper::create_post_notification(
        &r.user_id()?,
        post.id,
        NotifEventType::COMMENT_CREATED,
    )
    .await?;

    // Remove notifications targeting current user about the post
    notifications_helper::delete_all_post_notifications_targeting_user(r.user_id_ref()?, post.id)
        .await?;

    r.set_response(ResCreateComment::new(comment_id))
}

/// Get information about a single comment
pub async fn get_single(r: &mut HttpRequestHandler) -> RequestResult {
    let comment = r.post_comment_with_access("commentID")?;

    r.set_response(CommentAPI::new(&comment, &r.user_id_opt())?)
}

/// Change a comment's content
pub async fn edit(r: &mut HttpRequestHandler) -> RequestResult {
    let comment = r.post_comment_with_full_access("commentID")?;
    let new_content = r.post_content("content", 2, true)?;

    comments_helper::edit(comment.id, &new_content).await?;

    r.success("Content updated.")
}

/// Delete a comment
pub async fn delete(r: &mut HttpRequestHandler) -> RequestResult {
    let comment = r.post_comment_with_full_access("commentID")?;

    comments_helper::delete(&comment).await?;

    r.success("Comment deleted.")
}

/// Events handler
pub async fn handle_event(e: &events_helper::Event) -> Res {
    match e {
        Event::NewComment(comment) => {
            user_ws_controller::send_message_to_specific_connections(
                |c| c.posts.contains(&comment.post_id),
                |c| {
                    UserWsMessage::no_id_message(
                        "new_comment",
                        CommentAPI::new(comment, &Some(c.user_id().clone()))?,
                    )
                },
            )?;
        }

        Event::UpdatedComment(comment) => {
            user_ws_controller::send_message_to_specific_connections(
                |c| c.posts.contains(&comment.post_id),
                |c| {
                    UserWsMessage::no_id_message(
                        "comment_updated",
                        CommentAPI::new(comment, &Some(c.user_id().clone()))?,
                    )
                },
            )?;
        }

        Event::DeletedComment(comment) => {
            user_ws_controller::send_message_to_specific_connections(
                |c| c.posts.contains(&comment.post_id),
                |_| UserWsMessage::no_id_message("comment_deleted", comment.id.clone()),
            )?;
        }

        _ => {}
    }

    Ok(())
}
