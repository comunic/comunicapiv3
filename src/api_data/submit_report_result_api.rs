use crate::data::report::ReportID;

#[derive(serde::Serialize)]
pub struct SubmitReportResultApi {
    report_id: u64,
}

impl SubmitReportResultApi {
    pub fn new(r: ReportID) -> Self {
        Self { report_id: r.0 }
    }
}
