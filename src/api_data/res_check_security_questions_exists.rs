//! # Check if security questions are set for a user
//!
//! @author Pierre Hubert
use serde::Serialize;

#[derive(Serialize)]
pub struct ResCheckSecurityQuestionsExists {
    defined: bool,
}

impl ResCheckSecurityQuestionsExists {
    pub fn new(defined: bool) -> ResCheckSecurityQuestionsExists {
        ResCheckSecurityQuestionsExists { defined }
    }
}
