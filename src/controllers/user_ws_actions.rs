//! # User WebSocket actions
//!
//! This module contains all the base action that can be executed by the WebSocket

use crate::data::base_request_handler::BaseRequestHandler;
use crate::data::conversation::ConvID;
use crate::data::error::Res;
use crate::data::post::PostAccessLevel;
use crate::data::user_ws_request_handler::UserWsRequestHandler;

/// Update incognito status of the connection
pub fn set_incognito(r: &mut UserWsRequestHandler) -> Res {
    let new_state = r.post_bool("enable")?;
    r.update_conn(|c| c.incognito = new_state)?;

    r.success("Updated.")
}

/// Register a conversation
pub fn register_conv(r: &mut UserWsRequestHandler) -> Res {
    let conv_id = r.post_conv("convID")?.conv_id;
    r.update_conn(|c| {
        c.conversations.insert(conv_id);
    })?;
    r.success("ok")
}

/// Un-register a conversation
pub fn unregister_conv(r: &mut UserWsRequestHandler) -> Res {
    let conv_id = ConvID::new(r.post_u64("convID")?);
    r.update_conn(|c| {
        c.conversations.remove(&conv_id);
    })?;
    r.success("ok")
}

/// Register a post
pub fn register_post(r: &mut UserWsRequestHandler) -> Res {
    let post = r.post_post_with_access("postID", PostAccessLevel::BASIC_ACCESS)?;
    r.update_conn(|c| {
        c.posts.insert(post.id);
    })?;
    r.success("ok")
}

/// Un-register a post
pub fn unregister_post(r: &mut UserWsRequestHandler) -> Res {
    let post_id = r.post_u64("postID")?;
    r.update_conn(|c| {
        c.posts.remove(&post_id);
    })?;
    r.success("ok")
}
