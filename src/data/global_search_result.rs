//! # Global search result

use crate::data::group_id::GroupID;
use crate::data::user::UserID;

pub enum GlobalSearchResult {
    User(UserID),
    Group(GroupID),
}
