//! # Web Application Helper
//!
//! Contains methods specific to Comunic Web applications
//!
//! @author Pierre Hubert

use crate::data::error::ResultBoxError;
use crate::data::user::UserID;
use crate::data::user_membership::UserMembership;
use crate::helpers::{conversations_helper, friends_helper, groups_helper};

/// Get all the memberships of a user
pub fn get_user_memberships(user_id: &UserID) -> ResultBoxError<Vec<UserMembership>> {
    let friends = friends_helper::GetFriendsQuery::new(user_id).exec()?;
    let groups = groups_helper::get_list_user(user_id, false)?;
    let conversations_list = conversations_helper::get_list_user(user_id)?;

    let mut list = Vec::new();

    for friend in friends {
        list.push(UserMembership::Friend(friend));
    }

    for group in &groups {
        list.push(UserMembership::Group(
            group.clone(),
            groups_helper::get_last_activity(user_id, group)?,
        ));
    }

    for conv in conversations_list {
        list.push(UserMembership::Conversation(conv))
    }

    list.sort_by(|a, b| b.cmp(a));

    Ok(list)
}
