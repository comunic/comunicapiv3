//! # User membership API entry
//!
//! @author Pierre Hubert

use serde::Serialize;

use crate::api_data::conversation_api::ConversationAPI;
use crate::api_data::friend_api::FriendAPI;
use crate::api_data::type_container_api::TypeContainerAPI;
use crate::data::user_membership::UserMembership;

#[derive(Serialize)]
pub struct UserMembershipAPI {
    #[serde(flatten)]
    membership_type: TypeContainerAPI,
    id: Option<u64>,
    friend: Option<FriendAPI>,
    last_activity: Option<u64>,
    conv: Option<ConversationAPI>,
}

impl UserMembershipAPI {
    pub fn new(m: &UserMembership) -> UserMembershipAPI {
        match &m {
            UserMembership::Group(group_id, last_activity) => UserMembershipAPI {
                membership_type: TypeContainerAPI::new("group".to_string()),
                id: Some(group_id.id()),
                friend: None,
                last_activity: Some(*last_activity),
                conv: None,
            },

            UserMembership::Friend(friend) => UserMembershipAPI {
                membership_type: TypeContainerAPI::new("friend".to_string()),
                id: None,
                friend: Some(FriendAPI::new(friend)),
                last_activity: None,
                conv: None,
            },

            UserMembership::Conversation(conversation) => UserMembershipAPI {
                membership_type: TypeContainerAPI::new("conversation".to_string()),
                id: None,
                friend: None,
                last_activity: None,
                conv: Some(ConversationAPI::new(conversation)),
            },
        }
    }

    pub fn for_list(l: &[UserMembership]) -> Vec<UserMembershipAPI> {
        l.iter().map(Self::new).collect()
    }
}
