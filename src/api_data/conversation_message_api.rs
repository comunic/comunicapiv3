//! # Conversation message API
//!
//! API representation of a conversation message
//!
//! @author Pierre HUBERT

use serde::Serialize;
use serde_json::Value;

use crate::data::conversation_message::{ConversationMessage, ConversationServerMessageType};
use crate::utils::user_data_utils::user_data_url;

#[derive(Serialize)]
struct ConversationMessageFileAPI {
    url: String,
    size: u64,
    name: String,
    thumbnail: Option<String>,
    r#type: String,
}

#[derive(Serialize)]
pub struct ConversationMessageAPI {
    id: u64,
    conv_id: u64,
    user_id: Option<u64>,
    time_sent: u64,
    message: Option<String>,
    file: Option<ConversationMessageFileAPI>,
    server_message: Option<serde_json::Value>,
}

impl ConversationMessageAPI {
    /// Turn a conversation message into an API entry
    pub fn new(msg: &ConversationMessage) -> ConversationMessageAPI {
        let file = match &msg.file {
            None => None,
            Some(file) => Some(ConversationMessageFileAPI {
                url: user_data_url(&file.path.clone()),
                size: file.size,
                name: file.name.clone(),
                thumbnail: file.thumbnail.as_ref().map(|u| user_data_url(u)),
                r#type: file.r#type.clone(),
            }),
        };

        let server_message = match &msg.server_message {
            None => None,
            Some(msg) => {
                let mut value = serde_json::Map::new();

                match msg {
                    ConversationServerMessageType::UserCreatedConversation(user) => {
                        value.insert("type".to_string(), Value::from("user_created_conv"));
                        value.insert("user_id".to_string(), Value::from(user.id()));
                    }

                    ConversationServerMessageType::UserAddedAnotherUserToConversation(msg) => {
                        value.insert("type".to_string(), Value::from("user_added_another"));
                        value.insert(
                            "user_who_added".to_string(),
                            Value::from(msg.user_who_added.id()),
                        );
                        value.insert("user_added".to_string(), Value::from(msg.user_added.id()));
                    }

                    ConversationServerMessageType::UserLeftConversation(u) => {
                        value.insert("type".to_string(), Value::from("user_left"));
                        value.insert("user_id".to_string(), Value::from(u.id()));
                    }

                    ConversationServerMessageType::UserRemovedFromConversation(msg) => {
                        value.insert("type".to_string(), Value::from("user_removed_another"));
                        value.insert(
                            "user_who_removed".to_string(),
                            Value::from(msg.user_who_removed.id()),
                        );
                        value.insert(
                            "user_removed".to_string(),
                            Value::from(msg.user_removed.id()),
                        );
                    }
                }

                Some(serde_json::Value::Object(value))
            }
        };

        ConversationMessageAPI {
            id: msg.id,
            conv_id: msg.conv_id.id(),
            user_id: msg.user_id.clone().map(|u| u.id()),
            time_sent: msg.time_sent,
            message: msg.message.clone(),
            file,
            server_message,
        }
    }

    /// Turn a list of conversation messages into API entries
    pub fn for_list(l: &Vec<ConversationMessage>) -> Vec<ConversationMessageAPI> {
        l.iter().map(|m| ConversationMessageAPI::new(m)).collect()
    }
}
