//! # Posts targets
//!
//! @author Pierre Hubert
use serde::Serialize;

use crate::data::group_id::GroupID;
use crate::data::user::UserID;

#[derive(Serialize)]
pub struct PostsTargets {
    friends: Vec<u64>,
    groups: Vec<u64>,
}

impl PostsTargets {
    pub fn new(f: &[UserID], g: &[GroupID]) -> PostsTargets {
        PostsTargets {
            friends: f.iter().map(|f| f.id()).collect(),
            groups: g.iter().map(|f| f.id()).collect(),
        }
    }
}
