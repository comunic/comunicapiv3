//! # Create conversation result
//!
//! @author Pierre Hubert

use crate::data::conversation::ConvID;
use serde::Serialize;

#[derive(Serialize)]
#[allow(non_snake_case)]
pub struct ResCreateConversation {
    conversationID: u64,
    success: String,
}

impl ResCreateConversation {
    /// Construct a new Result instance
    pub fn new(conv_id: ConvID) -> ResCreateConversation {
        ResCreateConversation {
            conversationID: conv_id.id(),
            success: "The conversation was successfully created!".to_string(),
        }
    }
}
