//! Project constants
//!
//! This module contains all the hard-coded value that might be used everywhere in the project.
//!
//! @author Pierre Hubert

use std::time::Duration;

/// The name of the tables
pub mod database_tables_names {
    /// API services tokens table
    pub const CLIENTS_TABLE: &str = "comunic_clients";

    /// User access tokens table
    pub const USER_ACCESS_TOKENS_TABLE: &str = "comunic_user_access_tokens";

    /// User table
    pub const USERS_TABLE: &str = "utilisateurs";

    /// Friends table
    pub const FRIENDS_TABLE: &str = "amis";

    /// Custom emojis table
    pub const EMOJIS_TABLE: &str = "comunic_custom_emojis";

    /// Likes table
    pub const LIKES_TABLE: &str = "aime";

    /// Groups list table
    pub const GROUPS_LIST_TABLE: &str = "comunic_groups";

    /// Groups members table
    pub const GROUPS_MEMBERS_TABLE: &str = "comunic_groups_members";

    /// Conversations tables
    pub const CONV_LIST_TABLE: &str = "comunic_conversations_list";
    pub const CONV_MEMBERS_TABLE: &str = "comunic_conversations_members";
    pub const CONV_MESSAGES_TABLE: &str = "comunic_conversations_messages";

    /// Posts table
    pub const POSTS_TABLE: &str = "texte";

    /// Movies table
    pub const MOVIES_TABLE: &str = "galerie_video";

    /// Survey info table
    pub const SURVEY_INFO_TABLE: &str = "sondage";

    /// Survey choices table
    pub const SURVEY_CHOICES_TABLE: &str = "sondage_choix";

    /// Survey responses table
    pub const SURVEY_RESPONSE_TABLE: &str = "sondage_reponse";

    /// Comments table
    pub const COMMENTS_TABLE: &str = "commentaires";

    /// Notifications table
    pub const NOTIFICATIONS_TABLE: &str = "comunic_notifications";

    /// Reports table
    pub const REPORTS_TABLE: &str = "comunic_reports";

    /// Forez presence table
    pub const FOREZ_PRESENCE_TABLE: &str = "forez_presence";

    /// Administrators tables
    pub const ADMIN_LIST_TABLE: &str = "comunic_admin";
    pub const ADMIN_KEYS_TABLE: &str = "comunic_admin_key";
    pub const ADMIN_ROLES_TABLE: &str = "comunic_admin_roles";
    pub const ADMIN_LOGS_TABLE: &str = "comunic_admin_log";
}

/// Push Notifications Database prefix
pub mod push_notifications_db_prefix {
    pub const NONE_PREFIX: &str = "NONE";
    pub const INDEPENDENT_PREFIX: &str = "I:";
    pub const FIREBASE_PREFIX: &str = "F:";
}

/// Password policy
pub mod password_policy {
    /// Allow email in password ?
    pub const ALLOW_EMAIL_IN_PASSWORD: bool = false;

    /// Allow user name in password ?
    pub const ALLOW_NAME_IN_PASSWORD: bool = false;

    /// Minimal length of password
    pub const MIN_PASSWORD_LENGTH: u64 = 8;

    /// Password composition policy
    pub const MIN_NUMBER_LOWERCASE_LETTER: u64 = 1;
    pub const MIN_NUMBER_UPPERCASE_LETTER: u64 = 1;
    pub const MIN_NUMBER_DIGITS: u64 = 1;
    pub const MIN_NUMBER_SPECIAL_CHARACTERS: u64 = 1;
    pub const MIN_CATEGORIES_PRESENCE: u64 = 2;
}

/// Minimal conversation policy
pub mod conservation_policy {
    use std::time::Duration;

    /// Minimum lifetime for inactive account (3 months)
    pub const MIN_INACTIVE_ACCOUNT_LIFETIME: Duration = Duration::from_secs(60 * 60 * 24 * 30 * 3);

    /// Minimum lifetime for notifications (7 days)
    pub const MIN_NOTIFICATIONS_LIFETIME: Duration = Duration::from_secs(60 * 60 * 24 * 7);

    /// Minimum lifetime for comments (1 month)
    pub const MIN_COMMENTS_LIFETIME: Duration = Duration::from_secs(60 * 60 * 24 * 30);

    /// Minimum lifetime for posts (1 month)
    pub const MIN_POSTS_LIFETIME: Duration = Duration::from_secs(60 * 60 * 24 * 30);

    /// Minimum conversation messages lifetime (15 days)
    pub const MIN_CONVERSATION_MESSAGES_LIFETIME: Duration = Duration::from_secs(60 * 60 * 24 * 15);

    /// Minimum likes lifetime (1 month)
    pub const MIN_LIKES_LIFETIME: Duration = Duration::from_secs(60 * 60 * 24 * 30);

    /// Admin actions log duration (3 months)
    pub const ADMIN_ACTIONS_LOG_LIFETIME: Duration = Duration::from_secs(60 * 60 * 24 * 30 * 3);
}

/// WebSocket access token lifetime, in seconds
pub const WS_ACCESS_TOKEN_LIFETIME: u64 = 10;

/// WebSocket access token length
pub const WS_ACCESS_TOKEN_LENGTH: usize = 10;

/// Lifetime of limit counter (1 hour)
pub const LIMIT_COUNTER_LIFETIME: u64 = 60 * 60;

/// The account image to show for user who do not have any
pub const DEFAULT_ACCOUNT_IMAGE: &str = "avatars/0Reverse.png";

/// The account image to show for users who are not allowed to access other users account images
pub const ERROR_ACCOUNT_IMAGE: &str = "avatars/0Red.png";

/// The path where groups logos should be stored
pub const PATH_GROUPS_LOGOS: &str = "groups_logo";

/// The group logo to use when no custom logo have been uploaded
pub const DEFAULT_GROUP_LOGO: &str = "groups_logo/default.png";

/// The path where image associated with posts should be stored
pub const PATH_POST_IMAGES: &str = "imgpost";

/// The path were PDF included with posts should be stored
pub const PATH_POST_PDF: &str = "post_pdf";

/// The page where comments images are stored
pub const PATH_COMMENTS_IMAGES: &str = "imgcommentaire";

/// Maximum requests size (50 Mo)
pub const MAX_REQUEST_SIZE: usize = 50000000;

/// Maximum number of choices per survey
pub const MAXIMUM_NUMBER_SURVEY_CHOICES: usize = 20;

/// Length of password reset token
pub const PASSWORD_RESET_TOKEN_LENGTH: usize = 255;

/// Duration of the validity of a password reset token (6 hours)
pub const PASSWORD_RESET_TOKEN_LIFETIME: u64 = 60 * 60 * 6;

/// Length of admin reset tokens
pub const ADMIN_RESET_TOKEN_LENGTH: usize = 255;

/// Duration of the validity of an admin password reset token (1 hour)
pub const ADMIN_RESET_TOKEN_LIFETIME: u64 = 60 * 60;

/// Length of an admin access token
pub const ADMIN_ACCESS_TOKEN_LENGTH: usize = 300;

/// Duration of the validation of an admin access token without refresh
pub const ADMIN_ACCESS_TOKEN_LIFETIME: u64 = 60 * 10;

/// Minimum password length
pub const PASSWORD_MIN_LENGTH: usize = 3;

/// Supported languages (for ComunicWeb)
pub const SUPPORTED_LANGUAGES: &'static [&'static str] = &["en", "fr"];

/// Interval at which last active time of user should be recorded
pub const USER_LAST_ACTIVITY_REFRESH: Duration = Duration::from_secs(60);

/// Interval at which last activity of an access token should be recorded
pub const USER_ACCESS_TOKEN_ACTIVITY_REFRESH: Duration = Duration::from_secs(60 * 60);

/// Sleep time before initial clean up execution
pub const INITIAL_REFRESH_LOAD_INTERVAL: Duration = Duration::from_secs(5);

/// Interval between each cleanup operation (every hour)
pub const CLEAN_UP_INTERVAL: Duration = Duration::from_secs(60 * 60);

/// Minimal mobile version supported
pub const MIN_SUPPORTED_MOBILE_VERSION: &str = "1.1.2";

/// Conversations constants
pub mod conversations {
    /// Conversation name
    pub const MAX_CONVERSATION_NAME_LENGTH: usize = 20;

    /// Minimum message length
    pub const MIN_CONVERSATION_MESSAGE_LENGTH: usize = 1;
    pub const MAX_CONVERSATION_MESSAGE_LENGTH: usize = 16000;

    /// Allowed files type in conversations
    pub const ALLOWED_CONVERSATION_FILES_TYPES: [&str; 18] = [
        "image/png",
        "image/jpeg",
        "image/gif",
        "image/bmp",
        "application/pdf",
        "audio/mpeg",
        "video/mp4",
        "video/quicktime",
        "application/zip",
        // MS Office docs
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        "application/vnd.openxmlformats-officedocument.presentationml.presentation",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        // Open Office docs
        "application/vnd.oasis.opendocument.text",
        "application/vnd.oasis.opendocument.presentation",
        "application/vnd.oasis.opendocument.spreadsheet",
        // Source code docs (UTF-8 encoded)
        "text/x-csrc",
        "text/plain",
        "text/x-c++src",
    ];

    /// File maximum size in conversations (10 Mb)
    pub const CONVERSATION_FILES_MAX_SIZE: usize = 10 * 1024 * 1024;

    /// Minimum interval before notifying again that someone is writing
    pub const CONVERSATION_WRITING_EVENT_INTERVAL: u64 = 1;

    /// Lifetime of conversation writing event
    pub const CONVERSATION_WRITING_EVENT_LIFETIME: u64 = 3;

    /// Image size
    pub const MAX_CONV_IMAGE_MESSAGE_WIDTH: u32 = 2000;
    pub const MAX_CONV_IMAGE_MESSAGE_HEIGHT: u32 = 2000;

    /// Thumbnail size
    pub const MAX_CONV_MESSAGE_THUMBNAIL_WIDTH: u32 = 300;
    pub const MAX_CONV_MESSAGE_THUMBNAIL_HEIGHT: u32 = 300;

    /// Conversation logo size
    pub const MAX_CONV_LOGO_WIDTH: u32 = 200;
    pub const MAX_CONV_LOGO_HEIGHT: u32 = 200;
}

/// Account information policy
pub mod accounts_info_policy {
    /// Minimum & maximum first name length
    pub const MIN_FIRST_NAME_LENGTH: u8 = 2;
    pub const MAX_FIRST_NAME_LENGTH: u8 = 50;

    /// Minimum & maximum last name length
    pub const MIN_LAST_NAME_LENGTH: u8 = 2;
    pub const MAX_LAST_NAME_LENGTH: u8 = 50;

    /// Max location value size
    pub const MAX_LOCATION_LENGTH: usize = 45;
}

/// Url where Firebase push notifications can be sent
pub const FIREBASE_PUSH_MESSAGE_URL: &str =
    "https://fcm.googleapis.com/v1/projects/{PROJECT_ID}/messages:send";

/// Reports constants
pub mod reports {
    /// Maximum report comment length
    pub const MAX_COMMENT_LENGTH: u32 = 500;

    /// Specify whether a user can reports its own content or not
    pub const CAN_USER_REPORT_ITS_OWN_CONTENT: bool = true;
}

/// Admin-specific constants
pub mod admin {
    #[derive(Copy, Clone, Eq, PartialEq)]
    #[allow(non_camel_case_types)]
    pub enum AdminRole {
        MANAGE_ADMINS,
        MANAGE_USERS,
        ACCESS_ALL_ADMINS_LOGS,
    }

    pub struct AdminRoleMetadata {
        pub role: AdminRole,
        pub id: &'static str,
        pub name: &'static str,
        pub description: &'static str,
    }

    pub const ADMIN_ROLES_LIST: [AdminRoleMetadata; 3] = [
        AdminRoleMetadata {
            role: AdminRole::MANAGE_ADMINS,
            id: "manage_admins",
            name: "Manage administrators",
            description: "Allow the admin to create, list and update all administrators",
        },
        AdminRoleMetadata {
            role: AdminRole::MANAGE_USERS,
            id: "manage_users",
            name: "Manage Comunic users",
            description: "Allow the admin to list, reset password and delete Comunic users",
        },
        AdminRoleMetadata {
            role: AdminRole::ACCESS_ALL_ADMINS_LOGS,
            id: "access_all_admins_logs",
            name: "Access all admins logs",
            description: "Allow the admin to access the action history (log) of all admins",
        },
    ];
}
