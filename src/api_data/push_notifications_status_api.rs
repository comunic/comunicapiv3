//! # Push notification status API

use serde::Serialize;

use crate::data::config::conf;
use crate::data::user_token::PushNotificationToken;

#[derive(Serialize)]
pub struct PushNotificationsStatusAPI {
    pub status: String,
    pub independent_push_url: Option<String>,
}

impl PushNotificationsStatusAPI {
    pub fn new(t: &PushNotificationToken) -> Self {
        Self {
            status: match t {
                PushNotificationToken::UNDEFINED => "undefined",
                PushNotificationToken::NONE => "disabled",
                PushNotificationToken::INDEPENDENT(_) => "independent",
                PushNotificationToken::FIREBASE(_) => "firebase",
            }
            .to_string(),

            independent_push_url: match (t, &conf().independent_push_service) {
                (PushNotificationToken::INDEPENDENT(i), Some(conf)) => {
                    Some(conf.public_url.replace("{TOKEN_URL}", i))
                }
                _ => None,
            },
        }
    }
}
