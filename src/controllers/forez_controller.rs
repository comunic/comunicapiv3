//! # Forez controller
//!
//! This controller contains the logic specific to the integration of the #Forez application into
//! Comunic.
//!
//! @author Pierre Hubert

use crate::api_data::group_api::GroupApi;
use crate::api_data::user_info::APIUserInfo;
use crate::data::base_request_handler::BaseRequestHandler;
use crate::data::config::conf;
use crate::data::http_request_handler::HttpRequestHandler;
use crate::data::presence::Presence;
use crate::data::user_ws_request_handler::UserWsRequestHandler;
use crate::helpers::{forez_presence_helper, groups_helper, user_helper};
use crate::routes::RequestResult;

/// Get the list of declared Forez groups in the application
pub async fn get_list_groups(r: &mut HttpRequestHandler) -> RequestResult {
    let mut list = vec![];

    for group in &conf().forez_groups {
        list.push(GroupApi::new(
            &groups_helper::get_info(group)?,
            r.user_id_opt(),
        )?);
    }

    r.set_response(list)
}

/// Get advanced information about all the members of a group
pub async fn get_member_info(r: &mut HttpRequestHandler) -> RequestResult {
    let group = r.post_forez_group("group")?;
    let member = r.post_user_id("user")?;

    let membership = groups_helper::get_membership(&group, Some(member.clone()))?;

    if !membership.is_at_least_member() {
        r.bad_request("This user is not a member of the group yet!".to_string())?;
    }

    let user = user_helper::find_user_by_id(&member)?;
    r.set_response(APIUserInfo::new_advanced_info(&Some(member), &user)?)
}

/// Set presence
///
/// Presences format: YYYY,MM,DD;YYYY,MM,DD;...
pub fn set_presence(r: &mut UserWsRequestHandler) -> RequestResult {
    let group = r.post_forez_group("group")?;
    let presences = r.post_string_opt("presence", 0, false)?;

    let mut list = vec![];
    for p in presences.split(";") {
        if p == "" {
            continue;
        }

        let info: Vec<&str> = p.split(",").collect();

        if info.len() != 3 {
            r.bad_request("Invalid presence information!".to_string())?;
        }

        let year = info[0].parse::<u32>()?;
        let month = info[1].parse::<u16>()?;
        let day = info[2].parse::<u16>()?;

        if year < 2020 || year > 2100 {
            r.bad_request("Invalid year specified!".to_string())?;
        }

        if month < 1 || month > 12 {
            r.bad_request("Invalid month specified!".to_string())?;
        }

        if day < 1 || day > 31 {
            r.bad_request("Invalid day specified!".to_string())?;
        }

        let presence = Presence {
            id: 0,
            user_id: r.user_id()?,
            year,
            month,
            day,
        };

        if !list.contains(&presence) {
            list.push(presence);
        }
    }

    forez_presence_helper::update(&group, &r.user_id()?, list)?;

    r.success("Presences updated.")
}

/// Get the list of presences of all the members of a group
///
/// Format: USER_ID,YYYY,MM,DD
pub fn get_list<H: BaseRequestHandler>(r: &mut H) -> RequestResult {
    let group = r.post_forez_group("group")?;
    let list = forez_presence_helper::get_list(&group)?;

    let list = list
        .iter()
        .map(|p| format!("{},{},{},{}", p.user_id.id(), p.year, p.month, p.day))
        .collect::<Vec<String>>();

    r.set_response(list)
}

/// Add a new day of presence
pub fn add_day<H: BaseRequestHandler>(r: &mut H) -> RequestResult {
    let group = r.post_forez_group("group")?;

    let mut list = forez_presence_helper::get_user_presences(&group, &r.user_id()?)?;

    let presence = Presence {
        id: 0,
        user_id: r.user_id()?,
        year: r.post_u32("year")?,
        month: r.post_u16("month")?,
        day: r.post_u16("day")?,
    };

    if !list.contains(&presence) {
        list.push(presence);
    }

    forez_presence_helper::update(&group, &r.user_id()?, list)?;

    r.success("Updated presences.")
}

/// Remove a day of presence
pub fn del_day<H: BaseRequestHandler>(r: &mut H) -> RequestResult {
    let group = r.post_forez_group("group")?;

    let mut list = forez_presence_helper::get_user_presences(&group, &r.user_id()?)?;

    let presence = Presence {
        id: 0,
        user_id: r.user_id()?,
        year: r.post_u32("year")?,
        month: r.post_u16("month")?,
        day: r.post_u16("day")?,
    };

    list.retain(|el| el != &presence);
    forez_presence_helper::update(&group, &r.user_id()?, list)?;

    r.success("Updated presences.")
}
