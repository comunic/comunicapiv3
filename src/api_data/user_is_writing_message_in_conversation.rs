//! # User is writing message in conversation event
//!
//! @author Pierre Hubert
use serde::Serialize;

use crate::data::conversation::ConvID;
use crate::data::user::UserID;

#[derive(Serialize)]
pub struct UserIsWritingMessageInConversation {
    user_id: u64,
    conv_id: u64,
}

impl UserIsWritingMessageInConversation {
    pub fn new(user_id: &UserID, conv_id: ConvID) -> Self {
        Self {
            user_id: user_id.id(),
            conv_id: conv_id.id(),
        }
    }
}
