//! # Utilities
//!
//! This module contains utilities that can be used anywhere in the code

pub mod crypt_utils;
pub mod date_utils;
pub mod mp3_utils;
pub mod mp4_utils;
pub mod network_utils;
pub mod pdf_utils;
pub mod string_utils;
pub mod user_data_utils;
pub mod virtual_directories_utils;
pub mod webpage_utils;
pub mod zip_utils;
