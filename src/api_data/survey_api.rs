//! # Survey API information
//!
//! @author Pierre Hubert

use std::collections::HashMap;

use serde::Serialize;

use crate::api_data::survey_choice_api::SurveyChoiceAPI;
use crate::data::error::ResultBoxError;
use crate::data::survey::Survey;
use crate::data::user::UserID;
use crate::helpers::survey_helper;

#[derive(Serialize)]
#[allow(non_snake_case)]
pub struct SurveyAPI {
    ID: u64,
    userID: u64,
    postID: u64,
    creation_time: u64,
    question: String,
    user_choice: u64,
    choices: HashMap<u64, SurveyChoiceAPI>,
    allowNewChoices: bool,
}

impl SurveyAPI {
    /// Create a new survey API entry
    pub fn new(s: &Survey, curr_user_id: Option<UserID>) -> ResultBoxError<SurveyAPI> {
        let user_choice = match &curr_user_id {
            None => 0, /* -1 is not possible */
            Some(user_id) => survey_helper::get_user_choice(s.id, user_id)?,
        };

        Ok(SurveyAPI {
            ID: s.id,
            userID: s.user_id.id(),
            postID: s.post_id,
            creation_time: s.time_create,
            question: s.question.clone(),
            user_choice,
            choices: SurveyChoiceAPI::for_list(&s.choices),
            allowNewChoices: s.allow_new_choices,
        })
    }
}
