//! # Admin authentication success
//!
//! Structure returned when an administrator successfully authenticate
//!
//! @author Pierre Hubert

use serde::Serialize;

#[derive(Serialize)]
pub struct AdminAuthSuccess {
    token: String,
}

impl AdminAuthSuccess {
    pub fn new(t: String) -> Self {
        Self { token: t }
    }
}
