//! # New group information
//!
//! @author Pierre Hubert

use crate::data::user::UserID;

pub struct NewGroup {
    pub name: String,
    pub owner_id: UserID,
}
