//! # Legacy API boolean
//!
//! true => 1
//! false => 0

use serde::{Serialize, Serializer};

/// Special implementation of conversation name (false if none / the name otherwise)
pub struct LegacyBool(pub bool);

impl Serialize for LegacyBool {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
    where
        S: Serializer,
    {
        match &self.0 {
            true => serializer.serialize_i8(1),
            false => serializer.serialize_i8(0),
        }
    }
}
