//! Use this structure for all structures that needs a "type" field
//!
//! @author Pierre Hubert
use serde::ser::SerializeMap;
use serde::{Serialize, Serializer};

pub struct TypeContainerAPI {
    t: String,
}

impl TypeContainerAPI {
    pub fn new(t: String) -> TypeContainerAPI {
        TypeContainerAPI { t }
    }
}

impl Serialize for TypeContainerAPI {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
    where
        S: Serializer,
    {
        let mut map = serializer.serialize_map(Some(1))?;
        map.serialize_entry("type", &self.t)?;
        map.end()
    }
}
