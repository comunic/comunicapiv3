//! # Group member API entry
//!
//! @author Pierre Hubert

use serde::Serialize;

use crate::data::group_member::GroupMember;

#[derive(Serialize)]
pub struct GroupMemberAPI {
    user_id: u64,
    group_id: u64,
    time_create: u64,
    level: String,
}

impl GroupMemberAPI {
    /// Turn a `GroupMember` object into an API entry
    pub fn new(g: &GroupMember) -> GroupMemberAPI {
        GroupMemberAPI {
            user_id: g.user_id.id(),
            group_id: g.group_id.id(),
            time_create: g.time_create,
            level: g.level.to_api(),
        }
    }

    /// Turn a list of GroupMember object into API entries
    pub fn for_list(l: &Vec<GroupMember>) -> Vec<GroupMemberAPI> {
        l.iter().map(Self::new).collect()
    }
}
