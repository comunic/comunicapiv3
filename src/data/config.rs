use std::collections::HashMap;
use std::error::Error;

use yaml_rust::{Yaml, YamlLoader};

use crate::data::group_id::GroupID;
use crate::utils::date_utils::time;

/// Server configuration
///
/// @author Pierre Hubert
#[derive(Debug)]
pub struct DatabaseConfig {
    pub host: String,
    pub name: String,
    pub username: String,
    pub password: String,
    pub log_all_queries: bool,
}

#[derive(Debug)]
pub struct RtcRelayConfig {
    pub enabled: bool,
    pub ip: String,
    pub token: String,
    pub ice_servers: Vec<String>,
    pub max_users_per_calls: u64,
    pub allow_video: bool,
    pub max_users_per_video_calls: u64,
}

#[derive(Debug)]
pub struct IndependentPushService {
    pub enabled: bool,
    pub control_url: String,
    pub control_token: String,
    pub public_url: String,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum BannerNature {
    Information,
    Warning,
    Success,
}

#[derive(Debug, serde::Serialize)]
pub struct Banner {
    pub enabled: bool,
    pub expire: Option<u64>,
    pub nature: BannerNature,
    pub message: HashMap<String, String>,
    pub link: Option<String>,
}

impl Banner {
    pub fn is_visible(&self) -> bool {
        self.enabled && self.expire.map(|v| v < 1 || v > time()).unwrap_or(true)
    }
}

#[derive(Debug)]
pub struct Config {
    pub port: u64,
    pub listen_address: String,
    pub storage_url: String,
    pub storage_path: String,
    pub serve_storage_file: bool,
    pub terms_url: String,
    pub privacy_policy_url: String,
    pub contact_email: String,
    pub password_reset_url: String,
    pub play_store_url: String,
    pub android_direct_download_url: String,
    pub proxy: Option<String>,
    pub verbose_mode: bool,
    pub independent_push_service: Option<IndependentPushService>,
    pub database: DatabaseConfig,
    pub rtc_relay: Option<RtcRelayConfig>,
    pub admin_url: String,
    pub banner: Option<Banner>,
    pub allow_reporting: bool,
    pub forez_groups: Vec<GroupID>,
}

/// Globally available configuration
static mut CONF: Option<Config> = None;

impl Config {
    fn parse_string_val(val: String) -> String {
        if val.starts_with("$") {
            std::env::var(&val[1..]).unwrap()
        } else {
            val
        }
    }

    fn yaml_str(parsed: &Yaml, name: &str) -> String {
        let val = match &parsed[name] {
            Yaml::Real(r) | Yaml::String(r) => r.to_string(),
            Yaml::Integer(i) => i.to_string(),
            Yaml::Boolean(true) => "true".to_string(),
            Yaml::Boolean(false) => "false".to_string(),
            Yaml::Array(_) | Yaml::Hash(_) | Yaml::Alias(_) | Yaml::Null => {
                panic!("{} can not be converted to string", name);
            }
            Yaml::BadValue => {
                panic!("{} is missing", name);
            }
        }
        .to_string();

        // Check if we have to get an environment variable
        Self::parse_string_val(val)
    }

    fn yaml_u64(parsed: &Yaml, name: &str) -> u64 {
        Self::yaml_str(parsed, name).parse().unwrap()
    }

    fn yaml_bool(parsed: &Yaml, name: &str) -> bool {
        Self::yaml_str(parsed, name).to_lowercase().eq("true")
    }

    /// Load the configuration from a given path
    pub fn load(path: &str) -> Result<(), Box<dyn Error>> {
        // Read & parse configuration
        let conf_str = std::fs::read_to_string(path)?;
        let parsed = YamlLoader::load_from_str(&conf_str)?;

        if parsed.len() != 1 {
            panic!("parsed.len() != 1");
        }
        let parsed = &parsed[0];

        // Read configuration
        let parsed_db = &parsed["database"];
        let database_conf = DatabaseConfig {
            host: Config::yaml_str(parsed_db, "host"),
            name: Config::yaml_str(parsed_db, "name"),
            username: Config::yaml_str(parsed_db, "username"),
            password: Config::yaml_str(parsed_db, "password"),
            log_all_queries: Config::yaml_bool(parsed_db, "log-all-queries"),
        };

        let parsed_rtc = &parsed["rtc-relay"];
        let rtc_config = match parsed_rtc.is_badvalue() {
            true => None,
            false => Some(RtcRelayConfig {
                enabled: Config::yaml_bool(parsed_rtc, "enabled"),
                ip: Config::yaml_str(parsed_rtc, "ip"),
                token: Config::yaml_str(parsed_rtc, "token"),
                ice_servers: parsed_rtc["ice-servers"]
                    .as_vec()
                    .unwrap()
                    .iter()
                    .map(|f| Self::parse_string_val(f.as_str().unwrap().to_string()))
                    .collect(),
                max_users_per_calls: Config::yaml_u64(parsed_rtc, "max-users-per-calls"),
                allow_video: Config::yaml_bool(parsed_rtc, "allow-video"),
                max_users_per_video_calls: Config::yaml_u64(
                    parsed_rtc,
                    "max-users-per-video-calls",
                ),
            }),
        };

        let proxy = Config::yaml_str(parsed, "proxy");

        let parsed_independent_push_service = &parsed["independent-push-service"];
        let independent_push_service = match parsed_independent_push_service.is_badvalue() {
            true => None,
            false => Some(IndependentPushService {
                enabled: Config::yaml_bool(parsed_independent_push_service, "enabled"),
                control_url: Config::yaml_str(parsed_independent_push_service, "control-url"),
                control_token: Config::yaml_str(parsed_independent_push_service, "control-token"),
                public_url: Config::yaml_str(parsed_independent_push_service, "public-url"),
            }),
        };

        let parsed_banner = &parsed["banner"];
        let banner = match parsed_banner.is_badvalue() {
            true => None,
            false => Some(Banner {
                enabled: Self::yaml_bool(parsed_banner, "enabled"),
                expire: match Self::yaml_u64(parsed_banner, "expire") {
                    0 => None,
                    v => Some(v),
                },
                nature: match Self::yaml_str(parsed_banner, "nature").as_str() {
                    "information" => BannerNature::Information,
                    "warning" => BannerNature::Warning,
                    "success" => BannerNature::Success,
                    v => panic!("Invalid banner nature: {} !", v),
                },
                message: parsed_banner["message"]
                    .as_hash()
                    .unwrap()
                    .iter()
                    .map(|(k, v)| {
                        (
                            k.as_str().unwrap().to_string(),
                            Self::parse_string_val(v.as_str().unwrap().to_string()),
                        )
                    })
                    .collect(),
                link: parsed_banner["link"]
                    .as_str()
                    .map(str::to_string)
                    .map(Self::parse_string_val)
                    .and_then(|s| match s.is_empty() {
                        true => None,
                        false => Some(s),
                    }),
            }),
        };

        let forez_groups = match &parsed["forez_groups"] {
            Yaml::BadValue => vec![],
            _ => Self::yaml_str(parsed, "forez_groups")
                .split(",")
                .map(|f| GroupID::new(f.trim().parse::<u64>().unwrap()))
                .collect(),
        };

        let conf = Config {
            port: Config::yaml_u64(parsed, "server-port"),
            listen_address: Config::yaml_str(parsed, "server-address"),

            storage_url: Config::yaml_str(parsed, "storage-url"),
            storage_path: Config::yaml_str(parsed, "storage-path"),

            serve_storage_file: Config::yaml_bool(parsed, "serve-storage-files"),

            terms_url: Config::yaml_str(parsed, "terms-url"),
            privacy_policy_url: Config::yaml_str(parsed, "privacy-policy-url"),

            contact_email: Config::yaml_str(parsed, "contact-email"),
            password_reset_url: Config::yaml_str(parsed, "password-reset-url"),

            play_store_url: Config::yaml_str(parsed, "play-store-url"),
            android_direct_download_url: Config::yaml_str(parsed, "android-direct-download-url"),

            proxy: match proxy.as_ref() {
                "none" => None,
                s => Some(s.to_string()),
            },

            verbose_mode: Config::yaml_bool(parsed, "verbose-mode"),

            independent_push_service,

            database: database_conf,

            rtc_relay: rtc_config,

            admin_url: Self::yaml_str(parsed, "admin-url"),

            banner,

            allow_reporting: Self::yaml_bool(parsed, "allow_reporting"),

            forez_groups,
        };

        // Save new configuration in memory
        unsafe {
            CONF = Some(conf);
        }

        Ok(())
    }

    /// Get the address this server listen to
    pub fn server_listen_address(&self) -> String {
        format!("{}:{}", self.listen_address, self.port)
    }

    /// Check if rtc relay is enabled
    pub fn is_rtc_relay_enabled(&self) -> bool {
        self.rtc_relay.is_some() && self.rtc_relay.as_ref().unwrap().enabled
    }

    /// Check if independent push notifications service is enabled
    pub fn is_independent_push_notifications_service_enabled(&self) -> bool {
        self.independent_push_service.is_some()
            && self.independent_push_service.as_ref().unwrap().enabled
    }
}

/// Get an instance of the configuration
pub fn conf() -> &'static Config {
    unsafe {
        return &CONF.as_ref().unwrap();
    }
}
