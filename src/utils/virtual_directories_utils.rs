//! # Virtual directory utilities
//!
//! @author Pierre Hubert

/// Check out whether a virtual directory is valid or not
///
/// ```
/// use comunic_server::utils::virtual_directories_utils::check_virtual_directory;
///
/// assert_eq!(check_virtual_directory("ab"), false);
/// assert_eq!(check_virtual_directory("john"), true);
/// assert_eq!(check_virtual_directory("secret123"), true);
/// assert_eq!(check_virtual_directory("ab.php"), false);
/// ```
pub fn check_virtual_directory(dir: &str) -> bool {
    if dir.len() < 4 || dir.len() > 30 {
        return false;
    }

    for d in vec![
        ".html", ".txt", ".php", "à", "â", "é", "ê", "@", "/", "\"", "'", "<", ">", "?", "&", "#",
    ] {
        if dir.contains(d) {
            return false;
        }
    }

    true
}
