//! # Export of all account's data
//!
//! @author Pierre Hubert

use std::collections::{HashMap, HashSet};

use crate::data::comment::Comment;
use crate::data::conversation::{ConvID, Conversation};
use crate::data::conversation_message::ConversationMessage;
use crate::data::error::ResultBoxError;
use crate::data::friend::Friend;
use crate::data::group_id::GroupID;
use crate::data::post::{Post, PostPageKind};
use crate::data::survey_response::SurveyResponse;
use crate::data::user::{User, UserID};
use crate::data::user_like::UserLike;
use crate::helpers::comments_helper;

pub struct AccountExport {
    pub user: User,
    pub posts: Vec<Post>,
    pub comments: Vec<Comment>,
    pub likes: Vec<UserLike>,
    pub survey_responses: Vec<SurveyResponse>,
    pub all_conversation_messages: Vec<ConversationMessage>,
    pub conversations: Vec<Conversation>,
    pub conversation_messages: HashMap<ConvID, Vec<ConversationMessage>>,
    pub friends_list: Vec<Friend>,
    pub groups: Vec<GroupID>,
}

impl AccountExport {
    /// Get the IDs of the related users
    pub fn get_related_users_ids(&self) -> ResultBoxError<HashSet<UserID>> {
        let mut set = HashSet::new();

        // Own user
        set.insert(self.user.id.clone());

        // Friends
        self.friends_list.iter().for_each(|f| {
            set.insert(f.friend_id.clone());
        });

        // Posts
        for post in &self.posts {
            set.insert(post.user_id.clone());

            if let PostPageKind::PAGE_KIND_USER(id) = &post.target_page {
                set.insert(id.clone());
            }

            comments_helper::get(post.id)?.iter().for_each(|f| {
                set.insert(f.user_id.clone());
            })
        }

        // Comments
        self.comments.iter().for_each(|f| {
            set.insert(f.user_id.clone());
        });

        // Conversation members
        for conv in &self.conversations {
            conv.members_ids().into_iter().for_each(|f| {
                set.insert(f);
            });
        }

        // Conversation messages
        for (_, conv_messages) in &self.conversation_messages {
            conv_messages.iter().for_each(|f| {
                set.extend(f.referenced_users_id());
            })
        }

        Ok(set)
    }
}
