//! # Entities constructor
//!
//! This trait simplify the construction of new items

pub trait EntitiesConstructor {
    type Item;

    /// Turn a server item into an API entry
    fn new(i: &Self::Item) -> Self;

    /// Parse a list of it
    fn for_list(l: &[Self::Item]) -> Vec<Self>
    where
        Self: std::marker::Sized,
    {
        l.iter().map(Self::new).collect()
    }
}
