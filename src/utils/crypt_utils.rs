//! # Cryptographic utilities
//!
//! Author: Pierre Hubert

extern crate sha1;

use rand::distributions::Alphanumeric;
use rand::{thread_rng, Rng};
use sha1::Digest;

use crate::data::error::{ExecError, ResultBoxError};

/// Generate a new sha1 string
///
/// ```
/// use comunic_server::utils::crypt_utils::sha1_str;
///
/// let res = sha1_str("secret");
/// assert_eq!(res, "e5e9fa1ba31ecd1ae84f75caaa474f3a663f05f4");
/// ```
pub fn sha1_str(input: &str) -> String {
    let mut hasher = sha1::Sha1::default();
    hasher.update(input.as_bytes());
    let result = hasher.finalize();
    format!("{:x}", result)
}

/// One-way user password crypt
///
/// This method is just transitional
///
/// It currently depends on PHP
///
/// ```
/// use comunic_server::utils::crypt_utils::legacy_crypt_pass;
///
/// let res = legacy_crypt_pass("secret").unwrap();
/// assert_eq!(res, "e5GUe5kdeUMGs");
/// ```
pub fn legacy_crypt_pass(pass: &str) -> ResultBoxError<String> {
    let hash = sha1_str(pass);

    let result = std::process::Command::new("/usr/bin/php")
        .arg("-r")
        .arg(format!("echo crypt('{}', '{}');", hash, hash))
        .output()?;

    if !result.status.success() {
        return Err(ExecError::boxed_new("Failed to crypt password!"));
    }

    Ok(String::from_utf8(result.stdout)?)
}

/// Generate a random string of a given size
///
/// ```
/// use comunic_server::utils::crypt_utils::rand_str;
///
/// let size = 10;
/// let rand = rand_str(size);
/// assert_eq!(size, rand.len());
/// ```
pub fn rand_str(len: usize) -> String {
    thread_rng()
        .sample_iter(&Alphanumeric)
        .map(char::from)
        .take(len)
        .collect()
}
