use std::fmt;
use std::fmt::Formatter;

use mysql::serde::de::Error;
use serde::de::Visitor;

pub struct U64Visitor;

impl<'de> Visitor<'de> for U64Visitor {
    type Value = u64;

    fn expecting<'a>(&self, formatter: &mut Formatter<'a>) -> fmt::Result {
        formatter.write_str("An unsigned integer value")
    }

    fn visit_u64<E>(self, v: u64) -> Result<Self::Value, E>
    where
        E: Error,
    {
        Ok(v)
    }
}
