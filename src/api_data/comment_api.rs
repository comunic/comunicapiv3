//! # Comments API entry
//!
//! @author Pierre Hubert
use serde::Serialize;

use crate::data::comment::Comment;
use crate::data::error::ResultBoxError;
use crate::data::user::UserID;
use crate::helpers::likes_helper;
use crate::helpers::likes_helper::LikeType;
use crate::utils::user_data_utils::user_data_url;

#[derive(Serialize)]
#[allow(non_snake_case)]
pub struct CommentAPI {
    ID: u64,
    userID: u64,
    postID: u64,
    time_sent: u64,
    content: String,
    image_path: Option<String>,
    img_url: Option<String>,
    likes: u64,
    userlike: bool,
}

impl CommentAPI {
    /// Construct a new `CommentAPI` object
    pub fn new(c: &Comment, curr_user_id: &Option<UserID>) -> ResultBoxError<CommentAPI> {
        let mut c_api = CommentAPI {
            ID: c.id,
            userID: c.user_id.id(),
            postID: c.post_id,
            time_sent: c.time_sent,
            content: c.content.clone(),
            image_path: c.image_path.clone(),
            img_url: c.image_path.as_ref().map(|f| user_data_url(f)),
            likes: likes_helper::count(c.id, LikeType::COMMENT)? as u64,
            userlike: false,
        };

        // If required, check if current user is liking the comment
        if let Some(user_id) = curr_user_id {
            if c_api.likes > 0 {
                c_api.userlike = likes_helper::is_liking(user_id, c.id, LikeType::COMMENT)?;
            }
        }

        Ok(c_api)
    }

    pub fn for_list(
        l: &[Comment],
        curr_user_id: &Option<UserID>,
    ) -> ResultBoxError<Vec<CommentAPI>> {
        l.iter().map(|c| Self::new(c, curr_user_id)).collect()
    }
}
