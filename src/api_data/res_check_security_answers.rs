//! # Check security answsers result
//!
//! If the user gave valid security answers, we give him a password reset token.
//!
//! @author Pierre Hubert
use serde::Serialize;

#[derive(Serialize)]
pub struct ResCheckSecurityAnswers {
    reset_token: String,
}

impl ResCheckSecurityAnswers {
    pub fn new(reset_token: String) -> ResCheckSecurityAnswers {
        ResCheckSecurityAnswers { reset_token }
    }
}
