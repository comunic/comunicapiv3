//! # Admin account controller
//!
//! @author Pierre Hubert

use crate::api_data::admin::admin_auth_options::AdminAuthOptions;
use crate::api_data::admin::admin_auth_success::AdminAuthSuccess;
use crate::api_data::admin::admin_id_api::AdminIDAPI;
use crate::api_data::admin::admin_info_api::AdminInfoAPI;
use crate::api_data::admin::admin_res_create_account::AdminResCreateAccount;
use crate::api_data::admin::admin_res_create_reset_token::AdminResCreateResetToken;
use crate::constants::admin::AdminRole;
use crate::data::admin::{NewAdmin, NewAdminGeneralSettings};
use crate::data::admin_action_log::AdminAction;
use crate::data::base_request_handler::BaseRequestHandler;
use crate::data::http_request_handler::HttpRequestHandler;
use crate::helpers::admin_log_helper::log_admin_action;
use crate::helpers::{admin_access_token_helper, admin_account_helper, admin_account_key_helper};
use crate::routes::RequestResult;
use crate::utils::date_utils::time;

/// Create a new administrator account
pub async fn create(r: &mut HttpRequestHandler) -> RequestResult {
    let email = r.post_email("mail")?;
    let name = r.post_string_opt("name", 3, true)?;

    let new_admin = NewAdmin { name, email };
    let admin_id = admin_account_helper::create(&new_admin)?;

    log_admin_action(
        r.admin_id()?,
        &r.remote_ip(),
        AdminAction::CreatedAdmin {
            id: admin_id,
            name: new_admin.name,
            email: new_admin.email,
        },
    )?;

    r.set_response(AdminResCreateAccount::new(admin_id))
}

/// Get admin auth options
pub async fn get_auth_options(r: &mut HttpRequestHandler) -> RequestResult {
    let mail = r.post_email("mail")?;
    let admin = admin_account_helper::find_admin_by_email(&mail)?;
    let keys = admin_account_key_helper::get_admin_keys(admin.id)?;

    r.set_response(AdminAuthOptions::new(&admin, &keys))
}

/// Login admin using a reset token
pub async fn auth_with_reset_token(r: &mut HttpRequestHandler) -> RequestResult {
    let reset_token = r.post_string("token")?;
    let admin = admin_account_helper::find_admin_by_email(&r.post_email("mail")?)?;

    let token = r.some_or_internal_error(
        admin.reset_token,
        "Specified user has not valid reset token for now!",
    )?;

    if !token.token.eq(&reset_token) || time() > token.expire {
        return r.forbidden("Specified reset token is invalid!".to_string());
    }

    let token = admin_access_token_helper::create(admin.id)?;

    log_admin_action(admin.id, &r.remote_ip(), AdminAction::AuthWithResetToken)?;

    r.set_response(AdminAuthSuccess::new(token))
}

/// Sign out current admin
pub async fn sign_out(r: &mut HttpRequestHandler) -> RequestResult {
    admin_access_token_helper::destroy(r.admin_id()?)?;

    r.ok()
}

/// Get current admin ID
pub async fn get_admin_id(r: &mut HttpRequestHandler) -> RequestResult {
    r.set_response(AdminIDAPI::new(r.admin_id()?))
}

/// Get and return the list of administrators
pub async fn get_list(r: &mut HttpRequestHandler) -> RequestResult {
    let list = admin_account_helper::get_list()?
        .iter()
        .map(AdminInfoAPI::new)
        .collect::<Vec<AdminInfoAPI>>();

    r.set_response(list)
}

/// Get current admin information
pub async fn get_admin_info(r: &mut HttpRequestHandler) -> RequestResult {
    let admin = match r.has_post_parameter("id") {
        false => admin_account_helper::find_admin_by_id(r.admin_id()?)?,
        true => {
            let admin_id = r.post_admin_id("id")?;
            admin_account_helper::find_admin_by_id(admin_id)?
        }
    };

    r.set_response(AdminInfoAPI::new(&admin))
}

/// Update general settings
pub async fn update_general_settings(r: &mut HttpRequestHandler) -> RequestResult {
    let admin_id = r.post_admin_id("id")?;
    let new_name = r.post_string("name")?;
    let new_email = r.post_email("email")?;

    if admin_id != r.admin_id()? {
        r.check_admin_has_role(AdminRole::MANAGE_ADMINS)?;
    }

    admin_account_helper::set_general_settings(NewAdminGeneralSettings {
        id: admin_id,
        name: new_name.to_string(),
        email: new_email.to_string(),
    })?;

    log_admin_action(
        r.admin_id()?,
        &r.remote_ip(),
        AdminAction::UpdatedAdminGeneralSettings {
            target: admin_id,
            new_name,
            new_email,
        },
    )?;

    r.ok()
}

/// Generate access reset token
pub async fn generate_reset_token(r: &mut HttpRequestHandler) -> RequestResult {
    let admin_id = r.post_admin_id("id")?;

    if admin_id != r.admin_id()? {
        r.check_admin_has_role(AdminRole::MANAGE_ADMINS)?;
    }

    let token = admin_account_helper::create_new_reset_token(admin_id)?;

    log_admin_action(
        r.admin_id()?,
        &r.remote_ip(),
        AdminAction::GeneratedAdminResetToken { target: admin_id },
    )?;

    r.set_response(AdminResCreateResetToken::new(token))
}
