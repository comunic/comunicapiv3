//! # Admin public data
//!
//! @author Pierre Hubert

pub mod admin_auth_options;
pub mod admin_auth_success;
pub mod admin_create_password_reset_link_api;
pub mod admin_id_api;
pub mod admin_info_api;
pub mod admin_keys_api;
pub mod admin_log_api;
pub mod admin_res_create_account;
pub mod admin_res_create_reset_token;
pub mod admin_role_api;
pub mod admin_search_user_result_api;
pub mod admin_user_info_api;
