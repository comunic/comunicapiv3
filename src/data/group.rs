//! # Group information
//!
//! Group visibility level

use std::path::PathBuf;

use crate::constants::DEFAULT_GROUP_LOGO;
use crate::data::group_id::GroupID;
use crate::utils::user_data_utils::{user_data_path, user_data_url};

#[allow(non_camel_case_types)]
#[derive(Eq, PartialEq, Hash, Debug)]
pub enum GroupVisibilityLevel {
    OPEN_GROUP,
    PRIVATE_GROUP,
    SECRETE_GROUP,
}

impl GroupVisibilityLevel {
    pub fn to_api(&self) -> String {
        match self {
            GroupVisibilityLevel::OPEN_GROUP => "open",
            GroupVisibilityLevel::PRIVATE_GROUP => "private",
            GroupVisibilityLevel::SECRETE_GROUP => "secrete",
        }
        .to_string()
    }

    pub fn from_api(level: &str) -> GroupVisibilityLevel {
        match level {
            "open" => GroupVisibilityLevel::OPEN_GROUP,
            "private" => GroupVisibilityLevel::PRIVATE_GROUP,
            "secrete" => GroupVisibilityLevel::SECRETE_GROUP,
            _ => GroupVisibilityLevel::SECRETE_GROUP,
        }
    }
}

#[allow(non_camel_case_types)]
#[derive(Eq, PartialEq, Hash, Debug)]
pub enum GroupRegistrationLevel {
    OPEN_REGISTRATION = 0,
    MODERATED_REGISTRATION = 1,
    CLOSED_REGISTRATION = 2,
}

impl GroupRegistrationLevel {
    pub fn to_api(&self) -> String {
        match self {
            GroupRegistrationLevel::OPEN_REGISTRATION => "open",
            GroupRegistrationLevel::MODERATED_REGISTRATION => "moderated",
            GroupRegistrationLevel::CLOSED_REGISTRATION => "closed",
        }
        .to_string()
    }

    pub fn from_api(level: &str) -> GroupRegistrationLevel {
        match level {
            "open" => GroupRegistrationLevel::OPEN_REGISTRATION,
            "moderated" => GroupRegistrationLevel::MODERATED_REGISTRATION,
            "closed" => GroupRegistrationLevel::CLOSED_REGISTRATION,
            _ => GroupRegistrationLevel::CLOSED_REGISTRATION,
        }
    }
}

#[allow(non_camel_case_types)]
#[derive(Eq, PartialEq, Hash, Debug)]
pub enum GroupPostsCreationLevel {
    //Only the moderators and the administrator can create posts
    POSTS_LEVEL_MODERATORS = 0,

    //All the members of the group can create posts
    POSTS_LEVEL_ALL_MEMBERS = 1,
}

impl GroupPostsCreationLevel {
    pub fn to_api(&self) -> String {
        match self {
            GroupPostsCreationLevel::POSTS_LEVEL_MODERATORS => "moderators",
            GroupPostsCreationLevel::POSTS_LEVEL_ALL_MEMBERS => "members",
        }
        .to_string()
    }

    pub fn from_api(level: &str) -> GroupPostsCreationLevel {
        match level {
            "members" => GroupPostsCreationLevel::POSTS_LEVEL_ALL_MEMBERS,
            "moderators" => GroupPostsCreationLevel::POSTS_LEVEL_MODERATORS,
            _ => GroupPostsCreationLevel::POSTS_LEVEL_MODERATORS,
        }
    }
}

#[allow(non_camel_case_types)]
#[derive(Eq, PartialEq, Hash, Debug, PartialOrd)]
pub enum GroupAccessLevel {
    //Can not even know if the group exists or not
    NO_ACCESS = 0,

    //Access to the name of the group only
    LIMITED_ACCESS = 1,

    //Can see the posts of the group, but not a member of the group
    VIEW_ACCESS = 2,

    //Member access (same as view access but as member)
    MEMBER_ACCESS = 3,

    //Can create posts, even if posts creation is restricted
    MODERATOR_ACCESS = 4,

    //Can do everything
    ADMIN_ACCESS = 5,
}

/// Group information
#[derive(Debug)]
pub struct Group {
    pub id: GroupID,
    pub name: String,
    pub visibility: GroupVisibilityLevel,
    pub registration_level: GroupRegistrationLevel,
    pub posts_creation_level: GroupPostsCreationLevel,
    pub is_members_list_public: bool,
    pub logo: Option<String>,
    pub virtual_directory: Option<String>,
    pub time_create: u64,
    pub description: Option<String>,
    pub url: Option<String>,
}

impl Group {
    /// Check out whether current group has a logo or not
    pub fn has_logo(&self) -> bool {
        self.logo.is_some()
    }

    /// Determine the path of the logo to use for this group
    pub fn get_logo_path(&self) -> &str {
        match &self.logo {
            None => DEFAULT_GROUP_LOGO,
            Some(l) => l,
        }
    }

    /// Get the current URL for the logo of this group
    pub fn get_logo_url(&self) -> String {
        user_data_url(self.get_logo_path())
    }

    /// Get file access to the logo
    pub fn get_logo_sys_path(&self) -> PathBuf {
        if !self.has_logo() {
            panic!("This group has no logo!")
        }

        user_data_path(self.logo.as_ref().unwrap().as_ref())
    }
}

#[cfg(test)]
mod tests {
    use crate::data::group::GroupAccessLevel;

    #[test]
    fn access_level_coherence() {
        assert!(GroupAccessLevel::NO_ACCESS < GroupAccessLevel::LIMITED_ACCESS);
        assert!(GroupAccessLevel::LIMITED_ACCESS < GroupAccessLevel::VIEW_ACCESS);
        assert!(GroupAccessLevel::VIEW_ACCESS < GroupAccessLevel::MEMBER_ACCESS);
        assert!(GroupAccessLevel::MEMBER_ACCESS < GroupAccessLevel::MODERATOR_ACCESS);
        assert!(GroupAccessLevel::MODERATOR_ACCESS < GroupAccessLevel::ADMIN_ACCESS);
    }
}
