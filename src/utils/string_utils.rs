//! # String utilities
//!
//! This module contains utilities that can be used accross all the application

use std::str::FromStr;

use actix_web::http::Uri;
use regex::Regex;

/// Escape an HTML string
///
/// Removes the HTML code included inside a string
///
/// ```
/// use comunic_server::utils::string_utils::remove_html_nodes;
///
/// let s1 = "<b>hello world</b>";
/// let res1 = remove_html_nodes(s1);
/// assert_eq!(res1, "&lt;b&gt;hello world&lt;/b&gt;");
/// ```
pub fn remove_html_nodes(input: &str) -> String {
    input.replace('<', "&lt;").replace('>', "&gt;")
}

/// Check out whether a URL is valid or not
///
/// ```
/// use comunic_server::utils::string_utils::check_url;
///
/// let url1 = "http://communniquons.org/?url=some&arg2=myname#content3";
/// assert_eq!(check_url(url1), true);
///
/// let url2 = "h@ttp://communniquons.org/?url=some&arg2=myname#content3";
/// assert_eq!(check_url(url2), false);
/// ```
pub fn check_url(url: &str) -> bool {
    Uri::from_str(url).is_ok()
}

/// Check a string before its insertion
///
/// Legacy function that might be completed / replaced in the future
///
/// ```
/// use comunic_server::utils::string_utils::check_string_before_insert;
///
/// assert_eq!(check_string_before_insert("s"), false);
/// assert_eq!(check_string_before_insert("    s"), false);
/// assert_eq!(check_string_before_insert("Hello world"), true);
/// ```
pub fn check_string_before_insert(s: &str) -> bool {
    s.trim().len() > 2
}

/// Check the validity of a YouTube ID
///
/// ```
/// use comunic_server::utils::string_utils::check_youtube_id;
///
/// assert_eq!(check_youtube_id("/ab/"), false);
/// assert_eq!(check_youtube_id("abxZ96C"), true);
/// assert_eq!(check_youtube_id("a6C"), false);
/// ```
pub fn check_youtube_id(id: &str) -> bool {
    id.len() >= 5
        && !id.contains('/')
        && !id.contains('\\')
        && !id.contains('@')
        && !id.contains('&')
        && !id.contains('?')
        && !id.contains('.')
        && !id.contains('\'')
        && !id.contains('\"')
}

/// Check the validity of an emoji shortcut
///
/// ```
/// use comunic_server::utils::string_utils::check_emoji_code;
///
/// assert_eq!(check_emoji_code(":comunic:"), true);
/// assert_eq!(check_emoji_code(":comunic"), false);
/// assert_eq!(check_emoji_code("::"), false);
/// assert_eq!(check_emoji_code("a:comunic:"), false);
/// assert_eq!(check_emoji_code(":comunic:a"), false);
/// assert_eq!(check_emoji_code(":co:munic:"), false);
/// assert_eq!(check_emoji_code(":comuni@c:"), false);
/// assert_eq!(check_emoji_code("bbb:comuni@c:123"), false);
/// ```
pub fn check_emoji_code(shortcut: &str) -> bool {
    let r = Regex::new(r"^:[a-zA-Z0-9]+:$").unwrap();
    r.is_match(shortcut)
}

/// Check the validity of an HTML color
///
/// ```
/// use comunic_server::utils::string_utils::check_html_color;
///
/// assert_eq!(check_html_color("AAFF00"), true);
/// assert_eq!(check_html_color("ABC"), false);
/// assert_eq!(check_html_color("UUFF00"), false);
/// ```
pub fn check_html_color(color: &str) -> bool {
    let r = Regex::new(r"^[A-F0-9]{6}$").unwrap();
    r.is_match(color)
}
