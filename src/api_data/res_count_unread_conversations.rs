//! Count the number of unread conversations of a given user
//!
//! @author Pierre Hubert
use serde::Serialize;

#[derive(Serialize)]
pub struct ResultCountUnreadConversations {
    nb_unread: usize,
}

impl ResultCountUnreadConversations {
    /// Generate an new instance
    pub fn new(count: usize) -> ResultCountUnreadConversations {
        ResultCountUnreadConversations { nb_unread: count }
    }
}
