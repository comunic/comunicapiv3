//! User custom emoji
//!
//! @author Pierre Hubert

use std::path::PathBuf;

use crate::data::user::UserID;
use crate::utils::user_data_utils::user_data_path;

#[derive(Debug)]
pub struct CustomEmoji {
    pub id: u64,
    pub user_id: UserID,
    pub shortcut: String,
    pub path: String,
}

impl CustomEmoji {
    /// Get the system path pointing on this custom emoji
    pub fn sys_path(&self) -> PathBuf {
        user_data_path(self.path.as_ref())
    }
}
