//! # Admin roles controller
//!
//! @author Pierre Hubert

use crate::api_data::admin::admin_role_api::AdminRoleDetailsAPI;
use crate::constants::admin::{AdminRole, ADMIN_ROLES_LIST};
use crate::data::admin_action_log::AdminAction;
use crate::data::base_request_handler::BaseRequestHandler;
use crate::data::http_request_handler::HttpRequestHandler;
use crate::helpers::admin_log_helper::log_admin_action;
use crate::helpers::admin_roles_helper;
use crate::routes::RequestResult;

/// Get the list of roles embedded in the code
pub async fn get_list(r: &mut HttpRequestHandler) -> RequestResult {
    let res = ADMIN_ROLES_LIST
        .iter()
        .map(AdminRoleDetailsAPI::new)
        .collect::<Vec<AdminRoleDetailsAPI>>();

    r.set_response(res)
}

/// Toggle the status of a role for an admin
pub async fn toggle(r: &mut HttpRequestHandler) -> RequestResult {
    let admin_id = r.post_admin_id("adminID")?;
    let enable = r.post_bool("enable")?;
    let role_str = r.post_string("role")?;
    let role = r.some_or_bad_request(
        AdminRole::from_id(&role_str),
        "Specified role does not exists!",
    )?;

    if !enable {
        admin_roles_helper::remove_role(admin_id, role)?;
        log_admin_action(
            r.admin_id()?,
            &r.remote_ip(),
            AdminAction::RemoveAdminRole {
                target: admin_id,
                role: role_str,
            },
        )?;
    } else if !admin_roles_helper::has_role(admin_id, role)? {
        admin_roles_helper::add_role(admin_id, role)?;

        log_admin_action(
            r.admin_id()?,
            &r.remote_ip(),
            AdminAction::AddAdminRole {
                target: admin_id,
                role: role_str,
            },
        )?;
    }

    r.ok()
}
