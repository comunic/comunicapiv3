//! # Virtual directory controller
//!
//! @author Pierre Hubert

use crate::api_data::res_find_user_by_virtual_directory::FindUserByVirtualDirectoryAPIResult;
use crate::api_data::res_find_virtual_directory::ResultFindVirtualDirectory;
use crate::data::base_request_handler::BaseRequestHandler;
use crate::data::http_request_handler::HttpRequestHandler;
use crate::helpers::{groups_helper, user_helper};
use crate::routes::RequestResult;

/// Find a user by its virtual directory
pub async fn find_user(r: &mut HttpRequestHandler) -> RequestResult {
    let directory = r.post_virtual_directory("subfolder")?;

    let user = r.ok_or_not_found(
        user_helper::find_user_by_virtual_directory(&directory),
        "No user was found with this subfolder!",
    )?;

    r.set_response(FindUserByVirtualDirectoryAPIResult::new(user.id))
}

/// Search for user / group with a given virtual directory
pub async fn find(r: &mut HttpRequestHandler) -> RequestResult {
    let directory = r.post_virtual_directory("directory")?;

    let user = user_helper::find_user_by_virtual_directory(&directory);
    let group = groups_helper::find_by_virtual_directory(&directory);

    if user.is_err() && group.is_err() {
        println!(
            "Find virtual directory errors:\n* User: {}\n* Group: {}",
            user.unwrap_err(),
            group.unwrap_err()
        );
        r.not_found("Specified user / group virtual directory not found !".to_string())
    } else {
        r.set_response(ResultFindVirtualDirectory::new(user, group))
    }
}
