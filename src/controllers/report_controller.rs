use crate::api_data::http_error::HttpError;
use crate::api_data::submit_report_result_api::SubmitReportResultApi;
use crate::constants::reports;
use crate::data::base_request_handler::BaseRequestHandler;
use crate::data::config::conf;
use crate::data::group::GroupAccessLevel;
use crate::data::http_request_handler::HttpRequestHandler;
use crate::data::post::PostAccessLevel;
use crate::data::report::{Report, ReportID, ReportTarget, REPORT_CAUSES};
use crate::helpers::reports_helper;
use crate::routes::RequestResult;
use crate::utils::date_utils::time;

/// Submit a new report
pub async fn report(r: &mut HttpRequestHandler) -> RequestResult {
    if !conf().allow_reporting {
        r.bad_request("Reporting is disabled!".to_string())?;
    }

    let comment = r.post_string_without_html_opt("comment", 0)?;
    if comment.as_ref().map(String::len).unwrap_or(0) as u32 > reports::MAX_COMMENT_LENGTH {
        r.bad_request("Report comment is too long!".to_string())?;
    }

    let cause_id = r.post_string("cause")?;
    let cause = r.some_or_bad_request(
        REPORT_CAUSES.iter().find(|c| c.id().id().eq(&cause_id)),
        "No valid report cause was given in the request!",
    )?;

    let target = match r.post_string("target_type")?.as_str() {
        "post" => ReportTarget::Post(
            r.post_post_with_access("target_id", PostAccessLevel::BASIC_ACCESS)?
                .id,
        ),

        "comment" => ReportTarget::Comment(r.post_comment_with_access("target_id")?.id),

        "conversation" => ReportTarget::Conversation(r.post_conv("target_id")?.conv_id),

        "conversation_message" => {
            ReportTarget::ConversationMessage(r.post_conv_message_id("target_id")?.id)
        }

        "user" => ReportTarget::User(r.post_user_id("target_id")?),

        "group" => ReportTarget::Group(
            r.post_group_id_with_access("target_id", GroupAccessLevel::LIMITED_ACCESS)?,
        ),

        _ => {
            r.bad_request("Unknown target type!".to_string())?;
            unreachable!();
        }
    };

    let report = Report {
        id: ReportID(0),
        user_id: r.user_id()?,
        target,
        time: time(),
        cause,
        comment,
    };

    // Check for duplicate
    if reports_helper::already_exists(&report)? {
        r.set_error(HttpError::new(
            409,
            "You have already submitted a report for this resource!",
        ));
        return Ok(());
    }

    // Save report
    let report_id = reports_helper::save_report(report)?;

    r.set_response(SubmitReportResultApi::new(report_id))
}
