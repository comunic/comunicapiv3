//! # Server configuration
//!
//! @author Pierre Hubert
use std::collections::HashMap;

use serde::Serialize;

use crate::constants::accounts_info_policy::{
    MAX_FIRST_NAME_LENGTH, MAX_LAST_NAME_LENGTH, MAX_LOCATION_LENGTH, MIN_FIRST_NAME_LENGTH,
    MIN_LAST_NAME_LENGTH,
};
use crate::constants::conversations::{
    ALLOWED_CONVERSATION_FILES_TYPES, CONVERSATION_FILES_MAX_SIZE,
    CONVERSATION_WRITING_EVENT_INTERVAL, CONVERSATION_WRITING_EVENT_LIFETIME,
    MAX_CONVERSATION_MESSAGE_LENGTH, MAX_CONVERSATION_NAME_LENGTH, MAX_CONV_IMAGE_MESSAGE_WIDTH,
    MAX_CONV_LOGO_HEIGHT, MAX_CONV_LOGO_WIDTH, MAX_CONV_MESSAGE_THUMBNAIL_HEIGHT,
    MAX_CONV_MESSAGE_THUMBNAIL_WIDTH, MIN_CONVERSATION_MESSAGE_LENGTH,
};
use crate::constants::reports::CAN_USER_REPORT_ITS_OWN_CONTENT;
use crate::constants::{
    conservation_policy, password_policy, reports, MIN_SUPPORTED_MOBILE_VERSION,
};
use crate::data::api_client::APIClient;
use crate::data::config::{conf, Banner};
use crate::data::report::REPORT_CAUSES;

#[derive(Serialize)]
struct NotificationsConfig {
    has_firebase: bool,
    has_independent: bool,
}

#[derive(Serialize)]
struct PasswordPolicy {
    allow_email_in_password: bool,
    allow_name_in_password: bool,
    min_password_length: u64,
    min_number_upper_case_letters: u64,
    min_number_lower_case_letters: u64,
    min_number_digits: u64,
    min_number_special_characters: u64,
    min_categories_presence: u64,
}

#[derive(Serialize)]
struct DataConservationPolicy {
    min_inactive_account_lifetime: u64,
    min_notification_lifetime: u64,
    min_comments_lifetime: u64,
    min_posts_lifetime: u64,
    min_conversation_messages_lifetime: u64,
    min_likes_lifetime: u64,
}

#[derive(Serialize)]
struct ConversationsPolicy {
    max_conversation_name_len: usize,
    min_message_len: usize,
    max_message_len: usize,
    allowed_files_type: [&'static str; 18],
    files_max_size: usize,
    writing_event_interval: u64,
    writing_event_lifetime: u64,
    max_message_image_width: u32,
    max_message_image_height: u32,
    max_thumbnail_width: u32,
    max_thumbnail_height: u32,
    max_logo_width: u32,
    max_logo_height: u32,
}

#[derive(Serialize)]
struct AccountInformationPolicy {
    min_first_name_length: u8,
    max_first_name_length: u8,
    min_last_name_length: u8,
    max_last_name_length: u8,
    max_location_length: usize,
}

#[derive(Serialize)]
struct ReportCause {
    id: &'static str,
    label: HashMap<&'static str, &'static str>,
}

#[derive(Serialize)]
struct ReportPolicy {
    causes: Vec<ReportCause>,
    max_comment_length: u32,
    can_user_report_his_own_content: bool,
}

#[derive(Serialize)]
pub struct ServerConfig {
    min_supported_mobile_version: &'static str,
    terms_url: &'static str,
    privacy_policy_url: &'static str,
    contact_email: &'static str,
    play_store_url: &'static str,
    android_direct_download_url: String,

    banner: Option<&'static Banner>,
    push_notifications: NotificationsConfig,
    password_policy: PasswordPolicy,
    data_conservation_policy: DataConservationPolicy,
    conversations_policy: ConversationsPolicy,
    account_info_policy: AccountInformationPolicy,
    report_policy: Option<ReportPolicy>,
}

impl ServerConfig {
    pub fn new(c: &APIClient) -> Self {
        ServerConfig {
            min_supported_mobile_version: MIN_SUPPORTED_MOBILE_VERSION,
            terms_url: &conf().terms_url,
            privacy_policy_url: &conf().privacy_policy_url,
            contact_email: &conf().contact_email,
            play_store_url: &conf().play_store_url,
            android_direct_download_url: conf().android_direct_download_url.clone(),

            banner: match conf()
                .banner
                .as_ref()
                .map(|b| b.is_visible())
                .unwrap_or(false)
            {
                true => conf().banner.as_ref(),
                false => None,
            },

            push_notifications: NotificationsConfig {
                has_firebase: c.is_firebase_available(),
                has_independent: conf().is_independent_push_notifications_service_enabled(),
            },

            password_policy: PasswordPolicy {
                allow_email_in_password: password_policy::ALLOW_EMAIL_IN_PASSWORD,
                allow_name_in_password: password_policy::ALLOW_NAME_IN_PASSWORD,
                min_password_length: password_policy::MIN_PASSWORD_LENGTH,
                min_number_upper_case_letters: password_policy::MIN_NUMBER_UPPERCASE_LETTER,
                min_number_lower_case_letters: password_policy::MIN_NUMBER_LOWERCASE_LETTER,
                min_number_digits: password_policy::MIN_NUMBER_DIGITS,
                min_number_special_characters: password_policy::MIN_NUMBER_SPECIAL_CHARACTERS,
                min_categories_presence: password_policy::MIN_CATEGORIES_PRESENCE,
            },

            data_conservation_policy: DataConservationPolicy {
                min_inactive_account_lifetime: conservation_policy::MIN_INACTIVE_ACCOUNT_LIFETIME
                    .as_secs(),
                min_notification_lifetime: conservation_policy::MIN_NOTIFICATIONS_LIFETIME
                    .as_secs(),
                min_comments_lifetime: conservation_policy::MIN_COMMENTS_LIFETIME.as_secs(),
                min_posts_lifetime: conservation_policy::MIN_POSTS_LIFETIME.as_secs(),
                min_conversation_messages_lifetime:
                    conservation_policy::MIN_CONVERSATION_MESSAGES_LIFETIME.as_secs(),
                min_likes_lifetime: conservation_policy::MIN_LIKES_LIFETIME.as_secs(),
            },

            conversations_policy: ConversationsPolicy {
                max_conversation_name_len: MAX_CONVERSATION_NAME_LENGTH,
                min_message_len: MIN_CONVERSATION_MESSAGE_LENGTH,
                max_message_len: MAX_CONVERSATION_MESSAGE_LENGTH,
                allowed_files_type: ALLOWED_CONVERSATION_FILES_TYPES,
                files_max_size: CONVERSATION_FILES_MAX_SIZE,
                writing_event_interval: CONVERSATION_WRITING_EVENT_INTERVAL,
                writing_event_lifetime: CONVERSATION_WRITING_EVENT_LIFETIME,
                max_message_image_width: MAX_CONV_IMAGE_MESSAGE_WIDTH,
                max_message_image_height: MAX_CONV_IMAGE_MESSAGE_WIDTH,
                max_thumbnail_width: MAX_CONV_MESSAGE_THUMBNAIL_WIDTH,
                max_thumbnail_height: MAX_CONV_MESSAGE_THUMBNAIL_HEIGHT,
                max_logo_width: MAX_CONV_LOGO_WIDTH,
                max_logo_height: MAX_CONV_LOGO_HEIGHT,
            },

            account_info_policy: AccountInformationPolicy {
                min_first_name_length: MIN_FIRST_NAME_LENGTH,
                max_first_name_length: MAX_FIRST_NAME_LENGTH,
                min_last_name_length: MIN_LAST_NAME_LENGTH,
                max_last_name_length: MAX_LAST_NAME_LENGTH,
                max_location_length: MAX_LOCATION_LENGTH,
            },

            report_policy: match conf().allow_reporting {
                true => Some(ReportPolicy {
                    causes: REPORT_CAUSES
                        .iter()
                        .map(|r| ReportCause {
                            id: r.id().id(),
                            label: HashMap::from([("fr", r.label_fr), ("en", r.label_en)]),
                        })
                        .collect(),
                    max_comment_length: reports::MAX_COMMENT_LENGTH,
                    can_user_report_his_own_content: CAN_USER_REPORT_ITS_OWN_CONTENT,
                }),
                false => None,
            },
        }
    }
}
