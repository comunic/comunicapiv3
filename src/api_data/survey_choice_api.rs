//! # Survey choice API
//!
//! @author Pierre Hubert

use std::collections::HashMap;

use serde::Serialize;

use crate::data::survey::SurveyChoice;

#[derive(Serialize)]
#[allow(non_snake_case)]
pub struct SurveyChoiceAPI {
    choiceID: u64,
    name: String,
    responses: u64,
}

impl SurveyChoiceAPI {
    pub fn new(c: &SurveyChoice) -> SurveyChoiceAPI {
        SurveyChoiceAPI {
            choiceID: c.id,
            name: c.name.clone(),
            responses: c.count,
        }
    }

    pub fn for_list(c: &[SurveyChoice]) -> HashMap<u64, SurveyChoiceAPI> {
        let mut map = HashMap::with_capacity(c.len());
        c.iter().for_each(|c| {
            map.insert(c.id, Self::new(c));
        });
        map
    }
}
