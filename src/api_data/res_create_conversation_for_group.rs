//! # Conversation for group creation result
//!
//! This structure returns the ID of the create conversation

use serde::Serialize;

use crate::data::conversation::ConvID;

#[derive(Serialize)]
pub struct ResCreateConversationForGroup {
    conv_id: u64,
}

impl ResCreateConversationForGroup {
    pub fn new(conv_id: ConvID) -> Self {
        Self {
            conv_id: conv_id.id(),
        }
    }
}
