//! API output of finding user by virtual directory
//!
//! @author Pierre Hubert

use crate::data::user::UserID;
use serde::Serialize;

#[derive(Serialize)]
#[allow(non_snake_case)]
pub struct FindUserByVirtualDirectoryAPIResult {
    userID: u64,
}

impl FindUserByVirtualDirectoryAPIResult {
    /// Construct a new `FindUserByVirtualDirectoryAPIResult` instance
    pub fn new(user_id: UserID) -> FindUserByVirtualDirectoryAPIResult {
        FindUserByVirtualDirectoryAPIResult {
            userID: user_id.id(),
        }
    }
}
