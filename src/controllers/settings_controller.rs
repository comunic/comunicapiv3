//! # Settings controller
//!
//! @author Pierre Hubert

use crate::api_data::account_image_settings_api::AccountImageSettingsAPI;
use crate::api_data::data_conservation_settings_api::DataConservationSettingsAPI;
use crate::api_data::general_settings_api::GeneralSettingsAPI;
use crate::api_data::language_settings_api::LanguageSettingsAPI;
use crate::api_data::notification_settings_api::NotificationSettingsAPI;
use crate::api_data::res_create_custom_emoji::ResCreateCustomEmoji;
use crate::api_data::security_settings_api::SecuritySettingsAPI;
use crate::constants::accounts_info_policy::{MIN_FIRST_NAME_LENGTH, MIN_LAST_NAME_LENGTH};
use crate::constants::{conservation_policy, SUPPORTED_LANGUAGES};
use crate::data::base_request_handler::BaseRequestHandler;
use crate::data::general_settings::GeneralSettings;
use crate::data::http_request_handler::HttpRequestHandler;
use crate::data::lang_settings::LangSettings;
use crate::data::new_custom_emoji::NewCustomEmoji;
use crate::data::new_data_conservation_policy::NewDataConservationPolicy;
use crate::data::new_notifications_settings::NewNotificationsSettings;
use crate::data::security_settings::{SecurityQuestion, SecuritySettings};
use crate::data::user::{AccountImageVisibility, UserPageVisibility};
use crate::helpers::virtual_directory_helper::VirtualDirType;
use crate::helpers::{account_helper, custom_emojies_helper, user_helper};
use crate::routes::RequestResult;
use crate::utils::string_utils::remove_html_nodes;

/// Get the general settings of the user
pub async fn get_general(r: &mut HttpRequestHandler) -> RequestResult {
    let user = user_helper::find_user_by_id(r.user_id_ref()?)?;

    r.set_response(GeneralSettingsAPI::new(&user))
}

/// Set the general settings of the user
pub async fn set_general(r: &mut HttpRequestHandler) -> RequestResult {
    let page_status = match (r.post_bool("isPublic")?, r.post_bool("isOpen")?) {
        (true, true) => UserPageVisibility::OPEN,
        (true, false) => UserPageVisibility::PUBLIC,
        (_, _) => UserPageVisibility::PRIVATE,
    };

    let personal_website = r.post_url_opt("personnalWebsite", false)?;

    let virtual_directory = r.post_checked_virtual_directory_opt(
        "virtualDirectory",
        r.user_id_ref()?.id(),
        VirtualDirType::USER,
    )?;

    let new_settings = GeneralSettings {
        id: r.user_id()?,
        first_name: r.post_content("firstName", MIN_FIRST_NAME_LENGTH as usize, true)?,
        last_name: r.post_content("lastName", MIN_LAST_NAME_LENGTH as usize, true)?,
        page_status,
        block_comments: !r.post_bool("allowComments")?,
        allow_posts_from_friends: r.post_bool("allowPostsFromFriends")?,
        friends_list_public: r.post_bool("publicFriendsList")?,
        email_public: r.post_bool("email_public")?,
        personal_website,
        virtual_directory,
        allow_mails: r.post_bool("allow_comunic_mails")?,
        public_note: Some(r.post_content("publicNote", 0, false)?),
        location: r
            .post_string_optional("location")
            .map(|s| remove_html_nodes(&s)),
    };

    account_helper::set_general(&new_settings)?;

    r.success("Settings updated")
}

/// Check the availability of a virtual directory
pub async fn check_virtual_directory(r: &mut HttpRequestHandler) -> RequestResult {
    // We can do this check just by getting it, there is no need to store it in a variable
    r.post_checked_virtual_directory_opt("directory", r.user_id()?.id(), VirtualDirType::USER)?;

    r.success("The directory is available!")
}

/// Get the current language of the user
pub async fn get_language(r: &mut HttpRequestHandler) -> RequestResult {
    let user = user_helper::find_user_by_id(r.user_id_ref()?)?;

    r.set_response(LanguageSettingsAPI::new(&user))
}

/// Set the current language of the user
pub async fn set_language(r: &mut HttpRequestHandler) -> RequestResult {
    let lang = r.post_string("lang")?;

    if !SUPPORTED_LANGUAGES.contains(&lang.as_str()) {
        r.forbidden("Language not supported!".to_string())?;
    }

    let new_settings = LangSettings {
        id: r.user_id()?,
        lang,
    };

    account_helper::set_language_settings(&new_settings)?;

    r.success("Language settings updated.")
}

/// Get security settings
pub async fn get_security(r: &mut HttpRequestHandler) -> RequestResult {
    r.need_user_password("password")?;

    let user = user_helper::find_user_by_id(r.user_id_ref()?)?;

    r.set_response(SecuritySettingsAPI::new(&user))
}

/// Set security settings
pub async fn set_security(r: &mut HttpRequestHandler) -> RequestResult {
    r.need_user_password("password")?;

    let new_settings = SecuritySettings {
        id: r.user_id()?,
        question1: SecurityQuestion::new(
            &Option::Some(r.post_content("security_question_1", 0, false)?),
            &Option::Some(r.post_content("security_answer_1", 0, false)?),
        ),

        question2: SecurityQuestion::new(
            &Option::Some(r.post_content("security_question_2", 0, false)?),
            &Option::Some(r.post_content("security_answer_2", 0, false)?),
        ),
    };

    account_helper::set_security_settings(&new_settings)?;

    r.success("Security settings update.")
}

/// Check user password
pub async fn check_password(r: &mut HttpRequestHandler) -> RequestResult {
    r.need_user_password("password")?;

    r.success("The password is valid.")
}

/// Update user password
pub async fn update_password(r: &mut HttpRequestHandler) -> RequestResult {
    r.need_user_password("oldPassword")?;

    let new_password = r.post_string("newPassword")?;

    account_helper::change_password(r.user_id_ref()?, &new_password)?;

    r.success("Password updated !")
}

/// Get account image settings
pub async fn get_account_image_settings(r: &mut HttpRequestHandler) -> RequestResult {
    let user = user_helper::find_user_by_id(r.user_id_ref()?)?;

    r.set_response(AccountImageSettingsAPI::new(&user))
}

/// Upload a new account image
pub async fn upload_account_image(r: &mut HttpRequestHandler) -> RequestResult {
    if !r.has_file("picture") {
        return r.bad_request("An error occurred while receiving the image !".to_string());
    }

    let uri = r.save_post_image("picture", "avatars", 800, 800)?;

    account_helper::set_account_image(r.user_id_ref()?, &uri)?;

    r.success("Account image updated!")
}

/// Delete user account image
pub async fn delete_account_image(r: &mut HttpRequestHandler) -> RequestResult {
    account_helper::delete_account_image(r.user_id_ref()?)?;

    r.success("Account image deleted!")
}

/// Change account image visibility
pub async fn set_account_image_visibility(r: &mut HttpRequestHandler) -> RequestResult {
    let level = AccountImageVisibility::from_api(&r.post_string("visibility")?);

    account_helper::set_account_image_visibility(r.user_id_ref()?, level)?;

    r.success("Account image visibility level updated!")
}

/// Upload a custom emoji
pub async fn upload_custom_emoji(r: &mut HttpRequestHandler) -> RequestResult {
    let shortcut = r.post_emoji_shortcut("shortcut")?;

    if custom_emojies_helper::has_user_similar_shortcut(r.user_id_ref()?, &shortcut)? {
        r.forbidden("A custom emoji with the same shortcut is already defined!".to_string())?;
    }

    let path = r.save_post_image("image", "custom_emojies", 72, 72)?;

    let emoji_id = custom_emojies_helper::insert(&NewCustomEmoji {
        user_id: r.user_id()?,
        shortcut,
        path,
    })?;

    r.set_response(ResCreateCustomEmoji::new(emoji_id))
}

/// Delete custom emoji
pub async fn delete_custom_emoji(r: &mut HttpRequestHandler) -> RequestResult {
    let emoji = r.post_emoji_id("emojiID")?;

    custom_emojies_helper::delete(&emoji)?;

    r.success("Emoji deleted.")
}

/// Get data conservation policy
pub async fn get_data_conservation_policy(r: &mut HttpRequestHandler) -> RequestResult {
    let user = user_helper::find_user_by_id(r.user_id_ref()?)?;

    r.set_response(DataConservationSettingsAPI::new(&user))
}

/// Set data conservation policy
pub async fn set_data_conservation_policy(r: &mut HttpRequestHandler) -> RequestResult {
    r.need_user_password("password")?;

    let policy = NewDataConservationPolicy {
        user_id: r.user_id()?,
        delete_account_after: r.post_positive_u64_opt("inactive_account_lifetime")?,
        delete_notifications_after: r.post_positive_u64_opt("notification_lifetime")?,
        delete_comments_after: r.post_positive_u64_opt("comments_lifetime")?,
        delete_posts_after: r.post_positive_u64_opt("posts_lifetime")?,
        delete_conversation_messages_after: r
            .post_positive_u64_opt("conversation_messages_lifetime")?,
        delete_likes_after: r.post_positive_u64_opt("likes_lifetime")?,
    };

    // Check policy is respected
    if policy
        .delete_account_after
        .map(|f| f < conservation_policy::MIN_INACTIVE_ACCOUNT_LIFETIME.as_secs())
        == Some(true)
    {
        r.bad_request("Invalid lifetime for inactive account !".to_string())?;
    }

    if policy
        .delete_notifications_after
        .map(|f| f < conservation_policy::MIN_NOTIFICATIONS_LIFETIME.as_secs())
        == Some(true)
    {
        r.bad_request("Invalid lifetime for notifications !".to_string())?;
    }

    if policy
        .delete_comments_after
        .map(|f| f < conservation_policy::MIN_COMMENTS_LIFETIME.as_secs())
        == Some(true)
    {
        r.bad_request("Invalid lifetime for comments !".to_string())?;
    }

    if policy
        .delete_posts_after
        .map(|f| f < conservation_policy::MIN_POSTS_LIFETIME.as_secs())
        == Some(true)
    {
        r.bad_request("Invalid lifetime for posts !".to_string())?;
    }

    if policy
        .delete_conversation_messages_after
        .map(|f| f < conservation_policy::MIN_CONVERSATION_MESSAGES_LIFETIME.as_secs())
        == Some(true)
    {
        r.bad_request("Invalid lifetime for conversation messages !".to_string())?;
    }

    if policy
        .delete_likes_after
        .map(|f| f < conservation_policy::MIN_LIKES_LIFETIME.as_secs())
        == Some(true)
    {
        r.bad_request("Invalid lifetime for likes !".to_string())?;
    }

    account_helper::set_data_conservation_policy(policy)?;

    r.success("Successfully updated data conservation policy!")
}

/// Get notifications settings
pub async fn get_notifications(r: &mut HttpRequestHandler) -> RequestResult {
    let user = user_helper::find_user_by_id(r.user_id_ref()?)?;

    r.set_response(NotificationSettingsAPI::new(&user))
}

/// Set (update) notifications settings
pub async fn set_notifications(r: &mut HttpRequestHandler) -> RequestResult {
    account_helper::set_notifications_settings(NewNotificationsSettings {
        user_id: r.user_id()?,
        allow_notifications_sound: r.post_bool("allow_notifications_sound")?,
        allow_conversations: r.post_bool("allow_conversations")?,
    })?;

    r.ok()
}
