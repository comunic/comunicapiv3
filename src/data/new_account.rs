//! # New account
//!
//! @author Pierre Hubert

pub struct NewAccount {
    pub first_name: String,
    pub last_name: String,
    pub email: String,
    pub password: String,
}
