//! # Comunic Server Library
//!
//! This library contains all the logic of the Comunic Server.

#[macro_use]
extern crate lazy_static;

pub mod api_data;
pub mod controllers;
pub mod data;
pub mod helpers;
pub mod utils;

pub mod cleanup_thread;
pub mod constants;
pub mod routes;
pub mod server;
pub mod user_ws_routes;
