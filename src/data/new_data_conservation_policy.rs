//! # New data conservation policy settings
//!
//! @author Pierre Hubert

use crate::data::user::UserID;

pub struct NewDataConservationPolicy {
    pub user_id: UserID,
    pub delete_account_after: Option<u64>,
    pub delete_notifications_after: Option<u64>,
    pub delete_comments_after: Option<u64>,
    pub delete_posts_after: Option<u64>,
    pub delete_conversation_messages_after: Option<u64>,
    pub delete_likes_after: Option<u64>,
}
