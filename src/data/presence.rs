//! # Presence information
//!
//! @author Pierre Hubert

use crate::data::user::UserID;

pub struct Presence {
    pub id: u64,
    pub user_id: UserID,
    pub year: u32,
    pub month: u16,
    pub day: u16,
}

impl PartialEq for Presence {
    fn eq(&self, other: &Self) -> bool {
        self.user_id == other.user_id
            && self.year == other.year
            && self.month == other.month
            && self.day == other.day
    }
}
