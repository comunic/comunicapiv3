//! # Survey
//!
//! This structure contains all the information about a survey
//!
//! @author Pierre Hubert

use crate::data::user::UserID;

pub struct SurveyChoice {
    pub id: u64,
    pub name: String,
    pub count: u64,
}

pub struct Survey {
    pub id: u64,
    pub user_id: UserID,
    pub time_create: u64,
    pub post_id: u64,
    pub question: String,
    pub choices: Vec<SurveyChoice>,
    pub allow_new_choices: bool,
}
