//! # Search user result API entry
//!
//! @author Pierre Hubert

use crate::data::user::User;

#[derive(serde::Serialize)]
pub struct AdminSearchUserResult {
    id: u64,
    first_name: String,
    last_name: String,
    email: String,
    account_image: Option<String>,
}

impl AdminSearchUserResult {
    pub fn new(user: User) -> Self {
        Self {
            id: user.id.id(),
            account_image: user.account_image_url_for_admin(),
            first_name: user.first_name,
            last_name: user.last_name,
            email: user.email,
        }
    }
}
