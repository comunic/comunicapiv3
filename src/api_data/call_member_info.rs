//! # Call member information
//!
//! @author Pierre Hubert
use serde::Serialize;

use crate::data::user::UserID;
use crate::data::user_ws_connection::ActiveCall;

#[derive(Serialize)]
#[allow(non_snake_case)]
pub struct CallMemberInfo {
    userID: u64,
    ready: bool,
}

impl CallMemberInfo {
    pub fn new(user_id: &UserID, call: &ActiveCall) -> Self {
        Self {
            userID: user_id.id(),
            ready: call.ready,
        }
    }
}
