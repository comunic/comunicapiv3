//! # Friend API
//!
//! @author Pierre Hubert
use serde::Serialize;

use crate::api_data::legacy_api_bool::LegacyBool;
use crate::data::friend::Friend;

#[derive(Serialize)]
#[allow(non_snake_case)]
pub struct FriendAPI {
    ID_friend: u64,
    accepted: LegacyBool,
    time_last_activity: u64,
    following: LegacyBool,
    canPostTexts: bool,
}

impl FriendAPI {
    /// Turn a friend object into an API entry
    pub fn new(f: &Friend) -> FriendAPI {
        FriendAPI {
            ID_friend: f.friend_id.id(),
            accepted: LegacyBool(f.accepted),
            time_last_activity: f.computed_last_activity_time(),
            following: LegacyBool(f.following),
            canPostTexts: f.can_post_texts,
        }
    }

    /// Generate a new list of Friends
    pub fn from_list(l: &Vec<Friend>) -> Vec<FriendAPI> {
        l.iter().map(Self::new).collect()
    }
}
