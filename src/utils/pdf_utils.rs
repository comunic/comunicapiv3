//! # PDF utilities
//!
//! @author Pierre Hubert

use pdf::backend::Backend;
use pdf::file::FileOptions;

use crate::data::error::{ExecError, ResultBoxError};

/// Check out whether a PDF is a valid PDF or not, by trying to open it
pub fn is_valid_pdf(file: &actix_web::web::Bytes) -> ResultBoxError<bool> {
    let backend = file.to_vec();

    let pdf_file = FileOptions::cached().load(backend.as_slice())?;

    let start_offset = backend.locate_start_offset()?;

    let valid = match backend.read_xref_table_and_trailer(start_offset, &pdf_file.resolver()) {
        Ok((refs, _)) => {
            if refs.is_empty() {
                Err(ExecError::boxed_string(format!(
                    "Detected a PDF with 0 references (file size: {})!",
                    file.len()
                )))
            } else {
                Ok(true)
            }
        }

        Err(e) => {
            println!("Error while parsing PDF: {:?}", e);
            Ok(false)
        }
    };

    Ok(valid?)
}
