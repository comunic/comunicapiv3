//! # Group ID
//!
//! @author Pierre Hubert

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct GroupID(u64);

impl GroupID {
    /// Initialize a new group ID object
    pub fn new(id: u64) -> GroupID {
        GroupID(id)
    }

    /// Get the ID current stored in this structure
    pub fn id(&self) -> u64 {
        self.0
    }
}
