//! # MP utilities
//!
//! @author Pierre Hubert

/// Check out whether an MP4 file is valid or not
pub fn is_valid_mp4(file: &[u8]) -> bool {
    let cursor = std::io::Cursor::new(file);
    let reader = mp4::Mp4Reader::read_header(cursor, file.len() as u64);

    match reader {
        Ok(_) => true,
        Err(e) => {
            eprintln!("Failed to read MP4! {:#?}", e);
            false
        }
    }
}
