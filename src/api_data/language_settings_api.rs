//! # Language settings API
//!
//! @author Pierre Hubert

use serde::Serialize;

use crate::data::user::User;

#[derive(Serialize)]
#[allow(non_snake_case)]
pub struct LanguageSettingsAPI {
    lang: String,
}

impl LanguageSettingsAPI {
    pub fn new(user: &User) -> LanguageSettingsAPI {
        LanguageSettingsAPI {
            lang: user.lang.clone(),
        }
    }
}
