//! # Language settings
//!
//! @author Pierre Hubert

use crate::data::user::UserID;

pub struct LangSettings {
    pub id: UserID,
    pub lang: String,
}
