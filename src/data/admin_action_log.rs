//! # Admin action log

use crate::data::admin::AdminID;
use crate::data::user::UserID;

#[derive(serde::Serialize, serde::Deserialize, Clone)]
pub enum AdminAction {
    AuthWithResetToken,
    AuthWithAccessKey {
        key: String,
        key_id: u64,
    },
    RegisteredAdminKey {
        key_id: u64,
        key_name: String,
        target: AdminID,
    },
    DeletedAdminKey {
        key_id: u64,
        key_name: String,
        target: AdminID,
    },
    GeneratedAdminResetToken {
        target: AdminID,
    },
    CreatedAdmin {
        id: AdminID,
        name: String,
        email: String,
    },
    UpdatedAdminGeneralSettings {
        target: AdminID,
        new_email: String,
        new_name: String,
    },
    AddAdminRole {
        target: AdminID,
        role: String,
    },
    RemoveAdminRole {
        target: AdminID,
        role: String,
    },
    AccessUserPage {
        user_id: UserID,
        user_name: String,
    },
    ChangedEmailAddress {
        user_id: UserID,
        user_name: String,
        old_mail: String,
        new_mail: String,
    },
    CreatePasswordRecoveryLink {
        user_id: UserID,
        user_name: String,
    },
    DeleteUserAccount {
        user_id: UserID,
        user_name: String,
    },
    UnsupportedAction {
        raw_data: String,
    },
}

impl AdminAction {
    pub fn format_string(&self) -> &'static str {
        match self {
            AdminAction::AuthWithResetToken => { "Authenticated with a reset token" }

            AdminAction::AuthWithAccessKey { .. } =>
                { "Authenticated with key #{key_id} : \"{key}\"" }

            AdminAction::RegisteredAdminKey { .. } =>
                { "Registered a new key #{key_id} with name \'{key_name}\' for [admin]{target}[/admin]" }

            AdminAction::DeletedAdminKey { .. } =>
                { "Deleted key #{key_id} \'{key_name\' of [admin]{target}[/admin]" }

            AdminAction::GeneratedAdminResetToken { .. } =>
                { "Generated new admin access reset token for [admin]{target}[/admin]" }

            AdminAction::CreatedAdmin { .. } =>
                { "Created a new administrator [admin]#{id}[/admin] with name \'{name}\' and email \'email\'" }

            AdminAction::UpdatedAdminGeneralSettings { .. } =>
                { "Updated general admin settings of [admin]{target}[/admin]. New name: \'{new_name}\' / New email: \'{new_email}\'" }

            AdminAction::AddAdminRole { .. } =>
                { "Add a new role to [admin]{target}[/admin] => {role}" }

            AdminAction::RemoveAdminRole { .. } =>
                { "Removed a role from [admin]{target}[/admin] => {role}" }

            AdminAction::AccessUserPage { .. } =>
                { "Accessed information of user #{user_id} '{user_name}'." }

            AdminAction::ChangedEmailAddress { .. } =>
                { "Changed email address of user #{user_id} '{user_name}' from {old_mail} to {new_mail}" }

            AdminAction::CreatePasswordRecoveryLink { .. } =>
                { "Created a password recovery link for user #{user_id} '{user_name}'." }

            AdminAction::DeleteUserAccount { .. } =>
                { "Deleted user account #{user_id} of '{user_name}'." }

            AdminAction::UnsupportedAction { .. } => { "Unsupported action. Raw data: {raw_data}" }
        }
    }
}

pub struct AdminActionLog {
    pub id: u64,
    pub admin_id: AdminID,
    pub ip: String,
    pub time: u64,
    pub action: AdminAction,
}
