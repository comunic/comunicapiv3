/// API client information
///
/// @author Pierre HUBERT
#[derive(Debug)]
pub struct APIClient {
    pub id: u64,
    pub name: String,
    pub domain: Option<String>,
    pub comment: Option<String>,
    pub default_expiration_time: u64,
    pub firebase_project_name: Option<String>,
    pub firebase_service_account_file: Option<String>,
}

impl APIClient {
    pub fn is_firebase_available(&self) -> bool {
        self.firebase_project_name.is_some() && self.firebase_service_account_file.is_some()
    }
}
