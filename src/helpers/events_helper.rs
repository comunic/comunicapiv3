//! # Events helper
//!
//! @author Pierre Hubert

use async_recursion::async_recursion;

use crate::controllers::{
    calls_controller, comments_controller, conversations_controller, notifications_controller,
    rtc_relay_controller, user_ws_controller,
};
use crate::data::call_signal::{
    CloseCallStream, NewRtcRelayMessage, NewUserCallSignal, UserCallOfferRequest,
};
use crate::data::comment::Comment;
use crate::data::conversation::ConvID;
use crate::data::conversation_message::ConversationMessage;
use crate::data::error::Res;
use crate::data::user::UserID;
use crate::data::user_token::UserAccessToken;
use crate::data::user_ws_connection::UserWsConnection;
use crate::helpers::push_notifications_helper;

pub enum Event {
    /// Websocket of a user was closed
    ///
    /// This event is propagated BEFORE the removal of the connection from the list
    UserWsClosed(UserWsConnection),

    /// Destroyed a login token
    DestroyedLoginToken(UserAccessToken),

    /// Updated the number of notifications of one of multiple users user
    UpdatedNotificationsNumber(Vec<UserID>),

    /// Indicate that a user has seen the last message of a conversation
    SeenLastConversationMessage(UserID, ConvID),

    /// Updated the number of unread conversations
    UpdatedNumberUnreadConversations(Vec<UserID>),

    /// Created a new conversation message
    NewConversationMessage(ConversationMessage),

    /// Updated conversation message
    UpdatedConversationMessage(ConversationMessage),

    /// Deleted a conversation message
    DeleteConversationMessage(ConversationMessage),

    /// User is writing a message in a conversation
    UserIsWritingMessageInConversation(UserID, ConvID),

    /// Removed a user from a conversation
    RemovedUserFromConversation(UserID, ConvID),

    /// Delete a conversation
    DeletedConversation(ConvID),

    /// Created a new comment
    NewComment(Comment),

    /// Updated a comment
    UpdatedComment(Comment),

    /// Deleted a comment
    DeletedComment(Comment),

    /// Connection to RTC relay was closed
    ClosedRTCRelayWebSocket,

    /// User joined call
    UserJoinedCall(ConvID, UserID),

    /// User left call
    UserLeftCall(ConvID, UserID),

    /// Got a new user call signal
    NewUserCallSignal(NewUserCallSignal),

    /// Got a new RTC relay message
    NewRTCRelayMessage(NewRtcRelayMessage),

    /// User requested an offer for a call
    UserRequestedCallOffer(UserCallOfferRequest),

    /// Close call stream
    CloseCallStream(CloseCallStream),

    /// No event
    None,
}

/// Propagate an event through the different components of the application
#[async_recursion(?Send)]
pub async fn propagate_event(e: Event) -> Res {
    conversations_controller::handle_event(&e).await?;
    comments_controller::handle_event(&e).await?;
    notifications_controller::handle_event(&e)?;
    user_ws_controller::handle_event(&e)?;
    calls_controller::handle_event(&e).await?;
    rtc_relay_controller::handle_event(&e)?;
    push_notifications_helper::handle_event(&e).await?;
    Ok(())
}
