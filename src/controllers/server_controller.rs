use crate::api_data::server_config::ServerConfig;
use crate::data::base_request_handler::BaseRequestHandler;
use crate::data::http_request_handler::HttpRequestHandler;
use crate::routes::RequestResult;

/// Main server controller
///
/// @author Pierre Hubert

/// Root server index
pub async fn main_index(request: &mut HttpRequestHandler) -> RequestResult {
    request.success("Comunic API server V3. (c) Pierre Hubert 2020")
}

/// Get server configuration
pub async fn get_config(request: &mut HttpRequestHandler) -> RequestResult {
    request.set_response(ServerConfig::new(request.api_client()))
}
