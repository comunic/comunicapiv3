//! Find private conversations result
//!
//! @author Pierre Hubert

use serde::Serialize;

use crate::data::conversation::ConvID;

#[derive(Serialize)]
#[allow(non_snake_case)]
pub struct ResFindPrivateConversations {
    conversationsID: Vec<u64>,
}

impl ResFindPrivateConversations {
    /// Construct a new instance of this structure
    pub fn new(list: Vec<ConvID>) -> ResFindPrivateConversations {
        ResFindPrivateConversations {
            conversationsID: list.iter().map(|i| i.id()).collect(),
        }
    }
}
