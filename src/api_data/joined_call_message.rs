//! # Joined call message
//!
//! @author Pierre Hubert

use serde::Serialize;

use crate::data::conversation::ConvID;
use crate::data::user::UserID;

/// This structure is used to give information about a user who joined a call
#[derive(Serialize)]
#[allow(non_snake_case)]
pub struct JoinedCallMessage {
    callID: u64,
    userID: u64,
}

impl JoinedCallMessage {
    pub fn new(call_id: &ConvID, user_id: &UserID) -> Self {
        Self {
            callID: call_id.id(),
            userID: user_id.id(),
        }
    }
}
