//! # Network utilities
//!
//! @author Pierre Hubert

/// Check whether an IP address matches a given pattern. Pattern can be either:
/// * An IP address
/// * An IP mask ending with a star (*)
///
/// ```
/// use comunic_server::utils::network_utils::match_ip;
///
/// assert!(match_ip("127.0.0.1", "127.0.0.1"));
/// assert!(!match_ip("127.0.0.1", "127.0.0.2"));
/// assert!(match_ip("127.0.0.*", "127.0.0.2"));
/// assert!(!match_ip("127.0.0.*", "187.0.0.2"));
/// ```
///
pub fn match_ip(pattern: &str, ip: &str) -> bool {
    if pattern.eq(ip) {
        return true;
    }

    if pattern.ends_with('*') && ip.starts_with(&pattern.replace('*', "")) {
        return true;
    }

    false
}
