use crate::constants::database_tables_names::REPORTS_TABLE;
use crate::data::error::Res;
use crate::data::report::{Report, ReportID, ReportTarget};
use crate::data::user::UserID;
use crate::helpers::database;

/// Check if a report has already been saved by the same user on the same resource
pub fn already_exists(r: &Report) -> Res<bool> {
    let (target_type, target_id) = r.target.to_db();

    database::QueryInfo::new(REPORTS_TABLE)
        .cond_user_id("user_id", &r.user_id)
        .cond("target_type", target_type)
        .cond_u64("target_id", target_id)
        .exec_count_has_at_least_one_result()
}

/// Save a new report in the database
pub fn save_report(r: Report) -> Res<ReportID> {
    let (target_type, target_id) = r.target.to_db();

    database::InsertQuery::new(REPORTS_TABLE)
        .add_user_id("user_id", &r.user_id)
        .add_str("target_type", target_type)
        .add_u64("target_id", target_id)
        .add_u64("time", r.time)
        .add_str("cause", r.cause.id().id())
        .add_opt_str("comment", r.comment.as_ref())
        .insert_expect_result()
        .map(|i| ReportID(i))
}

/// Delete all the reports created by a user
pub fn delete_all_from_user(user: &UserID) -> Res {
    database::DeleteQuery::new(REPORTS_TABLE)
        .cond_user_id("user_id", user)
        .exec()
}

/// Delete all the reports targeting a specific resource
pub fn delete_all_for_target(target: ReportTarget) -> Res {
    let (target_type, target_id) = target.to_db();
    database::DeleteQuery::new(REPORTS_TABLE)
        .cond_str("target_type", target_type)
        .cond_u64("target_id", target_id)
        .exec()
}
