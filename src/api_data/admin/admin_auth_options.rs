//! # Admin auth options API structure
//!
//! @author Pierre Hubert

use serde::Serialize;

use crate::data::admin::{Admin, AdminKey};

#[derive(Serialize)]
struct AuthKey {
    name: String,
    id: u64,
    password: bool,
}

#[derive(Serialize)]
pub struct AdminAuthOptions {
    reset_token: bool,
    keys: Vec<AuthKey>,
}

impl AdminAuthOptions {
    pub fn new(admin: &Admin, keys: &[AdminKey]) -> Self {
        Self {
            reset_token: admin.reset_token.is_some(),
            keys: keys
                .iter()
                .map(|k| AuthKey {
                    id: k.id,
                    name: k.name.to_string(),
                    password: k.password.is_some(),
                })
                .collect(),
        }
    }
}
