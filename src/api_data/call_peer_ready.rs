//! # Call peer ready API
//!
//! @author Pierre Hubert
use serde::Serialize;

use crate::data::conversation::ConvID;
use crate::data::user::UserID;

#[derive(Serialize)]
#[allow(non_snake_case)]
pub struct CallPeerReadyAPI {
    callID: u64,
    peerID: u64,
}

impl CallPeerReadyAPI {
    pub fn new(call_id: &ConvID, user_id: &UserID) -> Self {
        Self {
            callID: call_id.id(),
            peerID: user_id.id(),
        }
    }
}
