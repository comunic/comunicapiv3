//! # Advanced group information
//!
//! @author Pierre Hubert
use serde::Serialize;

use crate::api_data::conversation_api::ConversationAPI;
use crate::api_data::group_api::GroupApi;
use crate::data::config::conf;
use crate::data::error::ResultBoxError;
use crate::data::group::Group;
use crate::data::user::UserID;
use crate::helpers::likes_helper::LikeType;
use crate::helpers::{conversations_helper, groups_helper, likes_helper};

#[derive(Serialize)]
pub struct AdvancedGroupApi {
    #[serde(flatten)]
    base_info: GroupApi,

    is_members_list_public: bool,
    time_create: u64,
    description: String,
    url: String,
    number_likes: u64,
    is_liking: bool,
    conversations: Vec<ConversationAPI>,
    is_forez_group: bool,
}

impl AdvancedGroupApi {
    /// Construct a new advanced group membership instance
    pub fn new(g: &Group, user_id: Option<UserID>) -> ResultBoxError<AdvancedGroupApi> {
        let membership = groups_helper::get_membership(&g.id, user_id.clone())?;

        let conversations = conversations_helper::get_list_group(&g.id)?
            .into_iter()
            .filter(|p| p.min_group_membership_level.as_ref().unwrap() >= &membership.level)
            .collect();

        Ok(AdvancedGroupApi {
            base_info: GroupApi::new_with_level(g, &membership)?,
            is_members_list_public: g.is_members_list_public,
            time_create: g.time_create,
            description: g.description.clone().unwrap_or("null".to_string()),
            url: g.url.clone().unwrap_or("null".to_string()),
            number_likes: likes_helper::count(g.id.id(), LikeType::GROUP)? as u64,
            is_liking: likes_helper::is_liking(
                &user_id.unwrap_or(UserID::invalid()),
                g.id.id(),
                LikeType::GROUP,
            )?,
            conversations: ConversationAPI::for_list(&conversations),
            is_forez_group: conf().forez_groups.contains(&g.id),
        })
    }
}
