//! # API Notification object
//!
//! @author Pierre Hubert

use serde::Serialize;

use crate::api_data::type_container_api::TypeContainerAPI;
use crate::data::notification::Notification;

#[derive(Serialize)]
pub struct NotificationAPI {
    id: u64,
    time_create: u64,
    seen: bool,
    from_user_id: u64,
    dest_user_id: u64,
    on_elem_id: u64,
    on_elem_type: String,
    #[serde(flatten)]
    type_container: TypeContainerAPI,
    event_visibility: String,
    from_container_id: Option<u64>,
    from_container_type: Option<String>,
}

impl NotificationAPI {
    pub fn new(n: &Notification) -> NotificationAPI {
        NotificationAPI {
            id: n.id,
            time_create: n.time_create,
            seen: n.seen,
            from_user_id: n.from_user_id.id(),
            dest_user_id: n.dest_user_id.id(),
            on_elem_id: n.on_elem_id,
            on_elem_type: n.on_elem_type.to_api(),
            type_container: TypeContainerAPI::new(n.kind.to_api()),
            event_visibility: n.visibility.to_api(),
            from_container_id: n.container_id,
            from_container_type: n.container_type.as_ref().map(|f| f.to_api()),
        }
    }

    pub fn for_list(l: &[Notification]) -> Vec<NotificationAPI> {
        l.iter().map(Self::new).collect()
    }
}
