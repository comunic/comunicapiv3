//! # Admin logs management
//!
//! @author Pierre Hubert

use crate::constants::conservation_policy::ADMIN_ACTIONS_LOG_LIFETIME;
use crate::constants::database_tables_names::ADMIN_LOGS_TABLE;
use crate::data::admin::AdminID;
use crate::data::admin_action_log::{AdminAction, AdminActionLog};
use crate::data::error::Res;
use crate::helpers::database;
use crate::utils::date_utils::time;

/// Record an administrator action
pub fn log_admin_action(admin: AdminID, ip: &str, action: AdminAction) -> Res {
    database::InsertQuery::new(ADMIN_LOGS_TABLE)
        .add_admin_id("admin_id", admin)
        .add_str("ip", ip)
        .add_u64("time", time())
        .add_str("action", &serde_json::to_string(&action)?)
        .insert_drop_result()
}

/// Clean old admin logs
pub fn clean_old_logs() -> Res {
    database::DeleteQuery::new(ADMIN_LOGS_TABLE)
        .set_custom_where("time < ?")
        .add_custom_where_arg_u64(time() - ADMIN_ACTIONS_LOG_LIFETIME.as_secs())
        .exec()
}

/// Get all administrators action log history
pub fn get_all_admin_logs() -> Res<Vec<AdminActionLog>> {
    database::QueryInfo::new(ADMIN_LOGS_TABLE).exec(db_to_log)
}

/// Get a specific administrator action log history
pub fn get_admin_logs(id: AdminID) -> Res<Vec<AdminActionLog>> {
    database::QueryInfo::new(ADMIN_LOGS_TABLE)
        .cond_admin_id("admin_id", id)
        .exec(db_to_log)
}

fn db_to_log(row: &database::RowResult) -> Res<AdminActionLog> {
    Ok(AdminActionLog {
        id: row.get_u64("id")?,
        admin_id: row.get_admin_id("admin_id")?,
        ip: row.get_str("ip")?,
        time: row.get_u64("time")?,
        action: serde_json::from_str(&row.get_str("action")?).unwrap_or(
            AdminAction::UnsupportedAction {
                raw_data: row.get_str("action").unwrap(),
            },
        ),
    })
}
