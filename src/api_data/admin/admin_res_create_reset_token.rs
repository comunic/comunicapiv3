//! # Result of creation of a reset token
//!
//! @author Pierre Hubert

use crate::data::admin::AdminResetToken;
use serde::Serialize;

#[derive(Serialize)]
pub struct AdminResCreateResetToken {
    token: String,
    expire: u64,
}

impl AdminResCreateResetToken {
    pub fn new(token: AdminResetToken) -> Self {
        Self {
            token: token.token,
            expire: token.expire,
        }
    }
}
