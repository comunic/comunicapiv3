//! # Get WebSocket token result
//!
//! @author Pierre Hubert
use serde::Serialize;

#[derive(Serialize)]
pub struct ResGetWsToken {
    token: String,
}

impl ResGetWsToken {
    pub fn new(token: String) -> Self {
        ResGetWsToken { token }
    }
}
