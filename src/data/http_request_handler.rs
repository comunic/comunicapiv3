use actix_http::header::{HeaderName, HeaderValue};
use actix_remote_ip::RemoteIP;
use std::collections::HashMap;
use std::str::FromStr;

use actix_web::http::StatusCode;
use actix_web::{HttpRequest, HttpResponse, HttpResponseBuilder};
use serde::Serialize;

use crate::api_data::http_error::HttpError;
use crate::data::admin::{AdminAccessToken, AdminID};
use crate::data::api_client::APIClient;
use crate::data::base_request_handler::{BaseRequestHandler, RequestValue};
use crate::data::config::conf;
use crate::data::error::{Res, ResultBoxError};
use crate::data::user_token::UserAccessToken;
use crate::helpers::{account_helper, admin_access_token_helper, api_helper};
use crate::routes::RequestResult;

/// Http request handler
///
/// @author Pierre Hubert

pub struct HttpRequestHandler {
    request: HttpRequest,
    body: HashMap<String, RequestValue>,
    response: Option<HttpResponse>,
    headers: HashMap<String, String>,
    client: Option<APIClient>,
    curr_user_token: Option<UserAccessToken>,
    curr_admin_token: Option<AdminAccessToken>,
    remote_ip: RemoteIP,
}

impl HttpRequestHandler {
    /// Construct a new request handler
    pub fn new(
        req: HttpRequest,
        body: HashMap<String, RequestValue>,
        remote_ip: RemoteIP,
    ) -> HttpRequestHandler {
        HttpRequestHandler {
            request: req,
            body,
            response: None,
            headers: HashMap::new(),
            client: None,
            curr_user_token: None,
            curr_admin_token: None,
            remote_ip,
        }
    }

    /// Check if a response has been set for this request
    pub fn has_response(&self) -> bool {
        self.response.is_some()
    }

    /// Get the response status code, eg. 200 or 404
    pub fn response_status_code(&self) -> u16 {
        self.response.as_ref().unwrap().status().as_u16()
    }

    /// Take the response from this struct
    pub fn response(self) -> ResultBoxError<HttpResponse> {
        let mut response = self.response.unwrap();

        // Put additional headers if required
        for (k, v) in &self.headers {
            response
                .headers_mut()
                .insert(HeaderName::from_str(k)?, HeaderValue::from_str(v)?);
        }

        Ok(response)
    }

    /// Get the path of the request
    pub fn request_path(&self) -> String {
        self.request.path().to_string()
    }

    /// Get information about the client which made the request
    pub fn api_client(&self) -> &APIClient {
        self.client.as_ref().unwrap()
    }

    /// Check API client tokens
    pub fn check_client_token(&mut self) -> RequestResult {
        let client_name = self.post_string("client")?;

        let client = self.ok_or_bad_request(
            api_helper::get_client(&client_name),
            "Client not recognized!",
        )?;

        if let Some(allowed_origin) = &client.domain {
            match self.request.headers().get("Referer") {
                None => self.bad_request("Unknown origin!".to_string())?,
                Some(s) => {
                    if !s.to_str()?.starts_with(allowed_origin) {
                        self.bad_request(
                            "Use of this client is prohibited from this domain!".to_string(),
                        )?;
                    }
                }
            }

            self.headers.insert(
                "Access-Control-Allow-Origin".to_string(),
                allowed_origin.to_string(),
            );
        }

        self.client = Some(client);

        Ok(())
    }

    /// Check login token
    pub fn check_user_token(&mut self) -> Res {
        let token = self.post_string("token")?;

        // Find user
        match account_helper::find_user_by_login_token(&token, self.api_client()) {
            Ok(token) => {
                if token.need_refresh() {
                    account_helper::refresh_access_token(&token)?;
                }

                self.curr_user_token = Some(token);

                Ok(())
            }
            Err(e) => {
                println!("Error marking login tokens as invalid: {}", e);
                self.response = Some(
                    actix_web::HttpResponse::build(actix_web::http::StatusCode::from_u16(412)?)
                        .json(HttpError::new(412, "Please check your login tokens!")),
                );
                Err(e)
            }
        }
    }

    /// Check admin origin
    pub fn check_admin_origin(&mut self) -> Res {
        if let Some(header) = self.request.headers().get("Origin") {
            if header.to_str()?.eq(&conf().admin_url) {
                self.headers.insert(
                    "Access-Control-Allow-Origin".to_string(),
                    conf().admin_url.to_string(),
                );

                return Ok(());
            }
        }

        self.bad_request("Invalid origin for admin requests!".to_string())
    }

    /// Check admin access token
    pub fn check_admin_access_token(&mut self) -> Res {
        let token = self.post_string("token")?;

        self.curr_admin_token = Some(admin_access_token_helper::find_by_token(&token)?);

        Ok(())
    }
}

impl BaseRequestHandler for HttpRequestHandler {
    /// Get request parameter
    fn post_parameter_opt(&self, name: &str) -> Option<&RequestValue> {
        self.body.get(name)
    }

    /// Set request response
    fn set_response<T: Serialize>(&mut self, data: T) -> RequestResult {
        self.response = Some(HttpResponse::Ok().json(data));
        Ok(())
    }

    /// Set request error
    fn set_error(&mut self, error: HttpError) {
        self.response = Some(
            HttpResponseBuilder::new(StatusCode::from_u16(error.error.code).unwrap()).json(error),
        );
    }

    /// Get the remote IP address
    fn remote_ip(&self) -> String {
        self.remote_ip.0.to_string()
    }

    fn user_access_token(&self) -> Option<&UserAccessToken> {
        self.curr_user_token.as_ref()
    }

    fn admin_id_opt(&self) -> Option<AdminID> {
        self.curr_admin_token.as_ref().map(|p| p.id)
    }
}
