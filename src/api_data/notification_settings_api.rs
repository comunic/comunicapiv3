//! # Notification settings API
//!
//! @author Pierre Hubert

use serde::Serialize;

use crate::data::user::User;

#[derive(Serialize)]
pub struct NotificationSettingsAPI {
    allow_conversations: bool,
    allow_notifications_sound: bool,
}

impl NotificationSettingsAPI {
    pub fn new(user: &User) -> Self {
        Self {
            allow_conversations: user.allow_notif_conv,
            allow_notifications_sound: user.allow_notif_sound,
        }
    }
}
