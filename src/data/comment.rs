//! # Comment information
//!
//! @author Pierre Hubert

use crate::data::user::UserID;

pub type CommentID = u64;

#[derive(Debug, Clone)]
pub struct Comment {
    pub id: CommentID,
    pub time_sent: u64,
    pub user_id: UserID,
    pub post_id: u64,
    pub content: String,
    pub image_path: Option<String>,
}
