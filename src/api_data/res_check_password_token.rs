//! # Successful validation of password reset token
//!
//! @author Pierre Hubert

use serde::Serialize;

use crate::data::user::User;

#[derive(Serialize)]
pub struct ResCheckPasswordToken {
    first_name: String,
    last_name: String,
    mail: String,
}

impl ResCheckPasswordToken {
    pub fn new(user: &User) -> Self {
        Self {
            first_name: user.first_name.to_string(),
            last_name: user.last_name.to_string(),
            mail: user.email.to_string(),
        }
    }
}
