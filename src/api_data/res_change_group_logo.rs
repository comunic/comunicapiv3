//! # Change group logo result
//!
//! @author Pierre Hubert
use serde::Serialize;

use crate::utils::user_data_utils::user_data_url;

#[derive(Serialize)]
pub struct ResChangeGroupLogo {
    success: String,
    url: String,
}

impl ResChangeGroupLogo {
    /// Construct a new instance of this structure
    pub fn new(path: &str) -> ResChangeGroupLogo {
        ResChangeGroupLogo {
            success: "Group logo has been successfully updated!".to_string(),
            url: user_data_url(path),
        }
    }
}
