//! # Administrator account creation result
//!
//! @author Pierre Hubert

use serde::Serialize;

use crate::data::admin::AdminID;

#[derive(Serialize)]
pub struct AdminResCreateAccount {
    id: u64,
}

impl AdminResCreateAccount {
    pub fn new(id: AdminID) -> Self {
        Self { id: id.id() }
    }
}
