//! # Friendship status API
//!
//! @author Pierre Hubert

use serde::Serialize;

use crate::data::friendship_status::FriendshipStatus;

#[derive(Serialize)]
pub struct FriendshipStatusAPI {
    are_friend: bool,
    sent_request: bool,
    received_request: bool,
    following: bool,
}

impl FriendshipStatusAPI {
    /// Create a new instance
    pub fn new(status: &FriendshipStatus) -> FriendshipStatusAPI {
        FriendshipStatusAPI {
            are_friend: status.are_friend,
            sent_request: status.sent_request,
            received_request: status.received_request,
            following: status.following,
        }
    }
}
