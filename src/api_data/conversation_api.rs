//! # Conversation API object
//!
//! @author Pierre Hubert
use serde::Serialize;

use crate::controllers::calls_controller;
use crate::data::conversation::{Conversation, ConversationMember};
use crate::helpers::calls_helper;
use crate::utils::user_data_utils::user_data_url;

#[derive(Serialize)]
struct ConversationMembersAPI {
    user_id: u64,
    last_message_seen: u64,
    last_access: u64,
    following: bool,
    is_admin: bool,
}

#[derive(Serialize)]
pub struct ConversationAPI {
    id: u64,
    last_activity: u64,
    name: Option<String>,
    color: Option<String>,
    logo: Option<String>,
    group_id: Option<u64>,
    group_min_membership_level: Option<String>,
    members: Vec<ConversationMembersAPI>,
    can_everyone_add_members: bool,
    can_have_call: bool,
    can_have_video_call: bool,
    has_call_now: bool,
}

impl ConversationMembersAPI {
    pub fn new(m: &ConversationMember) -> Self {
        Self {
            user_id: m.user_id.id(),
            last_message_seen: m.last_message_seen,
            following: m.following,
            is_admin: m.is_admin,
            last_access: m.last_access,
        }
    }
}

impl ConversationAPI {
    /// Construct a new Conversation instance
    pub fn new(conv: &Conversation) -> ConversationAPI {
        ConversationAPI {
            id: conv.id.id(),
            last_activity: conv.last_activity,
            name: conv.name.clone(),
            members: conv
                .members
                .iter()
                .map(ConversationMembersAPI::new)
                .collect(),
            can_everyone_add_members: conv.can_everyone_add_members,
            color: conv.color.clone(),
            logo: conv.logo.as_ref().map(|s| user_data_url(s)),

            group_id: conv.group_id.as_ref().map(|i| i.id()),
            group_min_membership_level: conv
                .min_group_membership_level
                .as_ref()
                .map(|f| f.to_api()),

            can_have_call: calls_helper::can_have_call(conv),
            can_have_video_call: calls_helper::can_have_video_calls(conv),
            has_call_now: calls_controller::is_conversation_having_call(&conv.id),
        }
    }

    pub fn for_list(l: &Vec<Conversation>) -> Vec<Self> {
        l.iter().map(Self::new).collect()
    }
}
