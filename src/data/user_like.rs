//! User like
//!
//! @author Pierre Hubert

use crate::data::user::UserID;

pub struct UserLike {
    pub id: u64,
    pub user_id: UserID,
    pub time_sent: u64,
    pub elem_type: String,
    pub elem_id: u64,
}
