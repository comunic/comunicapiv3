//! # Comunic administrator
//!
//! @author Pierre Hubert

use mysql::serde::{Deserializer, Serializer};
use serde::{Deserialize, Serialize};
use webauthn_rs::prelude::Passkey;

use crate::constants::admin::{AdminRole, ADMIN_ROLES_LIST};
use crate::data::u64_visitor::U64Visitor;

#[derive(Copy, Clone, Eq, PartialEq, Debug, Hash)]
pub struct AdminID(u64);

impl AdminID {
    pub fn new(id: u64) -> Self {
        Self(id)
    }

    pub fn id(&self) -> u64 {
        self.0
    }

    pub fn id_str(&self) -> String {
        format!("{}", self.0)
    }
}

pub struct NewAdmin {
    pub name: String,
    pub email: String,
}

pub struct AdminResetToken {
    pub token: String,
    pub expire: u64,
}

pub struct Admin {
    pub id: AdminID,
    pub time_create: u64,
    pub name: String,
    pub email: String,
    pub reset_token: Option<AdminResetToken>,
    pub roles: Vec<AdminRole>,
}

pub struct AdminKey {
    pub id: u64,
    pub admin_id: AdminID,
    pub name: String,
    pub time_add: u64,
    pub key: Passkey,
    pub password: Option<String>,
}

/// Admin access token
///
/// Used to store authentication of an admin
#[derive(Clone, Debug)]
pub struct AdminAccessToken {
    pub token: String,
    pub id: AdminID,
    pub last_refresh: u64,
}

/// New admin general settings
pub struct NewAdminGeneralSettings {
    pub id: AdminID,
    pub name: String,
    pub email: String,
}

impl AdminRole {
    pub fn from_id(id: &str) -> Option<Self> {
        ADMIN_ROLES_LIST
            .iter()
            .filter(|r| r.id.eq(id))
            .map(|r| r.role)
            .next()
    }

    pub fn to_id(&self) -> &'static str {
        ADMIN_ROLES_LIST
            .iter()
            .filter(|r| r.role.eq(self))
            .map(|r| r.id)
            .next()
            .expect("Should have found a role!!!")
    }
}

impl Serialize for AdminID {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
    where
        S: Serializer,
    {
        serializer.serialize_u64(self.0)
    }
}

impl<'de> Deserialize<'de> for AdminID {
    fn deserialize<D>(deserializer: D) -> Result<Self, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer
            .deserialize_u64(U64Visitor {})
            .map(AdminID::new)
    }
}
