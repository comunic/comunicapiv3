//! # Conversation message
//!
//! Information about a single conversation message

use std::collections::HashSet;

use crate::data::conversation::ConvID;
use crate::data::error::{ExecError, Res};
use crate::data::user::UserID;

pub type ConvMessageID = u64;

#[derive(Debug, Clone)]
pub struct ConversationMessageFile {
    pub path: String,
    pub size: u64,
    pub name: String,
    pub thumbnail: Option<String>,
    pub r#type: String,
}

#[derive(Debug, Clone)]
pub struct UserAddedAnotherUserToConversation {
    pub user_who_added: UserID,
    pub user_added: UserID,
}

#[derive(Debug, Clone)]
pub struct UserRemovedAnotherUserToConversation {
    pub user_who_removed: UserID,
    pub user_removed: UserID,
}

#[derive(Debug, Clone)]
pub enum ConversationServerMessageType {
    UserCreatedConversation(UserID),
    UserAddedAnotherUserToConversation(UserAddedAnotherUserToConversation),
    UserLeftConversation(UserID),
    UserRemovedFromConversation(UserRemovedAnotherUserToConversation),
}

impl ConversationServerMessageType {
    pub fn to_db(&self) -> String {
        let info = match self {
            ConversationServerMessageType::UserCreatedConversation(u) => {
                ("user_created_conv", Some(u.clone()), None)
            }
            ConversationServerMessageType::UserAddedAnotherUserToConversation(msg) => (
                "user_added_another_user",
                Some(msg.user_who_added.clone()),
                Some(msg.user_added.clone()),
            ),
            ConversationServerMessageType::UserLeftConversation(u) => {
                ("user_left", Some(u.clone()), None)
            }
            ConversationServerMessageType::UserRemovedFromConversation(msg) => (
                "user_removed",
                Some(msg.user_who_removed.clone()),
                Some(msg.user_removed.clone()),
            ),
        };

        format!(
            "{}-{}-{}",
            info.0,
            info.1.map(|u| u.id()).unwrap_or(0),
            info.2.map(|u| u.id()).unwrap_or(0)
        )
    }

    pub fn from_db(str: &str) -> Res<Self> {
        let split: Vec<&str> = str.split("-").collect();

        if split.len() != 3 {
            return Err(ExecError::boxed_new("Invalid ConversationSerMessageType"));
        }

        let id_1 = split[1].parse::<u64>()?;
        let id_2 = split[2].parse::<u64>()?;
        let info = (
            split[0],
            match id_1 {
                0 => None,
                id => Some(UserID::new(id)),
            },
            match id_2 {
                0 => None,
                id => Some(UserID::new(id)),
            },
        );

        match info {
            ("user_created_conv", Some(user_id), _) => Ok(Self::UserCreatedConversation(user_id)),

            ("user_added_another_user", Some(user_id), Some(user_2)) => Ok(
                Self::UserAddedAnotherUserToConversation(UserAddedAnotherUserToConversation {
                    user_who_added: user_id,
                    user_added: user_2,
                }),
            ),

            ("user_left", Some(user_id), _) => Ok(Self::UserLeftConversation(user_id)),

            ("user_removed", Some(user_id), Some(user_2)) => Ok(Self::UserRemovedFromConversation(
                UserRemovedAnotherUserToConversation {
                    user_who_removed: user_id,
                    user_removed: user_2,
                },
            )),

            _ => Err(ExecError::boxed_new("Unknown server message type!")),
        }
    }
}

/// Information about a single conversation message
#[derive(Debug, Clone)]
pub struct ConversationMessage {
    pub id: ConvMessageID,
    pub time_sent: u64,
    pub conv_id: ConvID,
    pub user_id: Option<UserID>,
    pub message: Option<String>,
    pub server_message: Option<ConversationServerMessageType>,
    pub file: Option<ConversationMessageFile>,
}

impl ConversationMessage {
    /// Get the entire list of referenced users in a conversation message
    pub fn referenced_users_id(&self) -> HashSet<UserID> {
        let mut users = HashSet::new();

        if let Some(user_id) = &self.user_id {
            users.insert(user_id.clone());
        }

        if let Some(srv_msg) = &self.server_message {
            match srv_msg {
                ConversationServerMessageType::UserCreatedConversation(user) => {
                    users.insert(user.clone());
                }
                ConversationServerMessageType::UserAddedAnotherUserToConversation(msg) => {
                    users.insert(msg.user_who_added.clone());
                    users.insert(msg.user_added.clone());
                }

                ConversationServerMessageType::UserLeftConversation(user) => {
                    users.insert(user.clone());
                }
                ConversationServerMessageType::UserRemovedFromConversation(msg) => {
                    users.insert(msg.user_who_removed.clone());
                    users.insert(msg.user_removed.clone());
                }
            }
        }

        users
    }
}
