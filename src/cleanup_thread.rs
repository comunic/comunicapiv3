//! # Cleanup thread
//!
//! This module manage a thread that automatically delete old data
//!
//! @author Pierre Hubert

use crate::constants::{CLEAN_UP_INTERVAL, INITIAL_REFRESH_LOAD_INTERVAL};
use crate::data::error::Res;
use crate::helpers::*;

/// Start the maintenance thread
pub fn start() -> Res {
    std::thread::spawn(clean_up_thread_handler);
    Ok(())
}

/// Clean up thread handler
fn clean_up_thread_handler() {
    // Let the server start before doing cleanup
    std::thread::sleep(INITIAL_REFRESH_LOAD_INTERVAL);

    loop {
        println!("Start clean up operation");
        match tokio::runtime::Runtime::new().unwrap().block_on(do_clean()) {
            Ok(_) => {
                println!("Clean thread executed successfully!");
            }

            Err(e) => {
                eprintln!("Failed to clean data! {:#?}", e);
            }
        }

        std::thread::sleep(CLEAN_UP_INTERVAL);
    }
}

/// Do the cleanup
async fn do_clean() -> Res {
    // Clean old login tokens
    account_helper::clean_up_old_access_tokens().await?;

    // Automatic account cleanup
    for user in user_helper::get_all_users()? {
        // Clean old likes
        likes_helper::clean_old_user_likes(&user)?;

        // Clean old notifications
        notifications_helper::clean_old_user_notifications(&user)?;

        // Clean old comments
        comments_helper::clean_old_comments(&user).await?;

        // Clean old posts
        posts_helper::clean_old_posts(&user).await?;

        // Clean old conversation messages
        conversations_helper::clean_old_messages(&user).await?;

        // Remove the account, if it have been inactive for a long time
        account_helper::remove_if_inactive_for_too_long_time(&user).await?;
    }

    // Clean up old admin actions
    admin_log_helper::clean_old_logs()?;

    Ok(())
}
