//! # Removed user from conversation message
//!
//! @author Pierre Hubert

use serde::Serialize;

use crate::data::conversation::ConvID;
use crate::data::user::UserID;

#[derive(Serialize)]
pub struct RemovedUserFromConversationMessage {
    user_id: u64,
    conv_id: u64,
}

impl RemovedUserFromConversationMessage {
    pub fn new(user_id: &UserID, conv_id: ConvID) -> Self {
        Self {
            user_id: user_id.id(),
            conv_id: conv_id.id(),
        }
    }
}
