use serde::Serialize;

/// HTTP request error
///
/// @author Pierre Hubert
#[derive(Serialize)]
pub struct InnerHTTPError {
    pub code: u16,
    pub message: String,
}

#[derive(Serialize)]
pub struct HttpError {
    pub error: InnerHTTPError,
}

impl HttpError {
    /// Construct a new custom error
    pub fn new(code: u16, message: &str) -> HttpError {
        HttpError {
            error: InnerHTTPError {
                code,
                message: message.to_string(),
            },
        }
    }

    /// Generate a 404 error
    pub fn not_found(message: &str) -> HttpError {
        HttpError::new(404, message)
    }

    /// Generate a 500 error
    pub fn internal_error(message: &str) -> HttpError {
        HttpError::new(500, message)
    }

    /// Generate a 400 error
    pub fn bad_request(message: &str) -> HttpError {
        HttpError::new(400, message)
    }

    /// Generate a 401 error
    pub fn forbidden(message: &str) -> HttpError {
        HttpError::new(401, message)
    }
}
