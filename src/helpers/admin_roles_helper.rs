//! # Admin roles helper
//!
//! @author Pierre Hubert

use crate::constants::admin::AdminRole;
use crate::constants::database_tables_names::ADMIN_ROLES_TABLE;
use crate::data::admin::AdminID;
use crate::data::error::{ExecError, Res};
use crate::helpers::database;
use crate::utils::date_utils::time;

/// Get the list of roles of a given administrator
pub fn get_roles(id: AdminID) -> Res<Vec<AdminRole>> {
    database::QueryInfo::new(ADMIN_ROLES_TABLE)
        .cond_admin_id("admin_id", id)
        .exec(db_to_role)
}

/// Add a new role to an admin
pub fn add_role(id: AdminID, role: AdminRole) -> Res {
    database::InsertQuery::new(ADMIN_ROLES_TABLE)
        .add_admin_id("admin_id", id)
        .add_str("role_id", role.to_id())
        .add_u64("time_insert", time())
        .insert_drop_result()
}

/// Remove a role to an admin
pub fn remove_role(id: AdminID, role: AdminRole) -> Res {
    database::DeleteQuery::new(ADMIN_ROLES_TABLE)
        .cond_admin_id("admin_id", id)
        .cond_str("role_id", role.to_id())
        .exec()
}
/// Check out whether an admin has a role or not
pub fn has_role(id: AdminID, role: AdminRole) -> Res<bool> {
    database::QueryInfo::new(ADMIN_ROLES_TABLE)
        .cond_admin_id("admin_id", id)
        .cond("role_id", role.to_id())
        .exec_count_has_at_least_one_result()
}

fn db_to_role(row: &database::RowResult) -> Res<AdminRole> {
    let role_id = row.get_str("role_id")?;

    let role = AdminRole::from_id(&role_id);

    match role {
        None => Err(ExecError::boxed_string(format!(
            "Role {} from database not found in the list!",
            role_id
        ))),
        Some(r) => Ok(r),
    }
}
