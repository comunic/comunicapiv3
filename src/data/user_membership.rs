//! # User membership
//!
//! @author Pierre Hubert

use std::cmp::Ordering;

use crate::data::conversation::Conversation;
use crate::data::friend::Friend;
use crate::data::group_id::GroupID;

#[derive(PartialEq, Eq)]
pub enum UserMembership {
    Group(GroupID, u64),
    Friend(Friend),
    Conversation(Conversation),
}

impl UserMembership {
    pub fn last_active(&self) -> u64 {
        match self {
            UserMembership::Group(_, last_active) => *last_active,
            UserMembership::Friend(f) => f.last_activity_time,
            UserMembership::Conversation(c) => c.last_activity,
        }
    }
}

impl PartialOrd for UserMembership {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.last_active().partial_cmp(&other.last_active())
    }
}

impl Ord for UserMembership {
    fn cmp(&self, other: &Self) -> Ordering {
        self.last_active().cmp(&other.last_active())
    }
}
