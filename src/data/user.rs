use std::hash::{Hash, Hasher};

use serde::{Deserialize, Deserializer, Serialize, Serializer};

use crate::data::u64_visitor::U64Visitor;
use crate::utils::user_data_utils::user_data_url;

///! User information
///!
///! @author Pierre Hubert
#[derive(Clone, PartialEq, Eq, Debug, Copy)]
pub struct UserID(u64);

impl UserID {
    /// Initialize a new user ID object
    pub fn new(id: u64) -> UserID {
        UserID(id)
    }

    /// Create a new invalid user id
    pub fn invalid() -> UserID {
        UserID(0)
    }

    /// Get the current ID stored in this structure
    pub fn id(&self) -> u64 {
        self.0
    }

    /// Check if the ID currently stored in this structure is valid or not
    pub fn is_valid(&self) -> bool {
        self.0 > 0
    }

    /// Turn this `UserID` object as an instance. An invalid user id will become `None` variant
    pub fn as_option(&self) -> Option<UserID> {
        match self.is_valid() {
            true => Some(self.clone()),
            false => None,
        }
    }

    /// Create an owned instance of this user ID object
    pub fn as_owned(&self) -> Self {
        Self(self.0)
    }
}

impl Hash for UserID {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.0.hash(state)
    }
}

impl PartialEq<&UserID> for UserID {
    fn eq(&self, other: &&UserID) -> bool {
        self.0 == other.0
    }
}

impl PartialEq<UserID> for &UserID {
    fn eq(&self, other: &UserID) -> bool {
        self.0 == other.0
    }
}

impl Serialize for UserID {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
    where
        S: Serializer,
    {
        serializer.serialize_u64(self.0)
    }
}

impl<'de> Deserialize<'de> for UserID {
    fn deserialize<D>(deserializer: D) -> Result<Self, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer
            .deserialize_u64(U64Visitor {})
            .map(|id| UserID::new(id))
    }
}

#[derive(Debug, PartialEq)]
pub enum UserPageVisibility {
    OPEN,
    PUBLIC,
    PRIVATE,
}

impl UserPageVisibility {
    pub fn to_api(&self) -> &'static str {
        match self {
            UserPageVisibility::OPEN => "open",
            UserPageVisibility::PUBLIC => "public",
            UserPageVisibility::PRIVATE => "private",
        }
    }
}

#[derive(Debug, PartialEq)]
#[allow(non_camel_case_types)]
pub enum AccountImageVisibility {
    FRIENDS,
    COMUNIC_USERS,
    EVERYONE,
}

impl AccountImageVisibility {
    pub fn to_api(&self) -> String {
        match self {
            AccountImageVisibility::FRIENDS => "friends".to_string(),
            AccountImageVisibility::COMUNIC_USERS => "public".to_string(),
            AccountImageVisibility::EVERYONE => "open".to_string(),
        }
    }

    pub fn from_api(s: &str) -> Self {
        match s {
            "friends" => AccountImageVisibility::FRIENDS,
            "public" => AccountImageVisibility::COMUNIC_USERS,
            "open" | _ => AccountImageVisibility::EVERYONE,
        }
    }

    pub fn to_db(&self) -> String {
        match self {
            AccountImageVisibility::FRIENDS => "friends".to_string(),
            AccountImageVisibility::COMUNIC_USERS => "comunic_users".to_string(),
            AccountImageVisibility::EVERYONE => "everyone".to_string(),
        }
    }
}

#[derive(Debug)]
pub struct User {
    pub id: UserID,
    pub email: String,
    pub password: String,
    pub first_name: String,
    pub last_name: String,
    pub last_activity: u64,
    pub user_page_visibility: UserPageVisibility,
    pub virtual_directory: Option<String>,
    pub account_image_path: Option<String>,
    pub account_image_visibility: AccountImageVisibility,
    pub public_friends_list: bool,
    pub is_email_public: bool,
    pub personal_website: Option<String>,
    pub public_note: Option<String>,
    pub location: Option<String>,
    pub block_comments_on_his_page: bool,
    pub allow_posts_from_friends: bool,
    pub account_creation_time: u64,
    pub allow_mails: bool,
    pub lang: String,

    /// Security questions
    pub security_question_1: Option<String>,
    pub security_answer_1: Option<String>,
    pub security_question_2: Option<String>,
    pub security_answer_2: Option<String>,

    /// Automatically delete the account after a period of inactivity. 0 = feature disabled
    pub delete_account_after: Option<u64>,
    pub delete_notifications_after: Option<u64>,
    pub delete_comments_after: Option<u64>,
    pub delete_posts_after: Option<u64>,
    pub delete_conversation_messages_after: Option<u64>,
    pub delete_likes_after: Option<u64>,

    /// Notifications settings
    pub allow_notif_conv: bool,
    pub allow_notif_sound: bool,
}

impl User {
    pub fn full_name(&self) -> String {
        format!("{} {}", self.first_name, self.last_name)
    }

    /// Check if user's page is public
    pub fn is_page_public(&self) -> bool {
        !matches!(self.user_page_visibility, UserPageVisibility::PRIVATE)
    }

    /// Check if user's page is open
    pub fn is_page_open(&self) -> bool {
        matches!(self.user_page_visibility, UserPageVisibility::OPEN)
    }

    /// Get the URL pointing to the default account image
    pub fn default_account_image_url() -> String {
        user_data_url(crate::constants::DEFAULT_ACCOUNT_IMAGE)
    }

    /// Get the URL pointing to the error account image, when the user is not allowed to see user
    /// account image
    pub fn error_account_image_url() -> String {
        user_data_url(crate::constants::ERROR_ACCOUNT_IMAGE)
    }

    /// Get the URL pointing to a user account image for an administrator. An administrator can
    /// only access to the user account image if it is visible to all Comunic users
    pub fn account_image_url_for_admin(&self) -> Option<String> {
        match (&self.account_image_visibility, &self.account_image_path) {
            (_, None) => None,
            (AccountImageVisibility::FRIENDS, _) => Some(User::error_account_image_url()),
            (_, Some(path)) => Some(user_data_url(path)),
        }
    }

    /// Check if this user has an account image or not
    pub fn has_account_image(&self) -> bool {
        self.account_image_path.is_some()
    }

    /// Check out whether security questions have been defined for this user or not
    pub fn has_security_questions(&self) -> bool {
        self.security_question_1.is_some()
            && self.security_answer_1.is_some()
            && self.security_question_2.is_some()
            && self.security_answer_2.is_some()
    }
}
