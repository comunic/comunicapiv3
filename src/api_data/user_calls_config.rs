//! # User side calls configuration
//!
//! @author Pierre Hubert

use serde::Serialize;

use crate::data::config::RtcRelayConfig;

#[derive(Serialize)]
#[allow(non_snake_case)]
pub struct UserCallsConfig {
    iceServers: Vec<String>,
}

impl UserCallsConfig {
    pub fn new(conf: &RtcRelayConfig) -> Self {
        Self {
            iceServers: conf.ice_servers.clone(),
        }
    }
}
