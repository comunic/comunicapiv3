-- Comunic Structure

SET NAMES utf8;
SET time_zone = '+00:00';


DROP TABLE IF EXISTS `aime`;
CREATE TABLE `aime` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_type` int(11) NOT NULL,
  `ID_personne` int(11) NOT NULL,
  `Date_envoi` datetime NOT NULL,
  `type` varchar(255) NOT NULL DEFAULT 'texte',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `amis`;
CREATE TABLE `amis` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_personne` int(11) NOT NULL,
  `ID_amis` int(11) NOT NULL,
  `actif` int(11) NOT NULL DEFAULT '0',
  `abonnement` int(11) NOT NULL DEFAULT '0',
  `autoriser_post_page` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `commentaires`;
CREATE TABLE `commentaires` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_personne` int(11) NOT NULL,
  `ID_texte` int(11) NOT NULL,
  `date_envoi` datetime NOT NULL,
  `time_insert` int(11) DEFAULT NULL,
  `commentaire` varchar(255) NOT NULL,
  `image_commentaire` longtext NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `comunic_clients`;
CREATE TABLE `comunic_clients` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `domain` varchar(45) DEFAULT NULL COMMENT 'Use to check Referer & define Access-Control-Allow-Origin',
  `comment` varchar(45) DEFAULT NULL COMMENT 'Information about the client',
  `default_expiration_time` int DEFAULT '2592000' COMMENT '2592000 = 1 month',
  `firebase_project_name` VARCHAR(45) NULL,
  `firebase_service_account_file` TEXT DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;



DROP TABLE IF EXISTS `comunic_user_access_tokens`;
CREATE TABLE `comunic_user_access_tokens` (
  `id` int NOT NULL AUTO_INCREMENT,
  `client_id` int NOT NULL,
  `token` varchar(255) NOT NULL,
  `user_id` int DEFAULT NULL,
  `last_refresh` int NOT NULL,
  `timeout` int NOT NULL,
  `push_notifications_token` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `comunic_conversations_list`;
CREATE TABLE `comunic_conversations_list` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `creation_time` int DEFAULT NULL,
  `can_everyone_add_members` tinyint DEFAULT '1',
  `color` varchar(6) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `group_id` int DEFAULT NULL,
  `min_group_membership_level` int DEFAULT NULL,
  `last_activity` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `comunic_conversations_messages`;
CREATE TABLE `comunic_conversations_messages` (
  `id` int NOT NULL AUTO_INCREMENT,
  `conv_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `time_sent` int DEFAULT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `file_path` varchar(255) DEFAULT NULL,
  `file_size` int DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_thumbnail` varchar(255) DEFAULT NULL,
  `file_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



DROP TABLE IF EXISTS `comunic_conversations_members`;
CREATE TABLE `comunic_conversations_members` (
  `id` int NOT NULL AUTO_INCREMENT,
  `conv_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `added_on` int DEFAULT NULL,
  `following` int DEFAULT '0',
  `is_admin` int DEFAULT '1',
  `last_message_seen` int DEFAULT '0',
  `last_access` int DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `comunic_groups`;
CREATE TABLE `comunic_groups` (
  `id` int NOT NULL AUTO_INCREMENT,
  `time_create` int DEFAULT NULL,
  `userid_create` int DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `path_logo` varchar(250) DEFAULT NULL,
  `visibility` int NOT NULL DEFAULT '1',
  `registration_level` int DEFAULT '1',
  `posts_level` int DEFAULT '0',
  `virtual_directory` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `is_members_list_public` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `comunic_groups_members`;
CREATE TABLE `comunic_groups_members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groups_id` int(11) DEFAULT NULL,
  `user_id` varchar(45) DEFAULT NULL,
  `time_create` varchar(45) DEFAULT NULL,
  `level` int(11) DEFAULT '2',
  `following` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `comunic_notifications`;
CREATE TABLE `comunic_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time_create` int(11) DEFAULT NULL,
  `seen` int(1) DEFAULT '0',
  `from_user_id` int(11) DEFAULT NULL,
  `dest_user_id` int(11) DEFAULT NULL,
  `on_elem_id` int(11) DEFAULT NULL,
  `on_elem_type` varchar(25) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `visibility` varchar(20) DEFAULT NULL,
  `from_container_id` int(11) DEFAULT NULL,
  `from_container_type` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `sondage`;
CREATE TABLE `sondage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_utilisateurs` int(11) NOT NULL,
  `ID_texte` int(11) NOT NULL,
  `date_creation` datetime NOT NULL,
  `question` varchar(255) NOT NULL,
  `allow_new_choices` int(11) DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `sondage_choix`;
CREATE TABLE `sondage_choix` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_sondage` int(11) NOT NULL,
  `date_creation` datetime NOT NULL,
  `Choix` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `sondage_reponse`;
CREATE TABLE `sondage_reponse` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_utilisateurs` int(11) NOT NULL,
  `ID_sondage` int(11) NOT NULL,
  `ID_sondage_choix` int(11) NOT NULL,
  `date_envoi` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `texte`;
CREATE TABLE `texte` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_personne` int(11) NOT NULL,
  `date_envoi` datetime NOT NULL,
  `time_insert` int(11) DEFAULT NULL,
  `texte` text NOT NULL,
  `ID_amis` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) DEFAULT '0',
  `niveau_visibilite` varchar(255) NOT NULL DEFAULT '1',
  `type` varchar(255) NOT NULL DEFAULT 'texte',
  `size` varchar(255) DEFAULT NULL,
  `file_type` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `annee_fin` varchar(255) DEFAULT NULL,
  `mois_fin` varchar(255) DEFAULT NULL,
  `jour_fin` varchar(255) DEFAULT NULL,
  `time_end` int(11) DEFAULT NULL,
  `url_page` varchar(255) DEFAULT NULL,
  `titre_page` varchar(255) DEFAULT NULL,
  `description_page` longtext,
  `image_page` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `utilisateurs`;
CREATE TABLE `utilisateurs` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `prenom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `date_creation` datetime NOT NULL,
  `mail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `public` int NOT NULL DEFAULT '0',
  `pageouverte` int NOT NULL DEFAULT '0',
  `question1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `reponse1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `question2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `reponse2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `bloquecommentaire` int NOT NULL DEFAULT '0',
  `last_activity` int NOT NULL DEFAULT '1',
  `autoriser_post_amis` int NOT NULL DEFAULT '1',
  `autorise_mail` int NOT NULL DEFAULT '1',
  `sous_repertoire` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `site_web` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '',
  `liste_amis_publique` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '1',
  `public_note` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `password_reset_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `password_reset_token_time_create` int DEFAULT NULL,
  `lang` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT 'en',
  `account_image_path` varchar(255) NOT NULL DEFAULT '',
  `account_image_visibility` varchar(45) NOT NULL DEFAULT 'everyone',
  `delete_account_after` int DEFAULT '0',
  `delete_notifications_after` int DEFAULT '7776000',
  `delete_comments_after` int DEFAULT '0',
  `delete_posts_after` int DEFAULT '0',
  `delete_conversation_messages_after` int DEFAULT '0',
  `delete_likes_after` int DEFAULT '0',
  `allow_notif_conv` int DEFAULT '1',
  `allow_notif_sound` int DEFAULT '1',
  `is_email_public` int DEFAULT '0',
  `location` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `comunic_custom_emojis`;
CREATE TABLE `comunic_custom_emojis` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NULL,
  `shortcut` VARCHAR(45) NULL,
  `path` VARCHAR(255) NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `forez_presence` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NULL,
  `group_id` INT NULL,
  `year` INT NULL,
  `month` INT NULL,
  `day` INT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `comunic_admin` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `time_create` INT NULL,
  `name` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `reset_token` VARCHAR(255) NULL,
  `reset_token_expire` INT NULL,
  PRIMARY KEY (`id`));


CREATE TABLE `comunic_admin_key` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `admin_id` INT NULL,
  `name` VARCHAR(45) NULL,
  `time_add` INT NULL,
  `credential` TEXT NULL,
  `password` VARCHAR(255) NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `comunic_admin_roles` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `admin_id` INT NOT NULL,
  `role_id` VARCHAR(25) NULL,
  `time_insert` INT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `comunic_admin_log` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `admin_id` INT NULL,
  `ip` VARCHAR(40) NULL,
  `time` INT NULL,
  `action` VARCHAR(255) NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `comunic_reports` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `target_type` VARCHAR(25) NOT NULL,
  `target_id` INT NOT NULL,
  `time` INT NOT NULL,
  `cause` VARCHAR(20) NOT NULL,
  `comment` TEXT NULL,
  PRIMARY KEY (`id`));
