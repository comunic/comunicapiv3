//! # Admin keys API
//!
//! @author Pierre Hubert
use serde::Serialize;

use crate::data::admin::AdminKey;

#[derive(Serialize)]
pub struct AdminKeyAPI {
    id: u64,
    name: String,
    time_add: u64,
    has_password: bool,
}

impl AdminKeyAPI {
    pub fn new(key: &AdminKey) -> Self {
        Self {
            id: key.id,
            name: key.name.to_string(),
            time_add: key.time_add,
            has_password: key.password.is_some(),
        }
    }
}
