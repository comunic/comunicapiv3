//! Find virtual directory result
//!
//! @author Pierre Hubert

use crate::data::error::ResultBoxError;
use crate::data::group_id::GroupID;
use crate::data::user::User;
use serde::Serialize;

#[derive(Serialize)]
pub struct ResultFindVirtualDirectory {
    kind: String,
    id: u64,
}

impl ResultFindVirtualDirectory {
    /// Construct a new instance
    pub fn new(
        user: ResultBoxError<User>,
        group: ResultBoxError<GroupID>,
    ) -> ResultFindVirtualDirectory {
        match (user, group) {
            // User
            (Ok(u), _) => ResultFindVirtualDirectory {
                kind: "user".to_string(),
                id: u.id.id(),
            },

            // Group
            (_, Ok(g)) => ResultFindVirtualDirectory {
                kind: "group".to_string(),
                id: g.id(),
            },

            _ => unreachable!(),
        }
    }
}
