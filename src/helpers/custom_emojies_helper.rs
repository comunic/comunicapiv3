//! # Custom emojies helper
//!
//! @author Pierre Hubert

use crate::constants::database_tables_names::EMOJIS_TABLE;
use crate::data::custom_emoji::CustomEmoji;
use crate::data::error::ResultBoxError;
use crate::data::new_custom_emoji::NewCustomEmoji;
use crate::data::user::UserID;
use crate::helpers::database;

/// Get the list of emojies of a user
pub fn get_list_user(user_id: &UserID) -> ResultBoxError<Vec<CustomEmoji>> {
    database::QueryInfo::new(EMOJIS_TABLE)
        .cond_user_id("user_id", user_id)
        .exec(db_to_custom_emoji)
}

/// Get information about a single emoji
pub fn get_single(emoji_id: u64) -> ResultBoxError<CustomEmoji> {
    database::QueryInfo::new(EMOJIS_TABLE)
        .cond_u64("id", emoji_id)
        .query_row(db_to_custom_emoji)
}

/// Check if a given user already as an emoji shortcut or not
pub fn has_user_similar_shortcut(user_id: &UserID, shortcut: &str) -> ResultBoxError<bool> {
    database::QueryInfo::new(EMOJIS_TABLE)
        .cond_user_id("user_id", user_id)
        .cond("shortcut", shortcut)
        .exec_count()
        .map(|r| r > 0)
}

/// Insert a new emoji in the database
pub fn insert(emoji: &NewCustomEmoji) -> ResultBoxError<u64> {
    database::InsertQuery::new(EMOJIS_TABLE)
        .add_user_id("user_id", &emoji.user_id)
        .add_str("shortcut", &emoji.shortcut)
        .add_str("path", &emoji.path)
        .insert_expect_result()
}

/// Delete a custom emoji
pub fn delete(c: &CustomEmoji) -> ResultBoxError {
    let path = c.sys_path();

    if path.exists() {
        std::fs::remove_file(path)?;
    }

    database::DeleteQuery::new(EMOJIS_TABLE)
        .cond_u64("id", c.id)
        .exec()
}

/// Delete all the custom emojies of a user
pub fn delete_all_user(user_id: &UserID) -> ResultBoxError {
    for emoji in &get_list_user(user_id)? {
        delete(emoji)?;
    }

    Ok(())
}

/// Turn a database entry into a [CustomEmoji]
fn db_to_custom_emoji(row: &database::RowResult) -> ResultBoxError<CustomEmoji> {
    Ok(CustomEmoji {
        id: row.get_u64("id")?,
        user_id: row.get_user_id("user_id")?,
        shortcut: row.get_str("shortcut")?,
        path: row.get_str("path")?,
    })
}
