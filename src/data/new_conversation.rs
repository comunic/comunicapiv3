//! # New conversation information
//!
//! @author Pierre Huber

use std::collections::HashSet;

use crate::data::group_id::GroupID;
use crate::data::group_member::GroupMembershipLevel;
use crate::data::user::UserID;

#[derive(Debug)]
pub struct NewConversation {
    pub owner_id: UserID,
    pub name: Option<String>,
    pub group_id: Option<GroupID>,
    pub group_min_membership_level: Option<GroupMembershipLevel>,
    pub color: Option<String>,
    pub logo: Option<String>,
    pub owner_following: bool,
    pub members: HashSet<UserID>,
    pub can_everyone_add_members: bool,
}
