//! # Security settings API
//!
//! @author Pierre Hubert

use serde::Serialize;

use crate::data::user::User;

#[derive(Serialize)]
#[allow(non_snake_case)]
pub struct SecuritySettingsAPI {
    id: u64,
    security_question_1: String,
    security_answer_1: String,
    security_question_2: String,
    security_answer_2: String,
}

impl SecuritySettingsAPI {
    pub fn new(user: &User) -> SecuritySettingsAPI {
        SecuritySettingsAPI {
            id: user.id.id(),
            security_question_1: user.security_question_1.clone().unwrap_or_default(),
            security_answer_1: user.security_answer_1.clone().unwrap_or_default(),
            security_question_2: user.security_question_2.clone().unwrap_or_default(),
            security_answer_2: user.security_answer_2.clone().unwrap_or_default(),
        }
    }
}
