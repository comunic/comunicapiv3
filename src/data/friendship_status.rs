//! # Friendship status
//!
//! @author Pierre Hubert

pub struct FriendshipStatus {
    pub are_friend: bool,
    pub sent_request: bool,
    pub received_request: bool,
    pub following: bool,
}
