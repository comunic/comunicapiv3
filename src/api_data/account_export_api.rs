//! # Export account API entry
//!
//! @author Pierre Hubert
use std::collections::HashMap;

use serde::Serialize;

use crate::api_data::comment_api::CommentAPI;
use crate::api_data::conversation_api::ConversationAPI;
use crate::api_data::conversation_message_api::ConversationMessageAPI;
use crate::api_data::entities_constructor::EntitiesConstructor;
use crate::api_data::friend_api::FriendAPI;
use crate::api_data::group_api::GroupApi;
use crate::api_data::post_api::PostAPI;
use crate::api_data::survey_response_api::SurveyResponseAPI;
use crate::api_data::user_info::{APIAdvancedInfo, APIUserInfo};
use crate::api_data::user_like_api::UserLikeAPI;
use crate::data::account_export::AccountExport;
use crate::data::error::ResultBoxError;
use crate::data::group::Group;
use crate::data::user::User;
use crate::helpers::groups_helper;
use crate::helpers::user_helper;

#[derive(Serialize)]
#[allow(non_snake_case)]
pub struct AccountExportAPI {
    userID: u64,
    advanced_info: APIAdvancedInfo,
    posts: Vec<PostAPI>,
    comments: Vec<CommentAPI>,
    likes: Vec<UserLikeAPI>,
    survey_responses: Vec<SurveyResponseAPI>,
    all_conversation_messages: Vec<ConversationMessageAPI>,
    conversations_list: Vec<ConversationAPI>,
    conversations_messages: HashMap<u64, Vec<ConversationMessageAPI>>,
    friends_list: Vec<FriendAPI>,
    groups: Vec<GroupApi>,
    users_info: HashMap<u64, APIUserInfo>,
}

impl AccountExportAPI {
    pub fn new(export: &AccountExport) -> ResultBoxError<AccountExportAPI> {
        let curr_user_id = &export.user.id;
        let export = AccountExportAPI {
            userID: curr_user_id.id(),
            advanced_info: APIUserInfo::new_advanced_info(&curr_user_id.as_option(), &export.user)?,
            posts: PostAPI::for_list(&export.posts, curr_user_id.as_option())?,
            comments: CommentAPI::for_list(&export.comments, &curr_user_id.as_option())?,
            likes: UserLikeAPI::for_list(&export.likes),
            survey_responses: SurveyResponseAPI::for_list(&export.survey_responses),
            all_conversation_messages: ConversationMessageAPI::for_list(
                &export.all_conversation_messages,
            ),
            conversations_list: ConversationAPI::for_list(&export.conversations),
            conversations_messages: export
                .conversation_messages
                .iter()
                .map(|r| (r.0.id(), ConversationMessageAPI::for_list(r.1)))
                .collect(),
            friends_list: FriendAPI::from_list(&export.friends_list),
            groups: export
                .groups
                .iter()
                .map(groups_helper::get_info)
                .collect::<Result<Vec<Group>, _>>()?
                .iter()
                .map(|g| GroupApi::new(g, curr_user_id.as_option()))
                .collect::<Result<Vec<GroupApi>, _>>()?,
            users_info: export
                .get_related_users_ids()?
                .iter()
                .map(user_helper::find_user_by_id)
                .collect::<Result<Vec<User>, _>>()?
                .iter()
                .map(|u| APIUserInfo::new(&curr_user_id.as_option(), u).map(|r| (u.id.id(), r)))
                .collect::<Result<HashMap<u64, APIUserInfo>, _>>()?,
        };

        Ok(export)
    }
}
