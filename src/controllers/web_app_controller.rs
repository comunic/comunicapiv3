//! # Web application controller
//!
//! @author Pierre Hubert

use crate::api_data::user_membership_api::UserMembershipAPI;
use crate::data::base_request_handler::BaseRequestHandler;
use crate::data::http_request_handler::HttpRequestHandler;
use crate::helpers::webapp_helper;
use crate::routes::RequestResult;

/// Get the list of memberships of a given user
pub async fn get_memberships(r: &mut HttpRequestHandler) -> RequestResult {
    let memberships = webapp_helper::get_user_memberships(r.user_id_ref()?)?;

    r.set_response(UserMembershipAPI::for_list(&memberships))
}
