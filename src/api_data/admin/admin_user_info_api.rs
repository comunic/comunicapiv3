//! # Admin API : User information
//!
//! Get information about a single user
//!
//! @author Pierre Hubert

use crate::data::user::User;

#[derive(serde::Serialize)]
pub struct AdminUserInfoAPI {
    id: u64,
    first_name: String,
    last_name: String,
    email: String,
    account_creation_time: u64,
    last_activity: u64,
    page_visibility: &'static str,
    directory: Option<String>,
    account_image: Option<String>,
    account_image_visibility: String,
    friend_list_public: bool,
    is_email_public: bool,
    personal_website: Option<String>,
    public_note: Option<String>,
    location: Option<String>,
    block_comments: bool,
    allow_posts_from_friends: bool,
    allow_mails: bool,
    lang: String,
}

impl AdminUserInfoAPI {
    pub fn new(user: User) -> Self {
        Self {
            account_image: user.account_image_url_for_admin(),
            account_image_visibility: user.account_image_visibility.to_api(),
            friend_list_public: user.public_friends_list,
            is_email_public: user.is_email_public,
            personal_website: user.personal_website,
            public_note: user.public_note,
            location: user.location,
            block_comments: user.block_comments_on_his_page,
            allow_posts_from_friends: user.allow_posts_from_friends,
            allow_mails: user.allow_mails,
            id: user.id.id(),
            first_name: user.first_name,
            last_name: user.last_name,
            email: user.email,
            account_creation_time: user.account_creation_time,
            last_activity: user.last_activity,
            page_visibility: user.user_page_visibility.to_api(),
            directory: user.virtual_directory,
            lang: user.lang,
        }
    }
}
