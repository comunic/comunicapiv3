//! # User controller
//!
//! This controller handles all the routes related about getting user information
//!
//! @author Pierre Hubert

use std::collections::HashMap;

use crate::api_data::user_info::APIUserInfo;
use crate::data::base_request_handler::BaseRequestHandler;
use crate::data::http_request_handler::HttpRequestHandler;
use crate::data::user::UserID;
use crate::helpers::user_helper;
use crate::helpers::user_helper::find_user_by_id;
use crate::routes::RequestResult;

/// Get information about a single user
pub async fn get_single(request: &mut HttpRequestHandler) -> RequestResult {
    let user_id = UserID::new(request.post_u64("userID")?);
    let user = match user_helper::find_user_by_id(&user_id) {
        Ok(user) => user,
        Err(e) => {
            println!("Error while getting user info: {}", e);
            request.not_found("Could not get user information!".to_string())?;
            unreachable!();
        }
    };

    request.set_response(APIUserInfo::new(&request.user_id_opt(), &user)?)
}

/// Get information about several users
pub async fn get_multiple(request: &mut HttpRequestHandler) -> RequestResult {
    let user_ids = request.post_numbers_list("usersID", 1)?;

    let mut map: HashMap<u64, APIUserInfo> = HashMap::new();

    for user_id in user_ids {
        let user_id = UserID::new(user_id);
        let user = request.ok_or_not_found(
            find_user_by_id(&user_id),
            "At least one user was not found!",
        )?;

        map.insert(
            user_id.id(),
            APIUserInfo::new(&request.user_id_opt(), &user)?,
        );
    }

    request.set_response(map)
}

/// Get advanced information about a user
pub async fn get_advanced_info(request: &mut HttpRequestHandler) -> RequestResult {
    let user_id = request.post_user_id("userID")?;

    if !user_helper::can_see_user_page(&request.user_id_opt().unwrap_or(UserID::new(0)), &user_id)?
    {
        request.forbidden("You are not allowed to see this user page!".to_string())?;
    }

    let user = user_helper::find_user_by_id(&user_id)?;
    request.set_response(APIUserInfo::new_advanced_info(
        &request.user_id_opt(),
        &user,
    )?)
}
