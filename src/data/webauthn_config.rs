//! # Webauthn config
//!
//! @author Pierre Hubert

use webauthn_rs::{Webauthn, WebauthnBuilder};

use crate::data::config::conf;

pub fn get_wan() -> Webauthn {
    WebauthnBuilder::new(
        conf()
            .admin_url
            .replace("https://", "")
            .replace("http://", "")
            .split(':')
            .next()
            .unwrap()
            .split('/')
            .next()
            .unwrap(),
        &url::Url::parse(&conf().admin_url).unwrap(),
    )
    .expect("Invalid Webauthn configuration!")
    .rp_name("ComunicAdmin")
    .build()
    .expect("Failed to build Webauthn object")
}
