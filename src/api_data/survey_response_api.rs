//! # Survey response API entry
//!
//! @author Pierre Hubert
use serde::Serialize;

use crate::api_data::entities_constructor::EntitiesConstructor;
use crate::data::survey_response::SurveyResponse;

#[derive(Serialize)]
#[allow(non_snake_case)]
pub struct SurveyResponseAPI {
    id: u64,
    time_sent: u64,
    userID: u64,
    surveyID: u64,
    choiceID: u64,
}

impl EntitiesConstructor for SurveyResponseAPI {
    type Item = SurveyResponse;

    fn new(r: &Self::Item) -> Self {
        SurveyResponseAPI {
            id: r.id,
            time_sent: r.time_sent,
            userID: r.user_id.id(),
            surveyID: r.survey_id,
            choiceID: r.choice_id,
        }
    }
}
