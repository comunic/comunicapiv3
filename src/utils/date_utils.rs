//! # Date utilities
//!
//! @author Pierre Hubert

use std::time::{SystemTime, UNIX_EPOCH};

use chrono::{Datelike, TimeZone, Utc};

/// Get the current time since epoch
///
/// ```
/// use comunic_server::utils::date_utils::time;
///
/// let time = time();
/// ```
pub fn time() -> u64 {
    SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_secs()
}

/// Get Mysql formatted date at specific time
///
/// ```
/// use comunic_server::utils::date_utils::time_to_mysql_date;
///
/// assert_eq!(time_to_mysql_date(1594140466), "2020-07-07 16:47:46");
/// ```
pub fn time_to_mysql_date(time: u64) -> String {
    let utc = Utc.timestamp_opt(time as i64, 0).unwrap();
    let str = utc.to_rfc3339();
    (&str[..19]).replace('T', " ")
}

/// Get current Mysql formatted date
pub fn mysql_date() -> String {
    time_to_mysql_date(time())
}

/// Get current year
pub fn current_year() -> i32 {
    let utc = Utc.timestamp_opt(time() as i64, 0).unwrap();
    utc.year()
}
