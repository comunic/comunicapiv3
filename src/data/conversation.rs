//! # Conversation information
//!
//! @author Pierre Hubert

use crate::data::group_id::GroupID;
use crate::data::group_member::GroupMembershipLevel;
use crate::data::user::UserID;

#[derive(Copy, Debug, PartialEq, Eq, Clone, Hash)]
pub struct ConvID(u64);

impl ConvID {
    pub fn new(id: u64) -> Self {
        ConvID(id)
    }

    pub fn id(&self) -> u64 {
        self.0.clone()
    }
}

#[derive(Debug)]
pub struct ConversationMember {
    pub member_id: u64,
    pub conv_id: ConvID,
    pub user_id: UserID,
    pub added_on: u64,
    pub following: bool,
    pub is_admin: bool,
    pub last_message_seen: u64,
    pub last_access: u64,
}

#[derive(Debug)]
pub struct Conversation {
    pub id: ConvID,
    pub name: Option<String>,
    pub color: Option<String>,
    pub logo: Option<String>,
    pub creation_time: u64,
    pub group_id: Option<GroupID>,
    pub min_group_membership_level: Option<GroupMembershipLevel>,
    pub can_everyone_add_members: bool,
    pub last_activity: u64,
    pub members: Vec<ConversationMember>,
}

impl PartialEq for Conversation {
    fn eq(&self, other: &Self) -> bool {
        self.id.eq(&other.id)
    }
}

impl Eq for Conversation {}

impl Conversation {
    /// Get the IDs of the members in the conversation
    pub fn members_ids(&self) -> Vec<UserID> {
        self.members.iter().map(|m| m.user_id.clone()).collect()
    }

    /// Check out whether this conversation is managed or not
    pub fn is_managed(&self) -> bool {
        self.is_linked_to_group()
    }

    /// Check if this conversation is linked to a group
    pub fn is_linked_to_group(&self) -> bool {
        self.group_id.is_some()
    }

    /// Check out whether a given user is an admin of a conversation or not
    pub fn is_admin(&self, user_id: &UserID) -> bool {
        self.members
            .iter()
            .any(|m| m.user_id == user_id && m.is_admin)
    }

    /// Check out whether a user can add members to a conversation or not
    pub fn can_user_add_members(&self, user_id: &UserID) -> bool {
        !self.is_managed() && (self.is_admin(user_id) || self.can_everyone_add_members)
    }

    /// Check out whether a user can mark other admin or not
    pub fn can_mark_other_users_admin(&self, user_id: &UserID) -> bool {
        !self.is_managed() && self.is_admin(user_id)
    }

    /// Check out whether a user can remove members from a conversation or not
    pub fn can_user_remove_members(&self, user_id: &UserID) -> bool {
        !self.is_managed() && self.is_admin(user_id)
    }

    /// Check out whether a user is the last administrator of a conversation or not
    pub fn is_last_admin(&self, user_id: &UserID) -> bool {
        let admins: Vec<&ConversationMember> = self.members.iter().filter(|m| m.is_admin).collect();

        admins.len() == 1 && admins[0].user_id == user_id
    }

    /// Get the membership of a user over a conversation
    pub fn get_membership(&self, user_id: &UserID) -> Option<&ConversationMember> {
        self.members.iter().filter(|u| u.user_id == user_id).next()
    }
}

/// Structure used to update the list of members of the conversation
pub struct ConversationMemberSetting {
    pub user_id: UserID,
    pub set_admin: bool,
}

/// Structure used to update conversation settings
#[derive(Debug)]
pub struct NewConversationSettings {
    pub conv_id: ConvID,
    pub color: Option<String>,
    pub name: Option<String>,
    pub can_everyone_add_members: bool,
}
