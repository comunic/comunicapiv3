//! # Notification
//!
//! @author Pierre Hubert

use crate::data::user::UserID;

#[allow(non_camel_case_types)]
pub enum NotifElemType {
    UNKNOWN,
    USER_PAGE,
    GROUP_PAGE,
    CONVERSATION,
    CONVERSATION_MESSAGE,
    POST,
    COMMENT,
    FRIENDSHIP_REQUEST,
    GROUP_MEMBERSHIP,
}

impl NotifElemType {
    pub fn from_db(d: &str) -> NotifElemType {
        match d {
            "user_page" => NotifElemType::USER_PAGE,
            "group_page" => NotifElemType::GROUP_PAGE,
            "conversation" => NotifElemType::CONVERSATION,
            "conversation_message" => NotifElemType::CONVERSATION_MESSAGE,
            "post" => NotifElemType::POST,
            "comment" => NotifElemType::COMMENT,
            "friend_request" => NotifElemType::FRIENDSHIP_REQUEST,
            "group_membership" => NotifElemType::GROUP_MEMBERSHIP,
            _ => NotifElemType::UNKNOWN,
        }
    }

    pub fn to_db(&self) -> String {
        match self {
            NotifElemType::USER_PAGE => "user_page",
            NotifElemType::GROUP_PAGE => "group_page",
            NotifElemType::CONVERSATION => "conversation",
            NotifElemType::CONVERSATION_MESSAGE => "conversation_message",
            NotifElemType::POST => "post",
            NotifElemType::COMMENT => "comment",
            NotifElemType::FRIENDSHIP_REQUEST => "friend_request",
            NotifElemType::GROUP_MEMBERSHIP => "group_membership",
            NotifElemType::UNKNOWN => "",
        }
        .to_string()
    }

    pub fn to_api(&self) -> String {
        self.to_db()
    }
}

#[allow(non_camel_case_types)]
pub enum NotifEventType {
    UNKNOWN,
    COMMENT_CREATED,
    SENT_FRIEND_REQUEST,
    ACCEPTED_FRIEND_REQUEST,
    REJECTED_FRIEND_REQUEST,
    ELEM_CREATED,
    ELEM_UPDATED,
    SENT_GROUP_MEMBERSHIP_INVITATION,
    ACCEPTED_GROUP_MEMBERSHIP_INVITATION,
    REJECTED_GROUP_MEMBERSHIP_INVITATION,
    SENT_GROUP_MEMBERSHIP_REQUEST,
    ACCEPTED_GROUP_MEMBERSHIP_REQUEST,
    REJECTED_GROUP_MEMBERSHIP_REQUEST,
}

impl NotifEventType {
    pub fn from_db(d: &str) -> NotifEventType {
        match d {
            "comment_created" => NotifEventType::COMMENT_CREATED,
            "sent_friend_request" => NotifEventType::SENT_FRIEND_REQUEST,
            "accepted_friend_request" => NotifEventType::ACCEPTED_FRIEND_REQUEST,
            "rejected_friend_request" => NotifEventType::REJECTED_FRIEND_REQUEST,
            "elem_created" => NotifEventType::ELEM_CREATED,
            "elem_updated" => NotifEventType::ELEM_UPDATED,
            "sent_group_membership_invitation" => NotifEventType::SENT_GROUP_MEMBERSHIP_INVITATION,
            "accepted_group_membership_invitation" => {
                NotifEventType::ACCEPTED_GROUP_MEMBERSHIP_INVITATION
            }
            "rejected_group_membership_invitation" => {
                NotifEventType::REJECTED_GROUP_MEMBERSHIP_INVITATION
            }
            "sent_group_membership_request" => NotifEventType::SENT_GROUP_MEMBERSHIP_REQUEST,
            "accepted_group_membership_request" => {
                NotifEventType::ACCEPTED_GROUP_MEMBERSHIP_REQUEST
            }
            "rejected_group_membership_request" => {
                NotifEventType::REJECTED_GROUP_MEMBERSHIP_REQUEST
            }
            _ => NotifEventType::UNKNOWN,
        }
    }

    pub fn to_db(&self) -> String {
        match self {
            NotifEventType::COMMENT_CREATED => "comment_created",
            NotifEventType::SENT_FRIEND_REQUEST => "sent_friend_request",
            NotifEventType::ACCEPTED_FRIEND_REQUEST => "accepted_friend_request",
            NotifEventType::REJECTED_FRIEND_REQUEST => "rejected_friend_request",
            NotifEventType::ELEM_CREATED => "elem_created",
            NotifEventType::ELEM_UPDATED => "elem_updated",
            NotifEventType::SENT_GROUP_MEMBERSHIP_INVITATION => "sent_group_membership_invitation",
            NotifEventType::ACCEPTED_GROUP_MEMBERSHIP_INVITATION => {
                "accepted_group_membership_invitation"
            }
            NotifEventType::REJECTED_GROUP_MEMBERSHIP_INVITATION => {
                "rejected_group_membership_invitation"
            }
            NotifEventType::SENT_GROUP_MEMBERSHIP_REQUEST => "sent_group_membership_request",
            NotifEventType::ACCEPTED_GROUP_MEMBERSHIP_REQUEST => {
                "accepted_group_membership_request"
            }
            NotifEventType::REJECTED_GROUP_MEMBERSHIP_REQUEST => {
                "rejected_group_membership_request"
            }
            NotifEventType::UNKNOWN => "",
        }
        .to_string()
    }

    pub fn to_api(&self) -> String {
        self.to_db()
    }
}

#[allow(non_camel_case_types)]
pub enum NotifEventVisibility {
    EVENT_PRIVATE,
    EVENT_PUBLIC,
}

impl NotifEventVisibility {
    pub fn from_db(d: &str) -> NotifEventVisibility {
        match d {
            "event_public" => NotifEventVisibility::EVENT_PUBLIC,
            "event_private" => NotifEventVisibility::EVENT_PRIVATE,
            _ => NotifEventVisibility::EVENT_PRIVATE,
        }
    }

    pub fn to_db(&self) -> String {
        match self {
            NotifEventVisibility::EVENT_PUBLIC => "event_public",
            NotifEventVisibility::EVENT_PRIVATE => "event_private",
        }
        .to_string()
    }

    pub fn to_api(&self) -> String {
        self.to_db()
    }
}

pub struct Notification {
    pub id: u64,
    pub time_create: u64,
    pub seen: bool,
    pub from_user_id: UserID,
    pub dest_user_id: UserID,
    pub on_elem_id: u64,
    pub on_elem_type: NotifElemType,
    pub kind: NotifEventType,
    pub visibility: NotifEventVisibility,
    pub container_id: Option<u64>,
    pub container_type: Option<NotifElemType>,
}

impl Notification {
    pub fn into_partial(self) -> PartialNotification {
        PartialNotification {
            id: Some(self.id),
            time_create: Some(self.time_create),
            seen: Some(self.seen),
            from_user_id: Some(self.from_user_id),
            dest_user_id: Some(self.dest_user_id),
            on_elem_id: Some(self.on_elem_id),
            on_elem_type: Some(self.on_elem_type),
            kind: Some(self.kind),
            visibility: Some(self.visibility),
            container_id: self.container_id,
            container_type: self.container_type,
        }
    }
}

pub struct PartialNotification {
    pub id: Option<u64>,
    pub time_create: Option<u64>,
    pub seen: Option<bool>,
    pub from_user_id: Option<UserID>,
    pub dest_user_id: Option<UserID>,
    pub on_elem_id: Option<u64>,
    pub on_elem_type: Option<NotifElemType>,
    pub kind: Option<NotifEventType>,
    pub visibility: Option<NotifEventVisibility>,
    pub container_id: Option<u64>,
    pub container_type: Option<NotifElemType>,
}

impl PartialNotification {
    pub fn new() -> PartialNotification {
        PartialNotification {
            id: None,
            time_create: None,
            seen: None,
            from_user_id: None,
            dest_user_id: None,
            on_elem_id: None,
            on_elem_type: None,
            kind: None,
            visibility: None,
            container_id: None,
            container_type: None,
        }
    }

    pub fn set_id(mut self, id: u64) -> PartialNotification {
        self.id = Some(id);
        self
    }

    pub fn has_id(&self) -> bool {
        self.id.is_some()
    }

    pub fn set_time_create(mut self, time: u64) -> PartialNotification {
        self.time_create = Some(time);
        self
    }

    pub fn set_dest_user_id(mut self, id: &UserID) -> PartialNotification {
        self.dest_user_id = Some(id.clone());
        self
    }

    pub fn set_from_user_id(mut self, id: &UserID) -> PartialNotification {
        self.from_user_id = Some(id.clone());
        self
    }

    pub fn set_on_elem_id(mut self, id: u64) -> PartialNotification {
        self.on_elem_id = Some(id);
        self
    }

    pub fn set_on_elem_type(mut self, t: NotifElemType) -> PartialNotification {
        self.on_elem_type = Some(t);
        self
    }

    pub fn set_type(mut self, t: NotifEventType) -> PartialNotification {
        self.kind = Some(t);
        self
    }
}
