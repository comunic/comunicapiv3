//! # Global search result API
//!
//! @author Pierre

use serde::Serialize;

use crate::data::global_search_result::GlobalSearchResult;

#[derive(Serialize)]
pub struct GlobalSearchResultAPI {
    kind: String,
    id: u64,
}

impl GlobalSearchResultAPI {
    /// Construct new entry
    pub fn new(res: &GlobalSearchResult) -> GlobalSearchResultAPI {
        match res {
            GlobalSearchResult::User(user_id) => GlobalSearchResultAPI {
                kind: "user".to_string(),
                id: user_id.id(),
            },
            GlobalSearchResult::Group(group_id) => GlobalSearchResultAPI {
                kind: "group".to_string(),
                id: group_id.id(),
            },
        }
    }

    /// Construct a list of results
    pub fn for_list(l: &Vec<GlobalSearchResult>) -> Vec<GlobalSearchResultAPI> {
        l.iter().map(|f| Self::new(f)).collect()
    }
}
