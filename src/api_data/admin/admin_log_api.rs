//! # Admin log api entry
//!
//! @author Pierre Hubert

use crate::data::admin_action_log::{AdminAction, AdminActionLog};

#[derive(serde::Serialize)]
pub struct AdminLogAPI {
    id: u64,
    admin_id: u64,
    ip: String,
    time: u64,
    action: AdminAction,
    format: &'static str,
}

impl AdminLogAPI {
    pub fn new(log: &AdminActionLog) -> Self {
        Self {
            id: log.id,
            admin_id: log.admin_id.id(),
            ip: log.ip.to_string(),
            time: log.time,
            action: log.action.clone(),
            format: log.action.format_string(),
        }
    }
}
