//! # Security settings
//!
//! Security settings of a user

use crate::data::user::UserID;

pub struct SecurityQuestion(Option<String>, Option<String>);

impl SecurityQuestion {
    pub fn new(question: &Option<String>, answer: &Option<String>) -> SecurityQuestion {
        SecurityQuestion(question.clone(), answer.clone())
    }

    pub fn question(&self) -> Option<String> {
        self.0.clone()
    }

    pub fn answer(&self) -> Option<String> {
        self.1.clone()
    }
}

pub struct SecuritySettings {
    pub id: UserID,
    pub question1: SecurityQuestion,
    pub question2: SecurityQuestion,
}
