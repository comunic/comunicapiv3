//! # User data utilities
//!
//! @author Pierre Hubert

use std::path::{Path, PathBuf};

use serde_json::to_string;

use crate::data::config::conf;
use crate::data::error::{ExecError, Res, ResultBoxError};
use crate::data::user::UserID;
use crate::utils::crypt_utils::rand_str;

/// Get the full URL to a user data file
///
/// `uri` should contain the path to the target resource
pub fn user_data_url(uri: &str) -> String {
    format!("{}{}", conf().storage_url, uri)
}

/// Get the system path to a user data file
pub fn user_data_path(uri: &Path) -> PathBuf {
    Path::new(conf().storage_path.as_str()).join(uri)
}

/// Prepare the creation of a file (user data file)
///
/// This function returns the relative folder path in user data directory where the file can be
/// created
pub fn prepare_file_creation(user_id: &UserID, folder: &str) -> ResultBoxError<PathBuf> {
    let subfolder = match user_id.id() {
        0 => Path::new(folder).to_path_buf(),
        id => Path::new(folder).join(to_string(&id)?),
    };

    let full_path = user_data_path(subfolder.as_path());

    if !full_path.exists() {
        // Create the directory
        std::fs::create_dir_all(&full_path)?;

        // Block directory listing
        std::fs::write(&full_path.join("index.html"), "<b>No listing.</b>")?;
    }

    Ok(subfolder)
}

/// Generate a new file name to store user data
pub fn generate_new_user_data_file_name(dir: &Path, ext: &str) -> ResultBoxError<PathBuf> {
    let sys_dir = user_data_path(dir);

    if !sys_dir.exists() {
        return Err(ExecError::boxed_string(format!(
            "Directory {:?} does not exists!",
            dir
        )));
    }

    loop {
        let file = format!("{}.{}", rand_str(30), ext);

        if !sys_dir.join(&file).exists() {
            return Ok(dir.join(&file));
        }
    }
}

/// Delete a file from user data, if it exists
pub fn delete_user_data_file_if_exists(path: &str) -> Res {
    let path = user_data_path(path.as_ref());
    if path.exists() {
        std::fs::remove_file(path)?;
    }
    Ok(())
}
