//! # WebSocket message
//!
//! @author Pierre Hubert

use serde::{Deserialize, Serialize};

use crate::data::error::Res;

#[derive(Clone, Serialize, Deserialize)]
pub struct UserWsMessage {
    pub id: Option<String>,
    pub title: String,
    pub data: serde_json::Value,
}

impl UserWsMessage {
    /// Construct a new WebSocket message with no ID
    pub fn no_id_message<T: Serialize>(title: &str, data: T) -> Res<Self> {
        Ok(UserWsMessage {
            id: None,
            title: title.to_string(),
            data: serde_json::to_value(data)?,
        })
    }
}
