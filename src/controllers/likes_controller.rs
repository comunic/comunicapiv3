//! # Likes controller
//!
//! @author Pierre Hubert

use crate::data::base_request_handler::BaseRequestHandler;
use crate::data::error::ExecError;
use crate::data::group::GroupAccessLevel;
use crate::data::post::PostAccessLevel;
use crate::helpers::likes_helper::LikeType;
use crate::helpers::{likes_helper, notifications_helper, user_helper};
use crate::routes::RequestResult;

struct LikeTarget(u64, LikeType);

/// Update like status
pub async fn update<H: BaseRequestHandler>(r: &mut H) -> RequestResult {
    let req_type = r.post_string("type")?;
    let is_liking = r.post_bool("like")?;

    let target = match req_type.as_str() {
        // In case of user
        "user" => {
            let user_id = r.post_user_id("id")?;

            if !user_helper::can_see_user_page(&r.user_id_or_invalid(), &user_id)? {
                r.forbidden("You cannot access this user information!".to_string())?;
            }

            LikeTarget(user_id.id(), LikeType::USER)
        }

        // In case of post
        "post" => {
            let post = r.post_post_with_access("id", PostAccessLevel::BASIC_ACCESS)?;

            // Delete any notification targeting this user about the post
            notifications_helper::delete_all_post_notifications_targeting_user(
                r.user_id_ref()?,
                post.id,
            )
            .await?;

            LikeTarget(post.id, LikeType::POST)
        }

        // In case of comment
        "comment" => {
            let comment = r.post_comment_with_access("id")?;

            // Delete any notification targeting this user about the post
            notifications_helper::delete_all_post_notifications_targeting_user(
                r.user_id_ref()?,
                comment.post_id,
            )
            .await?;

            LikeTarget(comment.id, LikeType::COMMENT)
        }

        // In case of group
        "group" => {
            let group_id = r.post_group_id_with_access("id", GroupAccessLevel::VIEW_ACCESS)?;
            LikeTarget(group_id.id(), LikeType::GROUP)
        }

        _ => {
            r.internal_error(ExecError::boxed_new("Unsupported like type!"))?;
            unreachable!();
        }
    };

    likes_helper::update(r.user_id_ref()?, is_liking, target.0, target.1)?;

    r.success("")
}
