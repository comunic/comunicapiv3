//! # API structures
//!
//! ## Presentation
//! This module contains all the serializable structures that can be sent through the API to the
//! Clients
//!
//! ## Author
//! Pierre Hubert

pub mod admin;

pub mod account_export_api;
pub mod account_image_settings_api;
pub mod advanced_group_api;
pub mod call_member_info;
pub mod call_peer_interrupted_streaming;
pub mod call_peer_ready;
pub mod comment_api;
pub mod conversation_api;
pub mod conversation_message_api;
pub mod current_user_id;
pub mod custom_emoji;
pub mod data_conservation_settings_api;
pub mod entities_constructor;
pub mod friend_api;
pub mod friendship_status_api;
pub mod general_settings_api;
pub mod global_search_result_api;
pub mod group_api;
pub mod group_member_api;
pub mod http_error;
pub mod joined_call_message;
pub mod language_settings_api;
pub mod left_call_message;
mod legacy_api_bool;
pub mod list_unread_conversations_api;
pub mod login_success;
pub mod new_call_signal;
pub mod notification_api;
pub mod notification_settings_api;
pub mod post_api;
pub mod posts_targets_api;
pub mod push_notifications_status_api;
pub mod removed_user_from_conv_message;
pub mod res_change_group_logo;
pub mod res_check_email_exists;
pub mod res_check_password_token;
pub mod res_check_security_answers;
pub mod res_check_security_questions_exists;
pub mod res_count_all_unreads;
pub mod res_count_unread_conversations;
pub mod res_create_comment;
pub mod res_create_conversation;
pub mod res_create_conversation_for_group;
pub mod res_create_custom_emoji;
pub mod res_create_group;
pub mod res_create_post;
pub mod res_find_private_conversations;
pub mod res_find_user_by_virtual_directory;
pub mod res_find_virtual_directory;
pub mod res_get_security_questions;
pub mod res_get_ws_token;
pub mod res_number_unread_notifications;
pub mod security_settings_api;
pub mod server_config;
pub mod submit_report_result_api;
pub mod survey_api;
pub mod survey_choice_api;
pub mod survey_response_api;
mod type_container_api;
pub mod user_calls_config;
pub mod user_info;
pub mod user_is_writing_message_in_conversation;
pub mod user_like_api;
pub mod user_mail_address;
pub mod user_membership_api;
