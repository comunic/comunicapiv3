//! # Push notifications controller
//!
//! @author Pierre Hubert

use crate::api_data::push_notifications_status_api::PushNotificationsStatusAPI;
use crate::data::base_request_handler::BaseRequestHandler;
use crate::data::config::conf;
use crate::data::http_request_handler::HttpRequestHandler;
use crate::data::user_token::PushNotificationToken;
use crate::helpers::{account_helper, independent_push_notifications_service_helper};
use crate::routes::RequestResult;

/// Get current push notifications status for a connection
pub async fn get_status(r: &mut HttpRequestHandler) -> RequestResult {
    let status = &r
        .user_access_token()
        .unwrap()
        .push_notifications_token
        .clone();

    r.set_response(PushNotificationsStatusAPI::new(status))
}

/// Configure push notifications for a client
pub async fn configure(r: &mut HttpRequestHandler) -> RequestResult {
    let status = r.post_string("status")?;

    let status = match status.as_str() {
        "disabled" => PushNotificationToken::NONE,

        "firebase" => {
            if !r.api_client().is_firebase_available() {
                return r.bad_request("Firebase is unavailable!".to_string());
            }

            PushNotificationToken::FIREBASE(r.post_string("firebase_token")?)
        }

        "independent" => {
            if !conf().is_independent_push_notifications_service_enabled() {
                return r.bad_request("Independent service is unavailable!".to_string());
            }

            let token = independent_push_notifications_service_helper::create_token().await?;

            PushNotificationToken::INDEPENDENT(token)
        }

        _ => {
            return r.bad_request(format!("Unknown status: {}", status));
        }
    };

    account_helper::set_push_notification_token(r.user_access_token().unwrap(), status).await?;

    r.ok()
}
