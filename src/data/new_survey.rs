//! # New survey information
//!
//! @author Pierre Hubert

use crate::data::user::UserID;

pub struct NewSurvey {
    pub post_id: u64,
    pub user_id: UserID,
    pub question: String,
    pub choices: Vec<String>,
    pub allow_new_choices: bool,
}
