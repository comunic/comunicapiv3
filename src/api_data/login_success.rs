use serde::{Deserialize, Serialize};

/// Data sent in case of successful login
///
/// @author Pierre Hubert
#[derive(Serialize, Deserialize)]
struct LoginTokens {
    token1: String,
    token2: String,
}

#[derive(Serialize, Deserialize)]
pub struct LoginSuccess {
    token: String,
}

impl LoginSuccess {
    pub fn new(token: &str) -> LoginSuccess {
        LoginSuccess {
            token: token.to_string(),
        }
    }
}
