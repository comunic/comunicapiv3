//! # Data conservation settings API object
//!
//! @author Pierre Hubert
use serde::Serialize;

use crate::data::user::User;

#[derive(Serialize)]
pub struct DataConservationSettingsAPI {
    inactive_account_lifetime: Option<u64>,
    notification_lifetime: Option<u64>,
    comments_lifetime: Option<u64>,
    posts_lifetime: Option<u64>,
    conversation_messages_lifetime: Option<u64>,
    likes_lifetime: Option<u64>,
}

impl DataConservationSettingsAPI {
    pub fn new(user: &User) -> Self {
        Self {
            inactive_account_lifetime: user.delete_account_after,
            notification_lifetime: user.delete_notifications_after,
            comments_lifetime: user.delete_comments_after,
            posts_lifetime: user.delete_posts_after,
            conversation_messages_lifetime: user.delete_conversation_messages_after,
            likes_lifetime: user.delete_likes_after,
        }
    }
}
