use crate::constants::push_notifications_db_prefix::{
    FIREBASE_PREFIX, INDEPENDENT_PREFIX, NONE_PREFIX,
};
use crate::constants::USER_ACCESS_TOKEN_ACTIVITY_REFRESH;
use crate::data::user::UserID;
use crate::utils::date_utils::time;

#[derive(Debug, Clone)]
pub enum PushNotificationToken {
    UNDEFINED,
    NONE,
    INDEPENDENT(String),
    FIREBASE(String),
}

impl PushNotificationToken {
    pub fn from_db(token: Option<String>) -> Self {
        match token {
            None => Self::UNDEFINED,
            Some(s) => {
                if s.is_empty() {
                    Self::UNDEFINED
                } else if s.starts_with(NONE_PREFIX) {
                    Self::NONE
                } else if s.starts_with(INDEPENDENT_PREFIX) {
                    Self::INDEPENDENT(s.replacen(INDEPENDENT_PREFIX, "", 1))
                } else {
                    Self::FIREBASE(s.replacen(FIREBASE_PREFIX, "", 1))
                }
            }
        }
    }

    pub fn to_db(&self) -> Option<String> {
        match self {
            PushNotificationToken::UNDEFINED => None,
            PushNotificationToken::NONE => Some(NONE_PREFIX.to_string()),
            PushNotificationToken::INDEPENDENT(k) => Some(format!("{}{}", INDEPENDENT_PREFIX, k)),
            PushNotificationToken::FIREBASE(k) => Some(format!("{}{}", FIREBASE_PREFIX, k)),
        }
    }
}

/// User access token information
///
/// Author : Pierre Hubert
#[derive(Debug, Clone)]
pub struct UserAccessToken {
    pub id: u64,
    pub client_id: u64,
    pub user_id: UserID,
    pub token: String,
    pub last_refresh: u64,
    pub timeout: u64,
    pub push_notifications_token: PushNotificationToken,
}

impl UserAccessToken {
    /// Check out whether access token should be refreshed
    pub fn need_refresh(&self) -> bool {
        self.last_refresh + USER_ACCESS_TOKEN_ACTIVITY_REFRESH.as_secs() < time()
    }
}

impl PartialEq<UserAccessToken> for UserAccessToken {
    fn eq(&self, other: &UserAccessToken) -> bool {
        self.id == other.id
    }
}
