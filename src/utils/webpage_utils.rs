//! # Webpage utilities
//!
//! @author Pierre Hubert

use std::time::Duration;

use webpage::{Webpage, WebpageOptions};

use crate::data::error::Res;
use crate::data::post::PostWebLink;

/// Attempt to get information about a URL given by a user
pub fn get_post_web_link(url: &str) -> Res<PostWebLink> {
    let mut options = WebpageOptions::default();
    options.allow_insecure = false;
    options.follow_location = false;
    options.max_redirections = 0;
    options.timeout = Duration::from_secs(3);
    options.useragent = "OpenGraph Fetcher v1.0".to_string();

    let page = Webpage::from_url(url, options)?;

    Ok(PostWebLink {
        url: url.to_string(),
        title: page
            .html
            .opengraph
            .properties
            .get("title")
            .map(String::to_string),
        description: page
            .html
            .opengraph
            .properties
            .get("description")
            .map(String::to_string),
        image: page
            .html
            .opengraph
            .images
            .first()
            .map(|m| m.url.to_string()),
    })
}
