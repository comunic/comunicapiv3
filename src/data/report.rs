use crate::data::comment::CommentID;
use crate::data::conversation::ConvID;
use crate::data::conversation_message::ConvMessageID;
use crate::data::group_id::GroupID;
use crate::data::post::PostID;
use crate::data::user::UserID;

pub enum ReportTarget {
    Post(PostID),
    Comment(CommentID),
    Conversation(ConvID),
    ConversationMessage(ConvMessageID),
    User(UserID),
    Group(GroupID),
}

impl ReportTarget {
    pub fn to_db(&self) -> (&'static str, u64) {
        match self {
            ReportTarget::Post(i) => ("post", *i),
            ReportTarget::Comment(i) => ("comment", *i),
            ReportTarget::Conversation(i) => ("conversation", i.id()),
            ReportTarget::ConversationMessage(i) => ("conv_msg", *i),
            ReportTarget::User(i) => ("user", i.id()),
            ReportTarget::Group(i) => ("group", i.id()),
        }
    }
}

#[derive(Eq, PartialEq, Copy, Clone)]
pub struct ReportCauseId(&'static str);

impl ReportCauseId {
    pub fn id(&self) -> &'static str {
        self.0
    }
}

pub struct ReportCause {
    _id: &'static str,
    pub label_fr: &'static str,
    pub label_en: &'static str,
}

pub const REPORT_CAUSES: [ReportCause; 5] = [
    ReportCause {
        _id: "spam",
        label_fr: "C'est du spam",
        label_en: "It is spam",
    },
    ReportCause {
        _id: "nudity",
        label_fr: "Scènes de nudité / de pornographie",
        label_en: "Nudity / Sexual activity",
    },
    ReportCause {
        _id: "violence",
        label_fr: "Scènes de violence",
        label_en: "Violence scenes",
    },
    ReportCause {
        _id: "harassment",
        label_fr: "Harcèlement",
        label_en: "Harassment",
    },
    ReportCause {
        _id: "other",
        label_fr: "Autre",
        label_en: "Other",
    },
];

impl ReportCause {
    pub fn id(&self) -> ReportCauseId {
        ReportCauseId(self._id)
    }
}

#[derive(Ord, PartialOrd, Eq, PartialEq, Hash, Copy, Clone)]
pub struct ReportID(pub u64);

pub struct Report {
    pub id: ReportID,
    pub user_id: UserID,
    pub target: ReportTarget,
    pub time: u64,
    pub cause: &'static ReportCause,
    pub comment: Option<String>,
}
