//! # Group information api
//!
//! @author Pierre Hubert
use serde::Serialize;

use crate::data::error::Res;
use crate::data::group::Group;
use crate::data::group_member::GroupMember;
use crate::data::user::UserID;
use crate::helpers::groups_helper;

#[derive(Serialize)]
pub struct GroupApi {
    id: u64,
    name: String,
    icon_url: String,
    number_members: u64,
    visibility: String,
    registration_level: String,
    posts_level: String,
    virtual_directory: String,
    membership: String,
    following: bool,
}

impl GroupApi {
    /// Construct a new group membership instance
    pub fn new(g: &Group, user_id: Option<UserID>) -> Res<GroupApi> {
        let membership = groups_helper::get_membership(&g.id, user_id)?;

        Self::new_with_level(g, &membership)
    }

    /// Construct a new group membership instance
    pub fn new_with_level(g: &Group, membership: &GroupMember) -> Res<GroupApi> {
        Ok(GroupApi {
            id: g.id.id(),
            name: g.name.clone(),
            icon_url: g.get_logo_url(),
            number_members: groups_helper::count_members(&g.id)? as u64,
            visibility: g.visibility.to_api(),
            registration_level: g.registration_level.to_api(),
            posts_level: g.posts_creation_level.to_api(),
            virtual_directory: g.virtual_directory.clone().unwrap_or("null".to_string()),

            membership: membership.level.to_api(),
            following: membership.following,
        })
    }
}
