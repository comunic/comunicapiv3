//! # Admin ID API
//!
//! @author Pierre Hubert

use serde::Serialize;

use crate::data::admin::AdminID;

#[derive(Serialize)]
pub struct AdminIDAPI {
    id: u64,
}

impl AdminIDAPI {
    pub fn new(id: AdminID) -> Self {
        Self { id: id.id() }
    }
}
