//! # Virtual directory helper
//!
//! @author Pierre Hubert

use crate::data::error::ResultBoxError;
use crate::data::group_id::GroupID;
use crate::data::user::UserID;
use crate::helpers::{account_helper, groups_helper};
use crate::utils::virtual_directories_utils::check_virtual_directory;

pub enum VirtualDirType {
    USER,
    GROUP,
}

/// Check the availability of a virtual directory
pub fn check_availability(
    dir: &str,
    target_id: u64,
    target_type: VirtualDirType,
) -> ResultBoxError<bool> {
    if !check_virtual_directory(dir) {
        return Ok(false);
    }

    match target_type {
        VirtualDirType::USER => Ok(account_helper::check_user_directory_availability(
            dir,
            Some(UserID::new(target_id)),
        )? && groups_helper::check_directory_availability(dir, None)?),
        VirtualDirType::GROUP => Ok(
            account_helper::check_user_directory_availability(dir, None)?
                && groups_helper::check_directory_availability(dir, Some(GroupID::new(target_id)))?,
        ),
    }
}
