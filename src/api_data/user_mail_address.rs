//! # Get current user email address result
//!
//! @author Pierre Hubert

use serde::Serialize;

use crate::data::user::User;

#[derive(Serialize)]
pub struct UserMailAddressAPI {
    mail: String,
}

impl UserMailAddressAPI {
    pub fn new(user: &User) -> Self {
        Self {
            mail: user.email.to_string(),
        }
    }
}
