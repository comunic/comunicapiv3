//! # Independent push notification service
//!
//! @author Pierre Hubert

use actix_http::header::HeaderValue;
use actix_web::http::StatusCode;
use reqwest::header::HeaderMap;
use reqwest::Client;
use serde::{Deserialize, Serialize};

use crate::data::config::{conf, IndependentPushService};
use crate::data::error::{ExecError, Res};
use crate::data::push_notification::PushNotification;

#[derive(Deserialize)]
struct CreateTokenResult {
    token: String,
}

#[derive(Serialize)]
struct RemoveTokenRequest {
    client: String,
}

#[derive(Serialize)]
struct PushNotificationRequest {
    clients: Vec<String>,
    id: String,
    title: String,
    body: String,
    image: Option<String>,
    timeout: Option<u64>,
}

#[derive(Serialize)]
struct CancelNotificationRequest {
    clients: Vec<String>,
    id: String,
}

fn svc_conf() -> Res<&'static IndependentPushService> {
    match &conf().independent_push_service {
        Some(c) => Ok(c),
        None => Err(ExecError::boxed_new("Missing push configuration!")),
    }
}

fn create_client() -> Res<Client> {
    let mut headers = HeaderMap::new();
    headers.insert(
        "token",
        HeaderValue::from_str(&svc_conf()?.control_token.clone())?,
    );
    Ok(Client::builder().default_headers(headers).build()?)
}

/// Create a new client token
pub async fn create_token() -> Res<String> {
    let client = create_client()?
        .get(&format!("{}{}", svc_conf()?.control_url, "create_client"))
        .send()
        .await?;

    let response: CreateTokenResult = client.json().await?;

    Ok(response.token)
}

/// Destroy a client token
pub async fn remove_token(t: &str) -> Res {
    let client = create_client()?
        .get(&format!("{}{}", svc_conf()?.control_url, "remove_client"))
        .json(&RemoveTokenRequest {
            client: t.to_string(),
        })
        .send()
        .await?;

    if client.status() == StatusCode::OK {
        return Ok(());
    }

    Err(ExecError::boxed_string(format!(
        "Failed to remove client, got a status {} for service!",
        client.status().as_u16()
    )))
}

/// Push notifications
pub async fn push_notifications(n: &PushNotification, targets: Vec<String>) -> Res {
    let client = create_client()?
        .get(&format!(
            "{}{}",
            svc_conf()?.control_url,
            "push_notification"
        ))
        .json(&PushNotificationRequest {
            clients: targets,
            id: n.id.clone(),
            title: n.title.clone(),
            body: n.body.clone(),
            image: n.image.clone(),
            timeout: n.timeout.clone(),
        })
        .send()
        .await?;

    if client.status() == StatusCode::OK {
        return Ok(());
    }

    Err(ExecError::boxed_string(format!(
        "Failed to push notification, got a status {} for service!",
        client.status().as_u16()
    )))
}

/// Cancel notifications
pub async fn cancel_notifications(id: String, targets: Vec<String>) -> Res {
    let client = create_client()?
        .get(&format!(
            "{}{}",
            svc_conf()?.control_url,
            "remove_notification"
        ))
        .json(&CancelNotificationRequest {
            clients: targets,
            id: id.clone(),
        })
        .send()
        .await?;

    if client.status() == StatusCode::OK {
        return Ok(());
    }

    Err(ExecError::boxed_string(format!(
        "Failed to remove notification, got a status {} for service!",
        client.status().as_u16()
    )))
}
