//! # MP3 utilities
//!
//! @author Pierre Hubert

/// Check out whether a file is a valid MP3 file or not
pub fn is_valid_mp3(file: &[u8]) -> bool {
    let res = mp3_metadata::read_from_slice(file);

    match res {
        Ok(e) => e.duration.as_secs() > 0,
        Err(e) => {
            eprintln!("Error while parsing MP3 file ! {:#?}", e);
            false
        }
    }
}
