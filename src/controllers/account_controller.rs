use percent_encoding::percent_decode_str;

use crate::api_data::account_export_api::AccountExportAPI;
use crate::api_data::current_user_id::CurrentUserID;
use crate::api_data::login_success::LoginSuccess;
use crate::api_data::res_check_email_exists::ResCheckEmailExists;
use crate::api_data::res_check_password_token::ResCheckPasswordToken;
use crate::api_data::res_check_security_answers::ResCheckSecurityAnswers;
use crate::api_data::res_check_security_questions_exists::ResCheckSecurityQuestionsExists;
use crate::api_data::res_get_security_questions::ResGetSecurityQuestions;
use crate::api_data::user_mail_address::UserMailAddressAPI;
use crate::constants::accounts_info_policy::{MIN_FIRST_NAME_LENGTH, MIN_LAST_NAME_LENGTH};
use crate::constants::PASSWORD_RESET_TOKEN_LENGTH;
use crate::data::base_request_handler::BaseRequestHandler;
use crate::data::error::ResultBoxError;
use crate::data::http_request_handler::HttpRequestHandler;
use crate::data::new_account::NewAccount;
use crate::data::user::{User, UserID};
use crate::helpers::{account_helper, user_helper};
use crate::routes::RequestResult;

/// Account controller
///
/// @author Pierre Hubert

impl HttpRequestHandler {
    /// Get information about a user based on its email address specified in the request
    pub fn post_user_info_from_email(&mut self, field: &str) -> ResultBoxError<User> {
        let email = self.post_email(field)?;

        self.ok_or_not_found(
            user_helper::find_user_by_email(&email),
            format!("Requested user in '{}' not found!", email).as_str(),
        )
    }

    /// Get the ID of the user associated with a password reset token
    pub fn post_user_id_from_password_reset_token(
        &mut self,
        field: &str,
    ) -> ResultBoxError<UserID> {
        let token = self.post_string_opt(field, PASSWORD_RESET_TOKEN_LENGTH, true)?;
        let user_id = self.ok_or_forbidden(
            account_helper::get_user_id_from_password_reset_token(&token),
            "Invalid password reset token!",
        )?;

        Ok(user_id)
    }
}

/// Create a new account
pub async fn create(r: &mut HttpRequestHandler) -> RequestResult {
    // Get & check email
    let email = r.post_email("emailAddress")?;
    if account_helper::exists_mail(&email)? {
        r.conflict("This email address already belongs to an account!".to_string())?;
    }

    let new_account = NewAccount {
        first_name: r.post_content("firstName", MIN_FIRST_NAME_LENGTH as usize, true)?,
        last_name: r.post_content("lastName", MIN_LAST_NAME_LENGTH as usize, true)?,
        email,
        password: r.post_string_opt("password", 3, true)?,
    };

    account_helper::create(&new_account)?;

    r.success("Account created!")
}

/// Sign in user
pub async fn login_user(request: &mut HttpRequestHandler) -> RequestResult {
    let email = request.post_email("mail")?;
    let password = request.post_string("password")?;

    if !mailchecker::is_valid(&email) {
        request.bad_request("Invalid email!".to_string())?;
    }

    // Authenticate user
    let token = account_helper::login_user(&email, &password, request.api_client());

    match token {
        Ok(t) => request.set_response(LoginSuccess::new(&t)),
        Err(e) => {
            println!("Error on login: {}", e);
            request.forbidden("Invalid email address / password!".to_string())
        }
    }
}

/// Sign out user
pub async fn logout_user(request: &mut HttpRequestHandler) -> RequestResult {
    if let Some(token) = request.user_access_token() {
        account_helper::destroy_login_tokens(token).await?;
    }

    request.success("User disconnected.")
}

/// Disconnect a user from all his devices
pub async fn disconnect_all_devices(r: &mut HttpRequestHandler) -> RequestResult {
    account_helper::destroy_all_user_tokens(r.user_id_ref()?).await?;

    r.success("Successfully disconnected!")
}

/// Get current user ID
pub async fn user_id(request: &mut HttpRequestHandler) -> RequestResult {
    request.set_response(CurrentUserID::new(&request.user_id()?))
}

/// Get current user email address
pub async fn get_mail(r: &mut HttpRequestHandler) -> RequestResult {
    let user = user_helper::find_user_by_id(r.user_id_ref()?)?;
    r.set_response(UserMailAddressAPI::new(&user))
}

/// Check out whether an email address exists or not
pub async fn exists_mail(r: &mut HttpRequestHandler) -> RequestResult {
    let email = r.post_email("email")?;

    r.set_response(ResCheckEmailExists::new(account_helper::exists_mail(
        &email,
    )?))
}

/// Check out whether a given user has set security questions or not
pub async fn has_security_questions(r: &mut HttpRequestHandler) -> RequestResult {
    let user = r.post_user_info_from_email("email")?;

    r.set_response(ResCheckSecurityQuestionsExists::new(
        user.has_security_questions(),
    ))
}

/// Get the security questions of a user
pub async fn get_security_questions(r: &mut HttpRequestHandler) -> RequestResult {
    let user = r.post_user_info_from_email("email")?;

    if !user.has_security_questions() {
        r.forbidden("Specified user has not defined security questions!".to_string())?;
    }

    r.set_response(ResGetSecurityQuestions::new(&user))
}

/// Check the security answers given by a user to reset a password
pub async fn check_security_answers(r: &mut HttpRequestHandler) -> RequestResult {
    let user = r.post_user_info_from_email("email")?;

    if !user.has_security_questions() {
        r.forbidden("Specified user has not setup security questions !".to_string())?;
    }

    let answers: Vec<String> = r
        .post_string_opt("answers", 3, true)?
        .split("&")
        .map(|s| {
            percent_decode_str(s)
                .decode_utf8_lossy()
                .to_lowercase()
                .trim()
                .to_string()
        })
        .collect::<Vec<String>>();

    if answers.len() != 2 {
        r.forbidden("Please specify two answers!".to_string())?;
    }

    if answers[0] != user.security_answer_1.unwrap().to_lowercase().trim()
        || answers[1] != user.security_answer_2.unwrap().to_lowercase().trim()
    {
        r.forbidden("Invalid security answers!".to_string())?;
    }

    let token = account_helper::generate_password_reset_token(&user.id)?;

    r.set_response(ResCheckSecurityAnswers::new(token))
}

/// Check the validity of a password reset token
pub async fn check_password_reset_token(r: &mut HttpRequestHandler) -> RequestResult {
    let user_id = r.post_user_id_from_password_reset_token("reset_token")?;
    let user = user_helper::find_user_by_id(&user_id)?;

    r.set_response(ResCheckPasswordToken::new(&user))
}

/// Reset user password
pub async fn reset_user_password(r: &mut HttpRequestHandler) -> RequestResult {
    let user_id = r.post_user_id_from_password_reset_token("reset_token")?;
    let new_password = r.post_string_opt("password", 3, true)?;

    account_helper::change_password(&user_id, &new_password)?;

    account_helper::destroy_password_reset_token_for_user(&user_id)?;

    r.success("Password changed!")
}

/// Export account's data
pub async fn export_data(r: &mut HttpRequestHandler) -> RequestResult {
    r.need_user_password("password")?;

    let data = account_helper::export(r.user_id_ref()?)?;

    r.set_response(AccountExportAPI::new(&data)?)
}

/// Delete an account
pub async fn delete_account(r: &mut HttpRequestHandler) -> RequestResult {
    r.need_user_password("password")?;

    if r.user_id()?.id() < 2 {
        // I do not want anybody delete my account...
        r.forbidden("You shall not delete MY account (whoever you are, please note that hacking is bad !!!)".to_string())?;
    }

    account_helper::delete(r.user_id_ref()?).await?;

    r.success("Account deleted.")
}
