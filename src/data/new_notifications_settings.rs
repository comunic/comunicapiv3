//! # New notifications settings
//!
//! @author Pierre Hubert

use crate::data::user::UserID;

pub struct NewNotificationsSettings {
    pub user_id: UserID,
    pub allow_notifications_sound: bool,
    pub allow_conversations: bool,
}
