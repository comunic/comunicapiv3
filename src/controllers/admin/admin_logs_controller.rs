//! # Admin action history (logs) controller
//!
//! @author Pierre Hubert

use crate::api_data::admin::admin_log_api::AdminLogAPI;
use crate::constants::admin::AdminRole;
use crate::data::base_request_handler::BaseRequestHandler;
use crate::data::http_request_handler::HttpRequestHandler;
use crate::helpers::{admin_log_helper, admin_roles_helper};
use crate::routes::RequestResult;

/// Get the list of logs of the user
pub async fn get_list(r: &mut HttpRequestHandler) -> RequestResult {
    let logs = match admin_roles_helper::has_role(r.admin_id()?, AdminRole::ACCESS_ALL_ADMINS_LOGS)?
    {
        true => admin_log_helper::get_all_admin_logs(),
        false => admin_log_helper::get_admin_logs(r.admin_id()?),
    }?;

    r.set_response(logs.iter().map(AdminLogAPI::new).collect::<Vec<_>>())
}
