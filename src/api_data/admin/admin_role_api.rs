//! # Administrator role details api
//!
//! @author Pierre Hubert

use serde::Serialize;

use crate::constants::admin::AdminRoleMetadata;

#[derive(Serialize)]
pub struct AdminRoleDetailsAPI {
    id: &'static str,
    name: &'static str,
    description: &'static str,
}

impl AdminRoleDetailsAPI {
    pub fn new(r: &AdminRoleMetadata) -> Self {
        Self {
            id: r.id,
            name: r.name,
            description: r.description,
        }
    }
}
