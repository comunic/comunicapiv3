//! # New call signal
//!
//! @author Pierre Hubert

use serde::Serialize;

use crate::data::conversation::ConvID;
use crate::data::error::Res;
use crate::data::user::UserID;

#[derive(Serialize)]
#[allow(non_snake_case)]
pub struct NewCallSignalAPI {
    callID: u64,
    peerID: u64,
    data: serde_json::Value,
}

impl NewCallSignalAPI {
    pub fn new(call_id: &ConvID, peer_id: &UserID, data: &str) -> Res<Self> {
        Ok(Self {
            callID: call_id.id(),
            peerID: peer_id.id(),
            data: serde_json::from_str(data)?,
        })
    }
}
