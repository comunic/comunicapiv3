//! # Comment creation result
//!
//! @author Pierre Hubert
use serde::Serialize;

#[derive(Serialize)]
#[allow(non_snake_case)]
pub struct ResCreateComment {
    success: bool,
    commentID: u64,
}

impl ResCreateComment {
    pub fn new(comment_id: u64) -> ResCreateComment {
        ResCreateComment {
            success: true,
            commentID: comment_id,
        }
    }
}
