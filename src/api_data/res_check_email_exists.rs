//! # Check if email exists result
//!
//! @author Pierre Hubert
use serde::Serialize;

#[derive(Serialize)]
pub struct ResCheckEmailExists {
    exists: bool,
}

impl ResCheckEmailExists {
    pub fn new(exists: bool) -> ResCheckEmailExists {
        ResCheckEmailExists { exists }
    }
}
