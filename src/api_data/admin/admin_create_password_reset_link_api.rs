//! # Create password recovery link result
//!
//! @author Pierre Hubert

#[derive(serde::Serialize)]
pub struct AdminCreatedPasswordResetLinkApi {
    url: String,
}

impl AdminCreatedPasswordResetLinkApi {
    pub fn new(url: String) -> Self {
        Self { url }
    }
}
