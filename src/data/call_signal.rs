//! # Call signal
//!
//! @author Pierre Hubert

use crate::data::error::{ExecError, Res};
use crate::data::user::UserID;

pub enum SdpType {
    Offer,
    Answer,
}

pub struct IceCandidate {
    pub candidate: String,
    pub sdp_m_line_index: u64,
    pub sdp_mid: String,
}

pub enum CallSignal {
    /// Session Description Protocol
    SDP(String, SdpType, webrtc_sdp::SdpSession),

    /// ICE candidate
    Candidate(IceCandidate, webrtc_sdp::attribute_type::SdpAttribute),
}

/// New call signal from client
pub struct NewUserCallSignal {
    pub call_hash: String,

    /// This value is set to none if the user who streams content is the same
    /// as the receiver
    pub user_id: Option<UserID>,
    pub signal: CallSignal,
    pub raw_data: String,
}

/// New call signal from RTC relay
pub struct NewRtcRelayMessage {
    pub call_hash: String,
    pub peer_id: String,
    pub data: String,
}

/// Request of a user for a call
pub struct UserCallOfferRequest {
    pub call_hash: String,
    pub user_id: UserID,
}

/// Close call stream
pub struct CloseCallStream {
    pub call_hash: String,
    pub peer_id: Option<UserID>,
}

impl SdpType {
    pub fn from_str(val: &str) -> Res<SdpType> {
        match val {
            "offer" => Ok(SdpType::Offer),
            "answer" => Ok(SdpType::Answer),
            _ => Err(ExecError::boxed_new(&format!(
                "SDP type {} is unknown!",
                val
            ))),
        }
    }
}
