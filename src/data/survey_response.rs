//! # Survey response
//!
//! This structure contains information about a single response to a survey
//!
//! @author Pierre Hubert

use crate::data::user::UserID;

pub struct SurveyResponse {
    pub id: u64,
    pub time_sent: u64,
    pub user_id: UserID,
    pub survey_id: u64,
    pub choice_id: u64,
}
