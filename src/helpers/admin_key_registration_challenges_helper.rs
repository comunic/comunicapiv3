//! # Administrators key registration challenges helper
//!
//! Allows to temporarily stores keys registration challenges
//!
//! @author Pierre Hubert

use std::collections::HashMap;
use std::sync::{Arc, Mutex};

use webauthn_rs::prelude::PasskeyRegistration;

use crate::data::admin::AdminID;
use crate::data::error::Res;

static mut CACHE: Option<Arc<Mutex<HashMap<AdminID, PasskeyRegistration>>>> = None;

/// Initialize this helper's cache
pub fn init() {
    unsafe {
        let map = HashMap::new();
        CACHE = Some(Arc::new(Mutex::new(map)));
    }
}

/// Store a new entry in the cache
pub fn set(admin: AdminID, key: PasskeyRegistration) -> Res {
    let cache = unsafe { CACHE.as_ref().unwrap().lock() };

    cache?.insert(admin, key);

    Ok(())
}

pub fn get(admin: AdminID) -> Res<Option<PasskeyRegistration>> {
    let cache = unsafe { CACHE.as_ref().unwrap().lock() };

    Ok(cache?.remove(&admin))
}
