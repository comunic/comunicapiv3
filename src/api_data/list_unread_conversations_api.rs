//! # List of unread conversations
use serde::Serialize;

use crate::api_data::conversation_api::ConversationAPI;
use crate::api_data::conversation_message_api::ConversationMessageAPI;
use crate::data::conversation::ConvID;
use crate::data::error::Res;
use crate::helpers::conversations_helper;

#[derive(Serialize)]
pub struct UnreadConversationAPI {
    conv: ConversationAPI,
    message: ConversationMessageAPI,
}

impl UnreadConversationAPI {
    /// Construct a new instance
    pub fn new(conv: ConvID) -> Res<UnreadConversationAPI> {
        Ok(UnreadConversationAPI {
            conv: ConversationAPI::new(&conversations_helper::get_single(conv)?),
            message: conversations_helper::get_last_messages(conv, 1)?
                .first()
                .map(ConversationMessageAPI::new)
                .unwrap(),
        })
    }

    /// Turn a list of unread conversation into API conversations
    pub fn for_list(l: &Vec<ConvID>) -> Res<Vec<UnreadConversationAPI>> {
        l.iter().map(|row| Self::new(row.clone())).collect()
    }
}
