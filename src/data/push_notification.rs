//! # Push notification
//!
//! Notification pushed to registered devices
//!
//! @author Pierre Hubert

#[derive(Debug)]
pub struct PushNotification {
    pub id: String,
    pub title: String,
    pub body: String,
    pub image: Option<String>,
    pub timeout: Option<u64>,
}
