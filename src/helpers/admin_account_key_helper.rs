//! # Admin account key helper
//!
//! @author Pierre Hubert

use bcrypt::{hash_with_result, DEFAULT_COST};
use webauthn_rs::prelude::Passkey;

use crate::constants::database_tables_names::ADMIN_KEYS_TABLE;
use crate::data::admin::{AdminID, AdminKey};
use crate::data::error::Res;
use crate::helpers::database;
use crate::utils::date_utils::time;

/// Save a new key in the database
pub fn add_key(id: AdminID, name: &str, key: Passkey, password: String) -> Res<u64> {
    database::InsertQuery::new(ADMIN_KEYS_TABLE)
        .add_admin_id("admin_id", id)
        .add_str("name", name)
        .add_u64("time_add", time())
        .add_str("credential", &serde_json::to_string(&key)?)
        .add_str(
            "password",
            &hash_with_result(password, DEFAULT_COST)?.to_string(),
        )
        .insert_expect_result()
}

/// Get the list of keys of a given admin
pub fn get_admin_keys(id: AdminID) -> Res<Vec<AdminKey>> {
    database::QueryInfo::new(ADMIN_KEYS_TABLE)
        .cond_admin_id("admin_id", id)
        .exec(db_to_admin_key)
}

/// Remove a key from the database
pub fn delete_key(key: AdminKey) -> Res {
    database::DeleteQuery::new(ADMIN_KEYS_TABLE)
        .cond_u64("id", key.id)
        .exec()
}

/// Turn database entry into an AdminKey structure
fn db_to_admin_key(row: &database::RowResult) -> Res<AdminKey> {
    Ok(AdminKey {
        id: row.get_u64("id")?,
        admin_id: row.get_admin_id("admin_id")?,
        name: row.get_str("name")?,
        time_add: row.get_u64("time_add")?,
        key: serde_json::from_str(&row.get_str("credential")?)?,
        password: row.get_optional_str("password")?,
    })
}
