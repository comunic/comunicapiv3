//! # Likes helper
//!
//! Module dedicated to likes management

use crate::constants::database_tables_names::LIKES_TABLE;
use crate::data::error::{Res, ResultBoxError};
use crate::data::user::{User, UserID};
use crate::data::user_like::UserLike;
use crate::helpers::database;
use crate::helpers::database::QueryInfo;
use crate::utils::date_utils::{mysql_date, time, time_to_mysql_date};

#[derive(Copy, Clone)]
pub enum LikeType {
    USER,
    POST,
    COMMENT,
    GROUP,
}

impl LikeType {
    /// Get matching database type
    pub fn to_db_type(&self) -> String {
        match self {
            LikeType::USER => "page".to_string(),
            LikeType::POST => "texte".to_string(),
            LikeType::COMMENT => "commentaire".to_string(),
            LikeType::GROUP => "group".to_string(),
        }
    }
}

/// Count the number of likes over an element
pub fn count(id: u64, kind: LikeType) -> ResultBoxError<usize> {
    QueryInfo::new(LIKES_TABLE)
        .cond_u64("ID_type", id)
        .cond("type", kind.to_db_type().as_ref())
        .exec_count()
}

/// Check if a user likes an element or not
pub fn is_liking(user_id: &UserID, id: u64, kind: LikeType) -> ResultBoxError<bool> {
    if !user_id.is_valid() {
        return Ok(false);
    }

    Ok(QueryInfo::new(LIKES_TABLE)
        .cond_u64("ID_type", id)
        .cond_user_id("ID_personne", user_id)
        .cond("type", kind.to_db_type().as_ref())
        .exec_count()?
        > 0)
}

/// Update like status
pub fn update(user_id: &UserID, liking: bool, id: u64, kind: LikeType) -> ResultBoxError {
    if !liking {
        return database::DeleteQuery::new(LIKES_TABLE)
            .cond_user_id("ID_personne", user_id)
            .cond_u64("ID_type", id)
            .cond_str("type", &kind.to_db_type())
            .exec();
    }

    if is_liking(user_id, id, kind)? {
        return Ok(());
    }

    database::InsertQuery::new(LIKES_TABLE)
        .add_user_id("ID_personne", user_id)
        .add_u64("ID_type", id)
        .add_str("Date_envoi", &mysql_date())
        .add_str("type", &kind.to_db_type())
        .insert_drop_result()
}

/// Delete all the likes associated with a post
pub fn delete_all(id: u64, kind: LikeType) -> ResultBoxError {
    database::DeleteQuery::new(LIKES_TABLE)
        .cond_u64("ID_type", id)
        .cond_str("type", &kind.to_db_type())
        .exec()
}

/// Export all the likes mention of the user
pub fn export_all_user(user_id: &UserID) -> ResultBoxError<Vec<UserLike>> {
    database::QueryInfo::new(LIKES_TABLE)
        .cond_user_id("ID_personne", user_id)
        .exec(db_to_user_like)
}

/// Delete all the likes created by a user
pub fn delete_all_user(user_id: &UserID) -> ResultBoxError {
    database::DeleteQuery::new(LIKES_TABLE)
        .cond_user_id("ID_personne", user_id)
        .exec()
}

/// Delete old user likes
pub fn clean_old_user_likes(user: &User) -> Res {
    if let Some(lifetime) = user.delete_likes_after {
        database::DeleteQuery::new(LIKES_TABLE)
            .cond_user_id("ID_personne", &user.id)
            .set_custom_where("Date_envoi < ?")
            .add_custom_where_arg_str(&time_to_mysql_date(time() - lifetime))
            .exec()?;
    }

    Ok(())
}

/// Turn a database entry into a like entry
fn db_to_user_like(r: &database::RowResult) -> ResultBoxError<UserLike> {
    Ok(UserLike {
        id: r.get_u64("ID")?,
        user_id: r.get_user_id("ID_personne")?,
        time_sent: r.get_date_as_time("Date_envoi")?,
        elem_type: r.get_str("type")?,
        elem_id: r.get_u64("ID_type")?,
    })
}
