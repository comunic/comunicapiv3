//! # Group creation result
//!
//! Result of the creation of a new group

use serde::Serialize;

use crate::data::group_id::GroupID;

#[derive(Serialize)]
pub struct GroupCreationResult {
    success: String,
    id: u64,
}

impl GroupCreationResult {
    /// Construct a new instance of this object
    pub fn new(id: &GroupID) -> GroupCreationResult {
        GroupCreationResult {
            success: "The group has been successfully created!".to_string(),
            id: id.id(),
        }
    }
}
