//! # Background Image Helper
//!
//! This is a legacy module. Background image are not
//! Planned to be implemented anymore

use crate::data::user::UserID;
use crate::utils::user_data_utils::user_data_url;

pub fn get_url(_user_id: &UserID) -> String {
    user_data_url("imgfond/0.jpg")
}
