//! # Call peer interrupted streaming
//!
//! @author Pierre Hubert

use serde::Serialize;

use crate::data::conversation::ConvID;
use crate::data::user::UserID;

#[derive(Serialize)]
#[allow(non_snake_case)]
pub struct CallPeerInterruptedStreamingAPI {
    callID: u64,
    peerID: u64,
}

impl CallPeerInterruptedStreamingAPI {
    pub fn new(call_id: &ConvID, peer_id: &UserID) -> Self {
        Self {
            callID: call_id.id(),
            peerID: peer_id.id(),
        }
    }
}
