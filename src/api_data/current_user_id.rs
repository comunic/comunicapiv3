use serde::{Deserialize, Serialize};

use crate::data::user::UserID;

/// Current user ID
#[derive(Serialize, Deserialize)]
#[allow(non_snake_case)]
pub struct CurrentUserID {
    userID: u64,
}

impl CurrentUserID {
    pub fn new(id: &UserID) -> CurrentUserID {
        CurrentUserID { userID: id.id() }
    }
}
