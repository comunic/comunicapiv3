//! # Friends controller
//!
//! @author Pierre Hubert

use crate::api_data::friend_api::FriendAPI;
use crate::api_data::friendship_status_api::FriendshipStatusAPI;
use crate::data::base_request_handler::BaseRequestHandler;
use crate::data::http_request_handler::HttpRequestHandler;
use crate::data::notification::NotifEventType;
use crate::helpers::{friends_helper, notifications_helper, user_helper};
use crate::routes::RequestResult;

/// Get the list of friends of the current user
pub async fn get_list(r: &mut HttpRequestHandler) -> RequestResult {
    let list = friends_helper::GetFriendsQuery::new(&r.user_id()?).exec()?;

    r.set_response(FriendAPI::from_list(&list))
}

/// Get information about a single friendship
pub async fn get_single_friendship_info(r: &mut HttpRequestHandler) -> RequestResult {
    let friend_id = r.post_user_id("friendID")?;

    let info = friends_helper::GetFriendsQuery::new(&r.user_id()?).get_single_friend(&friend_id);

    let info = r.ok_or_not_found(info, "The friendship was not found!")?;

    r.set_response(FriendAPI::new(&info))
}

/// Get the list of friends of another user
pub async fn get_other_user_list(r: &mut HttpRequestHandler) -> RequestResult {
    let user_id = r.post_user_id("userID")?;

    if !user_helper::can_see_user_page(&r.user_id_or_invalid(), &user_id)? {
        r.forbidden("You can not access this user information!".to_string())?;
    }

    if !user_helper::is_user_friends_list_public(&user_id)? {
        r.forbidden("The friend list of this user is not public!".to_string())?;
    }

    let friends = friends_helper::GetFriendsQuery::new(&user_id)
        .set_only_accepted(true)
        .exec()?;

    r.set_response(
        friends
            .iter()
            .map(|f| f.friend_id.id())
            .collect::<Vec<u64>>(),
    )
}

/// Get the status of a friendship
pub async fn get_status(r: &mut HttpRequestHandler) -> RequestResult {
    let friend_id = r.post_user_id("friendID")?;
    let curr_user_id = r.user_id()?;

    let status = friends_helper::get_status(&curr_user_id, &friend_id)?;

    r.set_response(FriendshipStatusAPI::new(&status))
}

/// Send a new friendship request
pub async fn send_request(r: &mut HttpRequestHandler) -> RequestResult {
    let friend_id = r.post_user_id("friendID")?;

    if friend_id == r.user_id()? {
        r.forbidden("You can not sent friendship request to yourself!".to_string())?;
    }

    let status = friends_helper::get_status(&r.user_id()?, &friend_id)?;

    if status.are_friend {
        r.forbidden("You are already a friend of this person!".to_string())?;
    }

    if status.sent_request || status.received_request {
        r.forbidden("A friendship request is already in progress!".to_string())?;
    }

    friends_helper::send_request(&r.user_id()?, &friend_id)?;

    // Create a notification
    notifications_helper::create_friends_notification(
        r.user_id_ref()?,
        &friend_id,
        NotifEventType::SENT_FRIEND_REQUEST,
    )
    .await?;

    r.success("The friendship request was successfully sent!")
}

/// Cancel a friendship request
pub async fn cancel_request(r: &mut HttpRequestHandler) -> RequestResult {
    let friend_id = r.post_user_id("friendID")?;

    if !friends_helper::sent_request(&r.user_id()?, &friend_id)? {
        r.forbidden("No friendship request was sent to this user!".to_string())?;
    }

    friends_helper::remove_request(&r.user_id()?, &friend_id)?;

    // Delete related notifications
    notifications_helper::delete_all_related_with_friendship_request(r.user_id_ref()?, &friend_id)
        .await?;

    r.success("Friendship request removed!")
}

/// Respond to a friendship request
pub async fn respond_request(r: &mut HttpRequestHandler) -> RequestResult {
    let friend_id = r.post_user_id("friendID")?;
    let accept = r.post_bool("accept")?;

    if !friends_helper::sent_request(&friend_id, r.user_id_ref()?)? {
        r.forbidden("No friendship request was sent by this user!".to_string())?;
    }

    friends_helper::respond_request(r.user_id_ref()?, &friend_id, accept)?;

    // Create a notification
    notifications_helper::create_friends_notification(
        r.user_id_ref()?,
        &friend_id,
        match accept {
            true => NotifEventType::ACCEPTED_FRIEND_REQUEST,
            false => NotifEventType::REJECTED_FRIEND_REQUEST,
        },
    )
    .await?;

    r.set_response("Response to the friendship request successfully saved!")
}

/// Remove a friend from the list
pub async fn remove_friend(r: &mut HttpRequestHandler) -> RequestResult {
    let friend_id = r.post_friend_id("friendID")?;

    friends_helper::remove_friendship(r.user_id_ref()?, &friend_id)?;

    // Delete any related notification
    notifications_helper::delete_all_related_with_friendship_request(r.user_id_ref()?, &friend_id)
        .await?;

    r.success("The friend was removed from the list!")
}

/// Update following status
pub async fn set_following(r: &mut HttpRequestHandler) -> RequestResult {
    let friend_id = r.post_friend_id("friendID")?;
    let follow = r.post_bool("follow")?;

    friends_helper::set_following(r.user_id_ref()?, &friend_id, follow)?;

    r.success("Following status updated!")
}

/// Update post texts authorization status
pub async fn set_can_post_texts(r: &mut HttpRequestHandler) -> RequestResult {
    let friend_id = r.post_friend_id("friendID")?;
    let allow = r.post_bool("allow")?;

    friends_helper::set_can_post_texts(r.user_id_ref()?, &friend_id, allow)?;

    r.success("Updated status!")
}
