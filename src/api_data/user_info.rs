//! API representation of user information
//!
//! @author Pierre Hubert
use serde::Serialize;

use crate::api_data::custom_emoji::CustomEmojiAPI;
use crate::data::error::ResultBoxError;
use crate::data::user::AccountImageVisibility::{COMUNIC_USERS, EVERYONE};
use crate::data::user::{User, UserID, UserPageVisibility};
use crate::helpers::likes_helper::LikeType;
use crate::helpers::{
    background_image_helper, custom_emojies_helper, friends_helper, likes_helper, user_helper,
};
use crate::utils::user_data_utils::user_data_url;

#[derive(Serialize)]
#[allow(non_snake_case)]
pub struct APIUserInfo {
    userID: u64,
    firstName: String,
    lastName: String,
    publicPage: bool,
    openPage: bool,
    virtualDirectory: String,
    accountImage: String,
    customEmojis: Vec<CustomEmojiAPI>,
}

#[derive(Serialize)]
#[allow(non_snake_case)]
pub struct APIAdvancedInfo {
    friend_list_public: bool,
    personnalWebsite: String,
    publicNote: String,
    location: Option<String>,
    email_address: Option<String>,
    noCommentOnHisPage: bool,
    allowPostFromFriendOnHisPage: bool,
    account_creation_time: u64,
    backgroundImage: String,
    number_friends: usize,
    pageLikes: usize,
    user_like_page: bool,
    can_post_texts: bool,

    #[serde(flatten)]
    base_info: APIUserInfo,
}

impl APIUserInfo {
    /// Construct a new API user instance. Note: `user_id` is the ID of the user who makes the
    /// requests, not the user whose we return information about!
    pub fn new(user_id: &Option<UserID>, info: &User) -> ResultBoxError<APIUserInfo> {
        Ok(APIUserInfo {
            userID: info.id.id(),
            firstName: info.first_name.to_string(),
            lastName: info.last_name.to_string(),
            publicPage: info.user_page_visibility != UserPageVisibility::PRIVATE,
            openPage: info.user_page_visibility == UserPageVisibility::OPEN,
            virtualDirectory: info.virtual_directory.clone().unwrap_or(String::new()),
            accountImage: APIUserInfo::get_account_image_url(user_id, info)?,
            customEmojis: custom_emojies_helper::get_list_user(&info.id)?
                .iter()
                .map(|f| CustomEmojiAPI::new(f))
                .collect(),
        })
    }

    /// Get advanced user information
    pub fn new_advanced_info(
        user_id: &Option<UserID>,
        info: &User,
    ) -> ResultBoxError<APIAdvancedInfo> {
        let user = APIUserInfo::new(&user_id, info)?;
        let curr_user_id = user_id.clone().unwrap_or(UserID::new(0));
        let signed_in = user_id.is_some();

        // Check if we can return the number of friends of the user
        let number_friends = if info.public_friends_list || curr_user_id == info.id {
            friends_helper::count_friends(&info.id)?
        } else {
            0
        };

        let likes_page = if signed_in {
            likes_helper::is_liking(&curr_user_id, info.id.id(), LikeType::USER)?
        } else {
            false
        };

        // Set advanced user information
        Ok(APIAdvancedInfo {
            base_info: user,
            friend_list_public: info.public_friends_list,
            personnalWebsite: info.personal_website.clone().unwrap_or(String::new()),
            publicNote: info.public_note.clone().unwrap_or(String::new()),
            location: info.location.clone(),
            email_address: match info.is_email_public {
                true => Some(info.email.clone()),
                false => None,
            },
            noCommentOnHisPage: info.block_comments_on_his_page,
            allowPostFromFriendOnHisPage: info.allow_posts_from_friends,
            account_creation_time: info.account_creation_time,
            backgroundImage: background_image_helper::get_url(&info.id),
            number_friends,
            pageLikes: likes_helper::count(info.id.id(), LikeType::USER)?,
            user_like_page: likes_page,
            can_post_texts: user_helper::can_create_posts(&curr_user_id, &info.id)?,
        })
    }

    /// Get the URL of a specific user account image
    pub fn get_account_image_url(user_id: &Option<UserID>, user: &User) -> ResultBoxError<String> {
        if !user.has_account_image() {
            return Ok(User::default_account_image_url());
        }

        let user_account_image = Ok(user_data_url(user.account_image_path.as_ref().unwrap()));

        if user.account_image_visibility == EVERYONE || user_id == &Some(user.id.clone()) {
            return user_account_image;
        }

        if user_id.is_none() {
            // User is not signed in
            return Ok(User::error_account_image_url());
        }

        if user.account_image_visibility == COMUNIC_USERS
            || friends_helper::are_friend(&user_id.clone().unwrap(), &user.id)?
        {
            return user_account_image;
        }

        Ok(User::error_account_image_url())
    }
}
