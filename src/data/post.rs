//! # Post
//!
//! @author Pierre Hubert

use crate::data::group_id::GroupID;
use crate::data::user::UserID;

#[derive(PartialEq)]
#[allow(non_camel_case_types)]
pub enum PostVisibilityLevel {
    //Posts that can be seen by anyone
    VISIBILITY_PUBLIC = 1,

    //Posts that can be seen by the friends of the user
    VISIBILITY_FRIENDS = 2,

    //Posts that can be seen by the user only
    VISIBILITY_USER = 3,

    //Posts that can be seen by the members of a group (same as friends)
    VISIBILITY_GROUP_MEMBERS = 50,
}

impl PostVisibilityLevel {
    pub fn to_api(&self) -> String {
        match self {
            PostVisibilityLevel::VISIBILITY_PUBLIC => "public",
            PostVisibilityLevel::VISIBILITY_FRIENDS => "friends",
            PostVisibilityLevel::VISIBILITY_USER => "private",
            PostVisibilityLevel::VISIBILITY_GROUP_MEMBERS => "members",
        }
        .to_string()
    }

    pub fn from_api(level: &str) -> PostVisibilityLevel {
        match level {
            "private" => PostVisibilityLevel::VISIBILITY_USER,
            "friends" => PostVisibilityLevel::VISIBILITY_FRIENDS,
            "members" => PostVisibilityLevel::VISIBILITY_GROUP_MEMBERS,
            "public" => PostVisibilityLevel::VISIBILITY_PUBLIC,
            _ => PostVisibilityLevel::VISIBILITY_PUBLIC,
        }
    }
}

/// Post access level (for a given user)
#[allow(non_camel_case_types)]
#[derive(PartialEq, PartialOrd)]
pub enum PostAccessLevel {
    //When a user can't access to a post
    NO_ACCESS = 0,

    //When a user can see a post and perform basic actions such as liking
    BASIC_ACCESS = 1,

    //When a user has intermediate access to the post (delete post)
    INTERMEDIATE_ACCESS = 2,

    //When a user has a full access to the post
    FULL_ACCESS = 3,
}

impl PostAccessLevel {
    pub fn to_api(&self) -> String {
        match self {
            PostAccessLevel::NO_ACCESS => "no-access",
            PostAccessLevel::BASIC_ACCESS => "basic",
            PostAccessLevel::INTERMEDIATE_ACCESS => "intermediate",
            PostAccessLevel::FULL_ACCESS => "full",
        }
        .to_string()
    }
}

#[allow(non_camel_case_types)]
#[derive(Debug)]
pub enum PostPageKind {
    PAGE_KIND_USER(UserID),
    PAGE_KIND_GROUP(GroupID),
}

pub struct PostFile {
    pub path: String,
    pub size: usize,
    pub file_type: Option<String>,
}

pub struct PostWebLink {
    pub url: String,
    pub title: Option<String>,
    pub description: Option<String>,
    pub image: Option<String>,
}

#[allow(non_camel_case_types)]
pub enum PostKind {
    POST_KIND_TEXT,
    POST_KIND_IMAGE(PostFile),
    POST_KIND_WEBLINK(PostWebLink),
    POST_KIND_PDF(PostFile),

    /// Countdown. Value : end time
    POST_KIND_COUNTDOWN(u64),

    POST_KIND_SURVEY,
    POST_KIND_YOUTUBE(String),
}

impl PostKind {
    pub fn to_api(&self) -> String {
        match self {
            PostKind::POST_KIND_TEXT => "text",
            PostKind::POST_KIND_IMAGE(_) => "image",
            PostKind::POST_KIND_WEBLINK(_) => "weblink",
            PostKind::POST_KIND_PDF(_) => "pdf",
            PostKind::POST_KIND_COUNTDOWN(_) => "countdown",
            PostKind::POST_KIND_SURVEY => "survey",
            PostKind::POST_KIND_YOUTUBE(_) => "youtube",
        }
        .to_string()
    }
}

pub type PostID = u64;

pub struct Post {
    pub id: PostID,
    pub user_id: UserID,
    pub time_create: u64,
    pub target_page: PostPageKind,
    pub content: Option<String>,
    pub visibility: PostVisibilityLevel,
    pub kind: PostKind,
}

impl Post {
    /// Get the ID of the target user page, if any
    pub fn user_page_id(&self) -> Option<&UserID> {
        match &self.target_page {
            PostPageKind::PAGE_KIND_USER(id) => Some(id),
            _ => None,
        }
    }

    /// Get the ID of the target group page, if any
    pub fn group_id(&self) -> Option<&GroupID> {
        match &self.target_page {
            PostPageKind::PAGE_KIND_GROUP(id) => Some(id),
            _ => None,
        }
    }

    /// Check out whether a post is targeting a user page or not
    pub fn is_on_user_page(&self) -> bool {
        match &self.target_page {
            PostPageKind::PAGE_KIND_USER(_) => true,
            _ => false,
        }
    }

    /// Check out whether a post is targeting a group page or not
    pub fn is_on_group_page(&self) -> bool {
        matches!(self.target_page, PostPageKind::PAGE_KIND_GROUP(_))
    }
}

#[cfg(test)]
mod tests {
    use crate::data::post::PostAccessLevel;

    #[test]
    fn access_level_coherence() {
        assert!(PostAccessLevel::NO_ACCESS < PostAccessLevel::BASIC_ACCESS);
        assert!(PostAccessLevel::BASIC_ACCESS < PostAccessLevel::INTERMEDIATE_ACCESS);
        assert!(PostAccessLevel::INTERMEDIATE_ACCESS < PostAccessLevel::FULL_ACCESS);
    }
}
