//! # Survey helper
//!
//! @author Pierre Hubert

use crate::constants::database_tables_names::{
    SURVEY_CHOICES_TABLE, SURVEY_INFO_TABLE, SURVEY_RESPONSE_TABLE,
};
use crate::data::error::{ExecError, ResultBoxError};
use crate::data::new_survey::NewSurvey;
use crate::data::survey::{Survey, SurveyChoice};
use crate::data::survey_response::SurveyResponse;
use crate::data::user::UserID;
use crate::helpers::database;
use crate::utils::date_utils::mysql_date;

/// Create a new survey
pub fn create(survey: &NewSurvey) -> ResultBoxError {
    let survey_id = database::InsertQuery::new(SURVEY_INFO_TABLE)
        .add_user_id("ID_utilisateurs", &survey.user_id)
        .add_u64("ID_texte", survey.post_id)
        .add_str("date_creation", &mysql_date())
        .add_str("question", survey.question.as_str())
        .add_legacy_bool("allow_new_choices", survey.allow_new_choices)
        .insert()?
        .ok_or(ExecError::new("Survey was created but no ID was returned!"))?;

    for choice in &survey.choices {
        create_choice(survey_id, choice)?;
    }

    Ok(())
}

/// Delete the survey associated to a post
pub fn delete(post_id: u64) -> ResultBoxError {
    let survey_id = get_id(post_id)?;

    database::DeleteQuery::new(SURVEY_RESPONSE_TABLE)
        .cond_u64("ID_sondage", survey_id)
        .exec()?;

    database::DeleteQuery::new(SURVEY_CHOICES_TABLE)
        .cond_u64("ID_sondage", survey_id)
        .exec()?;

    database::DeleteQuery::new(SURVEY_INFO_TABLE)
        .cond_u64("ID", survey_id)
        .exec()
}

/// Insert a new choice for a survey
pub fn create_choice(survey_id: u64, choice: &str) -> ResultBoxError {
    database::InsertQuery::new(SURVEY_CHOICES_TABLE)
        .add_u64("ID_sondage", survey_id)
        .add_str("date_creation", &mysql_date())
        .add_str("Choix", choice)
        .insert_drop_result()
}

/// Get the ID of a survey associated to a post
pub fn get_id(post_id: u64) -> ResultBoxError<u64> {
    database::QueryInfo::new(SURVEY_INFO_TABLE)
        .cond_u64("ID_texte", post_id)
        .add_field("ID")
        .query_row(|r| r.get_u64("ID"))
}

/// Get information about a survey
pub fn get_info(post_id: u64) -> ResultBoxError<Survey> {
    database::QueryInfo::new(SURVEY_INFO_TABLE)
        .cond_u64("ID_texte", post_id)
        .query_row(db_to_survey)
}

/// Get the choices of a survey
fn get_survey_choices(survey_id: u64) -> ResultBoxError<Vec<SurveyChoice>> {
    database::QueryInfo::new(SURVEY_CHOICES_TABLE)
        .alias("c")
        .set_join_type(database::DatabaseQueryJoinType::LEFT)
        .join(SURVEY_RESPONSE_TABLE, "r", "c.ID = r.ID_sondage_choix")
        .cond_u64("c.ID_sondage", survey_id)
        .set_group_by("c.ID")
        .add_field("c.*")
        .add_field("COUNT(r.ID) AS count_choice")
        .exec(db_to_survey_choice)
}

/// Get the choice of a user for a survey
pub fn get_user_choice(survey_id: u64, user_id: &UserID) -> ResultBoxError<u64> {
    Ok(database::QueryInfo::new(SURVEY_RESPONSE_TABLE)
        .cond_u64("ID_sondage", survey_id)
        .cond_user_id("ID_utilisateurs", user_id)
        .exec(|r| r.get_u64("ID_sondage_choix"))?
        .pop()
        .unwrap_or(0))
}

/// Check out whether a survey exists for a post or not
pub fn exists(post_id: u64) -> ResultBoxError<bool> {
    database::QueryInfo::new(SURVEY_INFO_TABLE)
        .cond_u64("ID_texte", post_id)
        .exec_count()
        .map(|r| r > 0)
}

/// Cancel the response of a user to a survey
pub fn cancel_response(user_id: &UserID, survey_id: u64) -> ResultBoxError {
    database::DeleteQuery::new(SURVEY_RESPONSE_TABLE)
        .cond_u64("ID_sondage", survey_id)
        .cond_user_id("ID_utilisateurs", user_id)
        .exec()
}

/// Check out whether a choice exists or not
pub fn choice_exists(survey_id: u64, choice_id: u64) -> ResultBoxError<bool> {
    database::QueryInfo::new(SURVEY_CHOICES_TABLE)
        .cond_u64("ID_sondage", survey_id)
        .cond_u64("ID", choice_id)
        .exec_count()
        .map(|f| f > 0)
}

/// Save the new response of a user to a survey
pub fn send_response(user_id: &UserID, survey_id: u64, choice_id: u64) -> ResultBoxError {
    database::InsertQuery::new(SURVEY_RESPONSE_TABLE)
        .add_user_id("ID_utilisateurs", user_id)
        .add_u64("ID_sondage", survey_id)
        .add_u64("ID_sondage_choix", choice_id)
        .add_str("date_envoi", &mysql_date())
        .insert_drop_result()
}

/// Block new survey choices from being created
pub fn block_new_choices_creation(survey_id: u64) -> ResultBoxError {
    database::UpdateInfo::new(SURVEY_INFO_TABLE)
        .cond_u64("ID", survey_id)
        .set_legacy_bool("allow_new_choices", false)
        .exec()
}

/// Export all the responses of a given user
pub fn export_all_user_responses(user_id: &UserID) -> ResultBoxError<Vec<SurveyResponse>> {
    database::QueryInfo::new(SURVEY_RESPONSE_TABLE)
        .cond_user_id("ID_utilisateurs", user_id)
        .exec(db_to_survey_response)
}

/// Delete all the responses of a user to surveys
pub fn delete_all_user_responses(user_id: &UserID) -> ResultBoxError {
    database::DeleteQuery::new(SURVEY_RESPONSE_TABLE)
        .cond_user_id("ID_utilisateurs", user_id)
        .exec()
}

/// Turn a database entry into a row object
fn db_to_survey(row: &database::RowResult) -> ResultBoxError<Survey> {
    let survey_id = row.get_u64("ID")?;

    Ok(Survey {
        id: survey_id,
        user_id: row.get_user_id("ID_utilisateurs")?,
        time_create: row.get_date_as_time("date_creation")?,
        post_id: row.get_u64("ID_texte")?,
        question: row.get_str("question")?,
        choices: get_survey_choices(survey_id)?,
        allow_new_choices: row.get_legacy_bool("allow_new_choices")?,
    })
}

/// Turn a database row into a survey choice
fn db_to_survey_choice(row: &database::RowResult) -> ResultBoxError<SurveyChoice> {
    Ok(SurveyChoice {
        id: row.get_u64("ID")?,
        name: row.get_str("Choix")?,
        count: row.get_u64("count_choice")?,
    })
}

/// Turn a database row into a SurveyResponse object
fn db_to_survey_response(row: &database::RowResult) -> ResultBoxError<SurveyResponse> {
    Ok(SurveyResponse {
        id: row.get_u64("ID")?,
        time_sent: row.get_date_as_time("date_envoi")?,
        user_id: row.get_user_id("ID_utilisateurs")?,
        survey_id: row.get_u64("ID_sondage")?,
        choice_id: row.get_u64("ID_sondage_choix")?,
    })
}
