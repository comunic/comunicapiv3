//! # General settings
//!
//! @author Pierre Hubert

use crate::data::user::{UserID, UserPageVisibility};

pub struct GeneralSettings {
    pub id: UserID,
    pub first_name: String,
    pub last_name: String,
    pub page_status: UserPageVisibility,
    pub block_comments: bool,
    pub allow_posts_from_friends: bool,
    pub friends_list_public: bool,
    pub email_public: bool,
    pub personal_website: Option<String>,
    pub virtual_directory: Option<String>,
    pub allow_mails: bool,
    pub public_note: Option<String>,
    pub location: Option<String>,
}
