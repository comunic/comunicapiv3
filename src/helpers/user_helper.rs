use crate::constants::database_tables_names::USERS_TABLE;
use crate::data::error::{Res, ResultBoxError};
use crate::data::user::UserPageVisibility::PUBLIC;
use crate::data::user::{AccountImageVisibility, User, UserID, UserPageVisibility};
use crate::helpers::friends_helper::are_friend;
use crate::helpers::{database, friends_helper};

/// User helper
///
/// @author Pierre Hubert

/// Get & return information about a user based on its ID
pub fn find_user_by_id(id: &UserID) -> ResultBoxError<User> {
    database::QueryInfo::new(USERS_TABLE)
        .cond_user_id("ID", id)
        .query_row(db_to_user)
}

/// Get & return information about a user based on his email
pub fn find_user_by_email(email: &str) -> ResultBoxError<User> {
    database::QueryInfo::new(USERS_TABLE)
        .cond("mail", email)
        .query_row(db_to_user)
}

/// Get & return information about a user based on his virtual directory
pub fn find_user_by_virtual_directory(dir: &str) -> ResultBoxError<User> {
    database::QueryInfo::new(USERS_TABLE)
        .cond("sous_repertoire", dir)
        .query_row(db_to_user)
}

/// Get the entire list of Comunic users
pub fn get_all_users() -> Res<Vec<User>> {
    database::QueryInfo::new(USERS_TABLE).exec(db_to_user)
}

/// Execute query & return result
fn db_to_user(res: &database::RowResult) -> ResultBoxError<User> {
    // Page status
    let page_status = if res.get_int64("pageouverte")? == 1 {
        UserPageVisibility::OPEN
    } else if res.get_int64("public")? == 1 {
        UserPageVisibility::PUBLIC
    } else {
        UserPageVisibility::PRIVATE
    };

    // Account image visibility
    let account_image_visibility = match res.get_str("account_image_visibility")?.as_ref() {
        "friends" => AccountImageVisibility::FRIENDS,
        "comunic_users" => AccountImageVisibility::COMUNIC_USERS,
        "everyone" | _ => AccountImageVisibility::EVERYONE,
    };

    Ok(User {
        id: res.get_user_id("ID")?,
        email: res.get_str("mail")?,
        password: res.get_str("password")?,
        first_name: res.get_str("prenom")?,
        last_name: res.get_str("nom")?,
        user_page_visibility: page_status,
        last_activity: res.get_u64("last_activity")?,
        virtual_directory: res.get_optional_str("sous_repertoire")?,
        account_image_path: res.get_optional_str("account_image_path")?,
        account_image_visibility,
        public_friends_list: res.get_legacy_bool("liste_amis_publique")?,
        is_email_public: res.get_legacy_bool("is_email_public")?,
        personal_website: res.get_optional_str("site_web")?,
        public_note: res.get_optional_str("public_note")?,
        location: res.get_optional_str("location")?,
        block_comments_on_his_page: res.get_legacy_bool("bloquecommentaire")?,
        allow_posts_from_friends: res.get_legacy_bool("autoriser_post_amis")?,
        account_creation_time: res.get_date_as_time("date_creation")?,
        allow_mails: res.get_legacy_bool("autorise_mail")?,
        lang: res.get_str("lang")?,

        // Security questions
        security_question_1: res.get_optional_str("question1")?,
        security_answer_1: res.get_optional_str("reponse1")?,
        security_question_2: res.get_optional_str("question2")?,
        security_answer_2: res.get_optional_str("reponse2")?,

        // Data cleanup
        delete_account_after: res.get_optional_positive_u64("delete_account_after")?,
        delete_notifications_after: res.get_optional_positive_u64("delete_notifications_after")?,
        delete_comments_after: res.get_optional_positive_u64("delete_comments_after")?,
        delete_posts_after: res.get_optional_positive_u64("delete_posts_after")?,
        delete_conversation_messages_after: res
            .get_optional_positive_u64("delete_conversation_messages_after")?,
        delete_likes_after: res.get_optional_positive_u64("delete_likes_after")?,

        // Conversation settings
        allow_notif_sound: res.get_legacy_bool("allow_notif_sound")?,
        allow_notif_conv: res.get_legacy_bool("allow_notif_conv")?,
    })
}

/// Check out whether a given id maps to a user or not
pub fn exists(id: &UserID) -> ResultBoxError<bool> {
    Ok(database::QueryInfo::new(USERS_TABLE)
        .cond_user_id("ID", id)
        .exec_count()?
        > 0)
}

/// Check if a given user can see another user's page
pub fn can_see_user_page(user_id: &UserID, target_user: &UserID) -> ResultBoxError<bool> {
    if user_id == target_user {
        return Ok(true);
    }

    let visibility = find_user_by_id(target_user)?.user_page_visibility;

    // Open page = OK
    if visibility == UserPageVisibility::OPEN {
        return Ok(true);
    }

    // The user need to be signed in
    if user_id.id() <= 0 {
        return Ok(false);
    }

    // Public Page = OK for signed in users
    if visibility == PUBLIC {
        return Ok(true);
    }

    // Check if the users are friends
    if !are_friend(user_id, target_user)? {
        return Ok(false);
    }

    return Ok(true);
}

/// Check out whether a user allow posts on his page or not
pub fn allow_posts_on_his_page(user_id: &UserID) -> ResultBoxError<bool> {
    Ok(find_user_by_id(user_id)?.allow_posts_from_friends)
}

/// Check out whether a user has blocked comments on his / her page
pub fn allow_comments(user_id: &UserID) -> ResultBoxError<bool> {
    Ok(!find_user_by_id(user_id)?.block_comments_on_his_page)
}

/// Check out whether the friends list of a user is public or not
pub fn is_user_friends_list_public(user_id: &UserID) -> ResultBoxError<bool> {
    Ok(find_user_by_id(user_id)?.public_friends_list)
}

/// Check out if a user can create posts on another user page
pub fn can_create_posts(user_id: &UserID, target_id: &UserID) -> ResultBoxError<bool> {
    // Login required
    if !user_id.is_valid() {
        return Ok(false);
    }

    // A user can always create posts on his page
    if user_id == target_id {
        return Ok(true);
    }

    // User must be able to see the page
    if !can_see_user_page(user_id, target_id)? {
        return Ok(false);
    }

    // Check if user allow posts on his page
    if !allow_posts_on_his_page(target_id)? {
        return Ok(false);
    }

    // Check if the friendship of the user allows him to create posts
    if !friends_helper::can_post_texts(user_id, target_id)? {
        return Ok(false);
    }

    Ok(true)
}

/// Search for user in the database
pub fn search_user(query: &str, limit: u64) -> ResultBoxError<Vec<UserID>> {
    let query = format!("%{}%", query.replace(" ", "%"));

    database::QueryInfo::new(USERS_TABLE)
        .add_field("ID")
        .set_custom_where("(nom LIKE ?) || (prenom LIKE ?) || (CONCAT(prenom, '%', nom) LIKE ?) || (CONCAT(nom, '%', prenom) LIKE ?)")
        .add_custom_where_argument_str(&query)
        .add_custom_where_argument_str(&query)
        .add_custom_where_argument_str(&query)
        .add_custom_where_argument_str(&query)
        .set_limit(limit)
        .exec(|row| row.get_user_id("ID"))
}

/// Search for user in the database / admin version
pub fn search_user_admin(name: &str, email: &str, limit: u64) -> ResultBoxError<Vec<User>> {
    let name_query = format!("%{}%", name.replace(" ", "%"));

    let mut custom_where = String::new();
    let mut query = database::QueryInfo::new(USERS_TABLE);

    if !name.is_empty() {
        custom_where.push_str("(nom LIKE ?) || (prenom LIKE ?) || (CONCAT(prenom, '%', nom) LIKE ?) || (CONCAT(nom, '%', prenom) LIKE ?)");

        query = query
            .add_custom_where_argument_str(&name_query)
            .add_custom_where_argument_str(&name_query)
            .add_custom_where_argument_str(&name_query)
            .add_custom_where_argument_str(&name_query);
    }

    if !email.is_empty() {
        if !custom_where.is_empty() {
            custom_where.push_str(" OR ");
        }
        custom_where.push_str("(mail LIKE ?)");
        query = query.add_custom_where_argument_str(&format!("%{}%", email))
    }

    query
        .set_custom_where(&custom_where)
        .set_limit(limit)
        .exec(db_to_user)
}
