//! # Left call message
//!
//! @author Pierre Hubert

use serde::Serialize;

use crate::data::conversation::ConvID;
use crate::data::user::UserID;

/// This structure is used to give information about a user who left a call
#[derive(Serialize)]
#[allow(non_snake_case)]
pub struct LeftCallMessage {
    callID: u64,
    userID: u64,
}

impl LeftCallMessage {
    pub fn new(call_id: &ConvID, user_id: &UserID) -> Self {
        Self {
            callID: call_id.id(),
            userID: user_id.id(),
        }
    }
}
