//! # Account image settings API

use serde::Serialize;

use crate::constants::DEFAULT_ACCOUNT_IMAGE;
use crate::data::user::User;
use crate::utils::user_data_utils::user_data_url;

#[derive(Serialize)]
#[allow(non_snake_case)]
pub struct AccountImageSettingsAPI {
    has_image: bool,
    image_url: String,
    visibility: String,
}

impl AccountImageSettingsAPI {
    pub fn new(user: &User) -> AccountImageSettingsAPI {
        AccountImageSettingsAPI {
            has_image: user.has_account_image(),
            image_url: user_data_url(
                user.account_image_path
                    .as_ref()
                    .unwrap_or(&DEFAULT_ACCOUNT_IMAGE.to_string()),
            ),
            visibility: user.account_image_visibility.to_api(),
        }
    }
}
