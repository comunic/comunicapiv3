//! # Friend information
//!
//! @author Pierre Hubert

use crate::controllers::user_ws_controller;
use crate::data::user::UserID;
use crate::utils::date_utils::time;

#[derive(PartialEq, Eq)]
pub struct Friend {
    pub friend_id: UserID,
    pub accepted: bool,
    pub following: bool,
    pub last_activity_time: u64,
    pub can_post_texts: bool,
}

impl Friend {
    /// Get the last activity of this friend. If this friend is both connected and not incognito,
    /// current time will be returned instead of time stored in database
    pub fn computed_last_activity_time(&self) -> u64 {
        if user_ws_controller::is_user_connected_not_incognito(&self.friend_id) {
            time()
        } else {
            self.last_activity_time
        }
    }
}
