//! # Number of unread notifications
//!
//! @author Pierre Hubert

use serde::Serialize;

#[derive(Serialize)]
pub struct ResNumberUnreadNotifications {
    number: u64,
}

impl ResNumberUnreadNotifications {
    pub fn new(number: u64) -> ResNumberUnreadNotifications {
        ResNumberUnreadNotifications { number }
    }
}
