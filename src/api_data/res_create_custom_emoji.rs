//! # Create custom emoji result
//!
//! @author Pierre Hubert

use serde::Serialize;

#[allow(non_snake_case)]
#[derive(Serialize)]
pub struct ResCreateCustomEmoji {
    emojiID: u64,
}

impl ResCreateCustomEmoji {
    pub fn new(emoji_id: u64) -> ResCreateCustomEmoji {
        ResCreateCustomEmoji { emojiID: emoji_id }
    }
}
