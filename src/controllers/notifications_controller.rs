//! # Notifications controller
//!
//! @author Pierre Hubert

use crate::api_data::notification_api::NotificationAPI;
use crate::api_data::res_count_all_unreads::ResCountAllUnread;
use crate::api_data::res_number_unread_notifications::ResNumberUnreadNotifications;
use crate::controllers::user_ws_controller;
use crate::data::base_request_handler::BaseRequestHandler;
use crate::data::error::{Res, ResultBoxError};
use crate::data::http_request_handler::HttpRequestHandler;
use crate::data::notification::Notification;
use crate::data::user::UserID;
use crate::data::user_ws_message::UserWsMessage;
use crate::helpers::events_helper::Event;
use crate::helpers::{conversations_helper, events_helper, friends_helper, notifications_helper};
use crate::routes::RequestResult;

impl HttpRequestHandler {
    /// Get the id of a notification included in the request
    pub fn post_notif_id(&mut self, name: &str) -> ResultBoxError<Notification> {
        let notif_id = self.post_u64(name)?;

        let notif = self.ok_or_not_found(
            notifications_helper::get_single(notif_id),
            "Specified notification not found!",
        )?;

        if notif.dest_user_id != self.user_id()? {
            self.forbidden("You are not allowed to access this notification!".to_string())?;
        }

        Ok(notif)
    }
}

/// Count the number of unread notifications
pub async fn count_unread(r: &mut HttpRequestHandler) -> RequestResult {
    let number = notifications_helper::count_unread(r.user_id_ref()?)?;
    r.set_response(ResNumberUnreadNotifications::new(number))
}

/// Count the number of unread notifications
pub async fn count_all_news(r: &mut HttpRequestHandler) -> RequestResult {
    let notifications = notifications_helper::count_unread(r.user_id_ref()?)?;
    let conversations = conversations_helper::count_unread_for_user(r.user_id_ref()?)?;
    let friends_requests = match r.post_bool_opt("friends_request", false) {
        true => Some(friends_helper::count_requests(r.user_id_ref()?)?),
        false => None,
    };

    r.set_response(ResCountAllUnread::new(
        notifications,
        conversations as u64,
        friends_requests,
    ))
}

/// Get the list of unread notifications
pub async fn get_list_unread(r: &mut HttpRequestHandler) -> RequestResult {
    let list = notifications_helper::get_list_unread(r.user_id_ref()?)?;

    r.set_response(NotificationAPI::for_list(&list))
}

/// Mark a notification as seen
pub async fn mark_seen(r: &mut HttpRequestHandler) -> RequestResult {
    let notif = r.post_notif_id("notifID")?;
    let delete_similar = r.post_bool_opt("delete_similar", false);

    let mut notif = notif.into_partial();

    // Check if we are targeting a precise notification or an undetermined number of similar
    // notifications
    if delete_similar {
        notif.id = None;
        notif.from_user_id = None;
    }

    notifications_helper::delete(&notif).await?;

    r.success("Notification deleted")
}

/// Delete all the notifications of the current user
pub async fn delete_all(r: &mut HttpRequestHandler) -> RequestResult {
    notifications_helper::delete_all_user(r.user_id_ref()?).await?;

    r.success("Notifications deleted.")
}

/// Send new notifications number to a specific user
pub fn send_new_notifications_number(user_id: &UserID) -> Res {
    if !user_ws_controller::is_user_connected(user_id) {
        return Ok(());
    }

    user_ws_controller::send_message_to_user(
        &UserWsMessage::no_id_message(
            "number_notifs",
            notifications_helper::count_unread(user_id)?,
        )?,
        user_id,
    )
}

/// Events handler
pub fn handle_event(e: &events_helper::Event) -> Res {
    match e {
        Event::UpdatedNotificationsNumber(users) => {
            for user in users.iter() {
                send_new_notifications_number(user)?;
            }
        }
        _ => {}
    }

    Ok(())
}
