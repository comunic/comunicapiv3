//! # Groups controller
//!
//! @author Pierre Hubert

use std::collections::HashMap;

use crate::api_data::advanced_group_api::AdvancedGroupApi;
use crate::api_data::group_api::GroupApi;
use crate::api_data::group_member_api::GroupMemberAPI;
use crate::api_data::res_change_group_logo::ResChangeGroupLogo;
use crate::api_data::res_create_conversation_for_group::ResCreateConversationForGroup;
use crate::api_data::res_create_group::GroupCreationResult;
use crate::constants::{DEFAULT_GROUP_LOGO, PATH_GROUPS_LOGOS};
use crate::data::base_request_handler::BaseRequestHandler;
use crate::data::conversation::Conversation;
use crate::data::error::Res;
use crate::data::group::{
    Group, GroupAccessLevel, GroupPostsCreationLevel, GroupRegistrationLevel, GroupVisibilityLevel,
};
use crate::data::group_id::GroupID;
use crate::data::group_member::{GroupMember, GroupMembershipLevel};
use crate::data::http_request_handler::HttpRequestHandler;
use crate::data::new_group::NewGroup;
use crate::data::notification::NotifEventType;
use crate::helpers::virtual_directory_helper::VirtualDirType;
use crate::helpers::{
    conversations_helper, groups_helper, notifications_helper, virtual_directory_helper,
};
use crate::routes::RequestResult;
use crate::utils::date_utils::time;

impl HttpRequestHandler {
    /// Get membership level for a conversation
    pub fn post_group_membership_level_for_conversation(
        &mut self,
        name: &str,
    ) -> Res<GroupMembershipLevel> {
        let level = GroupMembershipLevel::from_api(&self.post_string(name)?);

        if !level.is_at_least_member() {
            self.bad_request("Specified membership level is not enough!".to_string())?;
        }

        Ok(level)
    }

    /// Get a group conversation referenced in the request, for which current user is an admin
    fn post_group_conv_admin(&mut self, name: &str) -> Res<Conversation> {
        let conv_membership = self.post_conv_admin(name)?;
        let conv = conversations_helper::get_single(conv_membership.conv_id)?;

        if !conv.is_linked_to_group() {
            self.bad_request(
                "This conversation is not linked to a group, it can not be updated this way!"
                    .to_string(),
            )?;
        }

        Ok(conv)
    }
}

/// Create a new group
pub async fn create(r: &mut HttpRequestHandler) -> RequestResult {
    let new_group = NewGroup {
        name: r.post_string_opt("name", 3, true)?,
        owner_id: r.user_id()?,
    };

    let group_id = groups_helper::create(&new_group).await?;

    r.set_response(GroupCreationResult::new(&group_id))
}

/// Get the list of groups of the current user
pub async fn get_list_user(r: &mut HttpRequestHandler) -> RequestResult {
    let list = groups_helper::get_list_user(r.user_id_ref()?, false)?
        .iter()
        .map(|f| f.id())
        .collect::<Vec<u64>>();

    r.set_response(list)
}

/// Get information about a single group
pub async fn get_info_single(r: &mut HttpRequestHandler) -> RequestResult {
    let group_id = r.post_group_id_with_access("id", GroupAccessLevel::LIMITED_ACCESS)?;
    let group = groups_helper::get_info(&group_id)?;

    r.set_response(GroupApi::new(&group, r.user_id_opt())?)
}

/// Get information about multiple users
pub async fn get_info_multiple(r: &mut HttpRequestHandler) -> RequestResult {
    let groups_id = r.post_numbers_list("list", 1)?;

    let mut list = HashMap::new();

    for id in groups_id {
        let id = GroupID::new(id as u64);

        if !groups_helper::exists(&id)?
            || groups_helper::get_access_level(&id, r.user_id_opt())?
                < GroupAccessLevel::LIMITED_ACCESS
        {
            r.not_found(format!("Group {} not found!", id.id()))?;
        }

        let group = groups_helper::get_info(&id)?;

        list.insert(id.id().to_string(), GroupApi::new(&group, r.user_id_opt())?);
    }

    r.set_response(list)
}

/// Get advanced information about a user
pub async fn get_advanced_info(r: &mut HttpRequestHandler) -> RequestResult {
    let group_id = r.post_group_id_with_access("id", GroupAccessLevel::VIEW_ACCESS)?;
    let group = groups_helper::get_info(&group_id)?;

    r.set_response(AdvancedGroupApi::new(&group, r.user_id_opt())?)
}

/// Get the settings of the group
pub async fn get_settings(r: &mut HttpRequestHandler) -> RequestResult {
    let group_id = r.post_group_id_with_access("id", GroupAccessLevel::ADMIN_ACCESS)?;
    let group = groups_helper::get_info(&group_id)?;

    // For now, this method is the same as the get advanced info method,
    // but this might change in the future...
    r.set_response(AdvancedGroupApi::new(&group, r.user_id_opt())?)
}

/// Set new settings to the group
pub async fn set_settings(r: &mut HttpRequestHandler) -> RequestResult {
    let group_id = r.post_group_id_with_access("id", GroupAccessLevel::ADMIN_ACCESS)?;

    let new_settings = Group {
        id: group_id.clone(),
        name: r.post_string_without_html("name", 3, true)?,
        visibility: GroupVisibilityLevel::from_api(&r.post_string("visibility")?),
        registration_level: GroupRegistrationLevel::from_api(&r.post_string("registration_level")?),
        posts_creation_level: GroupPostsCreationLevel::from_api(&r.post_string("posts_level")?),
        is_members_list_public: r.post_bool("is_members_list_public")?,
        logo: None,
        virtual_directory: r.post_checked_virtual_directory_opt(
            "virtual_directory",
            group_id.id(),
            VirtualDirType::GROUP,
        )?,
        time_create: 0,
        description: r.post_string_without_html_opt("description", 0)?,
        url: r.post_url_opt("url", false)?,
    };

    groups_helper::set_settings(&new_settings)?;

    r.success("Group settings have been successfully updated!")
}

/// Check out whether a virtual directory is available for a group or not
pub async fn check_virtual_dir(r: &mut HttpRequestHandler) -> RequestResult {
    let group_id = r.post_group_id_with_access("groupID", GroupAccessLevel::ADMIN_ACCESS)?;
    let dir = r.post_virtual_directory("directory")?;

    if !virtual_directory_helper::check_availability(&dir, group_id.id(), VirtualDirType::GROUP)? {
        r.forbidden("The requested virtual directory seems not to be available!".to_string())?;
    }

    r.success("Requested virtual directory seems to be available!")
}

/// Change a group's logo
pub async fn upload_logo(r: &mut HttpRequestHandler) -> RequestResult {
    let group_id = r.post_group_id_with_access("id", GroupAccessLevel::ADMIN_ACCESS)?;

    if !r.has_file("logo") {
        r.bad_request("Did not receive logo!".to_string())?;
    }

    groups_helper::delete_logo(&group_id)?;

    let logo_path = r.save_post_image("logo", PATH_GROUPS_LOGOS, 500, 500)?;

    groups_helper::set_logo_path(&group_id, Some(logo_path.clone()))?;

    r.set_response(ResChangeGroupLogo::new(&logo_path))
}

/// Delete a group's logo
pub async fn delete_logo(r: &mut HttpRequestHandler) -> RequestResult {
    let group_id = r.post_group_id_with_access("id", GroupAccessLevel::ADMIN_ACCESS)?;

    groups_helper::delete_logo(&group_id)?;

    r.set_response(ResChangeGroupLogo::new(DEFAULT_GROUP_LOGO))
}

/// Create a new group's conversation
pub async fn create_conversation(r: &mut HttpRequestHandler) -> RequestResult {
    let group = r.post_group_id_with_access("group_id", GroupAccessLevel::ADMIN_ACCESS)?;
    let min_membership_level =
        r.post_group_membership_level_for_conversation("min_membership_level")?;
    let name = r.post_string("name")?;

    let conv_id =
        conversations_helper::create_conversation_for_group(group, min_membership_level, &name)
            .await?;

    r.set_response(ResCreateConversationForGroup::new(conv_id))
}

/// Update a group's conversation visibility
pub async fn set_conversation_visibility(r: &mut HttpRequestHandler) -> RequestResult {
    let conv = r.post_group_conv_admin("conv_id")?;
    let min_level = r.post_group_membership_level_for_conversation("min_membership_level")?;

    conversations_helper::set_min_group_conversation_membership_level(conv.id, min_level).await?;

    r.ok()
}

/// Delete a group's conversation
pub async fn delete_conversation(r: &mut HttpRequestHandler) -> RequestResult {
    let conv = r.post_group_conv_admin("conv_id")?;

    conversations_helper::delete_conversation(&conv).await?;

    r.ok()
}

/// Get the list of members of a group
pub async fn get_members(r: &mut HttpRequestHandler) -> RequestResult {
    let group_id = r.post_group_id("id")?;
    let group_access_level = groups_helper::get_access_level(&group_id, r.user_id_opt())?;
    let group = groups_helper::get_info(&group_id)?;

    if group_access_level < GroupAccessLevel::VIEW_ACCESS
        || (!group.is_members_list_public
            && group_access_level < GroupAccessLevel::MODERATOR_ACCESS)
    {
        r.forbidden("You can not access the list of members of this group!".to_string())?;
    }

    let members = groups_helper::get_list_members(&group_id)?;

    r.set_response(GroupMemberAPI::for_list(&members))
}

/// Cancel an invitation sent to a user
pub async fn cancel_invitation(r: &mut HttpRequestHandler) -> RequestResult {
    let group_id = r.post_group_id_with_access("groupID", GroupAccessLevel::MODERATOR_ACCESS)?;
    let user_id = r.post_user_id("userID")?;

    if groups_helper::get_membership_level(&group_id, Some(user_id.clone()))?
        != GroupMembershipLevel::INVITED
    {
        r.forbidden("This user has not been invited to join this group!".to_string())?;
    }

    groups_helper::delete_member(&group_id, &user_id).await?;

    // Delete related notifications
    notifications_helper::delete_all_related_to_group_membership_notifications(&user_id, &group_id)
        .await?;

    r.success("Membership invitation has been cancelled!")
}

/// Invite a user to join a group
pub async fn invite_user(r: &mut HttpRequestHandler) -> RequestResult {
    let group_id = r.post_group_id_with_access("group_id", GroupAccessLevel::MODERATOR_ACCESS)?;
    let user_id = r.post_user_id("userID")?;

    if groups_helper::get_membership_level(&group_id, Some(user_id.clone()))?
        != GroupMembershipLevel::VISITOR
    {
        r.bad_request("The user is not a visitor of the group!".to_string())?;
    }

    groups_helper::send_invitation(&group_id, &user_id).await?;

    // Send a notification
    notifications_helper::create_group_membership_notification(
        &user_id,
        Some(r.user_id_ref()?),
        &group_id,
        NotifEventType::SENT_GROUP_MEMBERSHIP_INVITATION,
    )
    .await?;

    r.success("The user has been successfully invited to join the group!")
}

/// Respond to a user invitation
pub async fn respond_invitation(r: &mut HttpRequestHandler) -> RequestResult {
    let group_id = r.post_group_id_with_access("id", GroupAccessLevel::LIMITED_ACCESS)?;
    let accept = r.post_bool("accept")?;

    if !groups_helper::received_invitation(&group_id, &r.user_id()?)? {
        r.not_found("Invitation not found!".to_string())?
    }

    groups_helper::respond_invitation(&group_id, &r.user_id()?, accept).await?;

    if accept {
        groups_helper::set_following(&group_id, &r.user_id()?, true)?;
    }

    // Create a notification
    notifications_helper::create_group_membership_notification(
        r.user_id_ref()?,
        None,
        &group_id,
        match accept {
            true => NotifEventType::ACCEPTED_GROUP_MEMBERSHIP_INVITATION,
            false => NotifEventType::REJECTED_GROUP_MEMBERSHIP_INVITATION,
        },
    )
    .await?;

    r.success("Response to the invitation was successfully saved!")
}

/// Send a request to join a group
pub async fn send_request(r: &mut HttpRequestHandler) -> RequestResult {
    let group_id = r.post_group_id_with_access("id", GroupAccessLevel::LIMITED_ACCESS)?;

    if groups_helper::get_membership_level(&group_id, r.user_id_opt())?
        != GroupMembershipLevel::VISITOR
    {
        r.forbidden("You are not currently a visitor of the group!".to_string())?;
    }

    let group = groups_helper::get_info(&group_id)?;

    let level = match group.registration_level {
        GroupRegistrationLevel::OPEN_REGISTRATION => GroupMembershipLevel::MEMBER,
        GroupRegistrationLevel::MODERATED_REGISTRATION => GroupMembershipLevel::PENDING,
        GroupRegistrationLevel::CLOSED_REGISTRATION => {
            r.forbidden(
                "You are not authorized to send a registration request for this group!".to_string(),
            )?;
            unreachable!();
        }
    };

    groups_helper::insert_member(&GroupMember {
        id: 0,
        user_id: r.user_id()?,
        group_id: group_id.clone(),
        time_create: time(),
        level,
        following: true,
    })
    .await?;

    // Send a notification, if required
    if matches!(
        group.registration_level,
        GroupRegistrationLevel::MODERATED_REGISTRATION
    ) {
        notifications_helper::create_group_membership_notification(
            r.user_id_ref()?,
            None,
            &group_id,
            NotifEventType::SENT_GROUP_MEMBERSHIP_REQUEST,
        )
        .await?;
    }

    r.success("The membership has been successfully saved!")
}

/// Cancel a group membership request
pub async fn cancel_request(r: &mut HttpRequestHandler) -> RequestResult {
    let group_id = r.post_group_id_with_access("id", GroupAccessLevel::LIMITED_ACCESS)?;

    if groups_helper::get_membership_level(&group_id, r.user_id_opt())?
        != GroupMembershipLevel::PENDING
    {
        r.forbidden("You did not send a membership request to this group!".to_string())?;
    }

    groups_helper::delete_member(&group_id, &r.user_id()?).await?;

    // Delete any related notification
    notifications_helper::delete_all_related_to_group_membership_notifications(
        r.user_id_ref()?,
        &group_id,
    )
    .await?;

    r.success("The request has been successfully cancelled!")
}

/// Remove a member from a group (as a moderator or an admin)
pub async fn delete_member(r: &mut HttpRequestHandler) -> RequestResult {
    let group_id = r.post_group_id_with_access("groupID", GroupAccessLevel::MODERATOR_ACCESS)?;

    // Get the membership of the user making the request
    let curr_user_membership = groups_helper::get_membership(&group_id, r.user_id_opt())?;

    // Get information about the member to delete
    let user_id = r.post_user_id("userID")?;
    let membership = groups_helper::get_membership(&group_id, Some(user_id.clone()))?;

    if user_id == r.user_id()? && groups_helper::is_last_admin(&group_id, &r.user_id()?)? {
        r.forbidden("You are the last administrator of this group!".to_string())?;
    }

    // Only administrator can delete members that are more than members (moderators & administrators)
    if membership.level < GroupMembershipLevel::MEMBER
        && curr_user_membership.level != GroupMembershipLevel::ADMINISTRATOR
    {
        r.forbidden("Only administrators can delete this membership!".to_string())?;
    }

    groups_helper::delete_member(&group_id, &user_id).await?;

    // Delete related notifications
    notifications_helper::delete_all_related_to_group_membership_notifications(&user_id, &group_id)
        .await?;

    r.success("Membership of the user has been successfully deleted!")
}

/// Update a user's membership
pub async fn update_membership(r: &mut HttpRequestHandler) -> RequestResult {
    let group_id = r.post_group_id_with_access("groupID", GroupAccessLevel::ADMIN_ACCESS)?;
    let user_id = r.post_user_id("userID")?;

    if user_id == r.user_id()? {
        r.bad_request("You can not update your own membership!".to_string())?;
    }

    let level = groups_helper::get_membership_level(&group_id, Some(user_id.clone()))?;

    if level > GroupMembershipLevel::MEMBER {
        r.forbidden("This user is not a member of the group!".to_string())?;
    }

    let new_level = GroupMembershipLevel::from_api(&r.post_string("level")?);

    if new_level > GroupMembershipLevel::MEMBER {
        r.forbidden("You can not assign this visibility level!".to_string())?;
    }

    groups_helper::update_membership_level(&group_id, &user_id, new_level).await?;

    r.success("User membership has been successfully updated!")
}

/// Respond to a group membership request
pub async fn respond_request(r: &mut HttpRequestHandler) -> RequestResult {
    let group_id = r.post_group_id_with_access("groupID", GroupAccessLevel::MODERATOR_ACCESS)?;
    let user_id = r.post_user_id("userID")?;
    let accept = r.post_bool("accept")?;

    if groups_helper::get_membership_level(&group_id, Some(user_id.clone()))?
        != GroupMembershipLevel::PENDING
    {
        r.forbidden("This user has not requested a membership for this group!".to_string())?;
    }

    groups_helper::respond_request(&group_id, &user_id, accept).await?;

    // Create a notification
    notifications_helper::create_group_membership_notification(
        &user_id,
        Some(r.user_id_ref()?),
        &group_id,
        match accept {
            true => NotifEventType::ACCEPTED_GROUP_MEMBERSHIP_REQUEST,
            false => NotifEventType::REJECTED_GROUP_MEMBERSHIP_REQUEST,
        },
    )
    .await?;

    r.success("The response to the request has been successfully saved!")
}

/// Get information about a single user membership over a group
pub async fn get_membership(r: &mut HttpRequestHandler) -> RequestResult {
    let group_id = r.post_group_id_with_access("groupID", GroupAccessLevel::MODERATOR_ACCESS)?;
    let user_id = r.post_user_id("userID")?;

    let membership = groups_helper::get_membership(&group_id, Some(user_id))?;

    r.set_response(GroupMemberAPI::new(&membership))
}

/// Delete the membership of a user over a group
pub async fn remove_membership(r: &mut HttpRequestHandler) -> RequestResult {
    let group_id = r.post_group_id_with_access("id", GroupAccessLevel::LIMITED_ACCESS)?;

    if groups_helper::is_last_admin(&group_id, &r.user_id()?)? {
        r.forbidden("You are the last administrator of this group!".to_string())?;
    }

    groups_helper::delete_member(&group_id, &r.user_id()?).await?;

    // Delete group membership notifications
    notifications_helper::delete_all_related_to_group_membership_notifications(
        r.user_id_ref()?,
        &group_id,
    )
    .await?;

    r.success("Your membership has been successfully deleted!")
}

/// Update following status over a group
pub async fn set_following(r: &mut HttpRequestHandler) -> RequestResult {
    let group_id = r.post_group_id_with_access("groupID", GroupAccessLevel::MEMBER_ACCESS)?;
    let following = r.post_bool("follow")?;

    groups_helper::set_following(&group_id, &r.user_id()?, following)?;

    r.success("Following status updated.")
}

/// Delete a group
pub async fn delete_group(r: &mut HttpRequestHandler) -> RequestResult {
    let group_id = r.post_group_id_with_access("groupID", GroupAccessLevel::ADMIN_ACCESS)?;
    r.need_user_password("password")?;

    groups_helper::delete(&group_id).await?;

    r.success("Group deleted.")
}
