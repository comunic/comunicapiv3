//! # Custom emoji API object
//!
//! @author Pierre Hubert
use serde::Serialize;

use crate::data::custom_emoji::CustomEmoji;
use crate::utils::user_data_utils::user_data_url;

#[derive(Serialize)]
#[allow(non_snake_case)]
pub struct CustomEmojiAPI {
    id: u64,
    userID: u64,
    shortcut: String,
    url: String,
}

impl CustomEmojiAPI {
    /// Create a new Custom Emoji API entry
    pub fn new(custom_emoji: &CustomEmoji) -> CustomEmojiAPI {
        CustomEmojiAPI {
            id: custom_emoji.id,
            userID: custom_emoji.user_id.id(),
            shortcut: custom_emoji.shortcut.to_string(),
            url: user_data_url(&custom_emoji.path),
        }
    }
}
