//! # Post API entry
//!
//! @author Pierre Hubert
use serde::Serialize;

use crate::api_data::comment_api::CommentAPI;
use crate::api_data::survey_api::SurveyAPI;
use crate::data::error::ResultBoxError;
use crate::data::post::{Post, PostKind};
use crate::data::user::UserID;
use crate::helpers::likes_helper::LikeType;
use crate::helpers::{comments_helper, likes_helper, posts_helper, survey_helper};
use crate::utils::user_data_utils::user_data_url;

#[derive(Serialize)]
#[allow(non_snake_case)]
pub struct PostAPI {
    ID: u64,
    userID: u64,
    user_page_id: u64,
    group_id: u64,
    post_time: u64,
    content: Option<String>,
    visibility_level: String,
    user_access: String,
    kind: String,

    // File specific
    file_size: Option<usize>,
    file_type: Option<String>,
    file_path: Option<String>,
    file_path_url: Option<String>,

    // Weblink specific
    link_url: Option<String>,
    link_title: Option<String>,
    link_description: Option<String>,
    link_image: Option<String>,

    // Countdown timer specific
    time_end: Option<u64>,

    // Survey specific
    data_survey: Option<SurveyAPI>,

    // Likes information
    likes: u64,
    userlike: bool,

    // Comments
    comments: Option<Vec<CommentAPI>>,
}

impl PostAPI {
    /// Turn a `Post` entry into an API entry
    pub fn new(p: &Post, user: &Option<UserID>) -> ResultBoxError<PostAPI> {
        let mut post = PostAPI {
            ID: p.id,
            userID: p.user_id.id(),
            user_page_id: p.user_page_id().unwrap_or(&UserID::invalid()).id(),
            group_id: p.group_id().map(|f| f.id()).unwrap_or(0),
            post_time: p.time_create,
            content: p.content.clone(),
            visibility_level: p.visibility.to_api(),
            user_access: posts_helper::get_access_level(&p, user)?.to_api(),
            kind: p.kind.to_api(),

            // File specific
            file_size: None,
            file_type: None,
            file_path: None,
            file_path_url: None,

            // Weblink specific
            link_url: None,
            link_title: None,
            link_description: None,
            link_image: None,

            // Countdown timer-specific
            time_end: None,

            // Survey specific
            data_survey: None,

            // Likes information
            likes: likes_helper::count(p.id, LikeType::POST)? as u64,
            userlike: user
                .as_ref()
                .map(|user_id| likes_helper::is_liking(user_id, p.id, LikeType::POST))
                .unwrap_or(Ok(false))?,

            // Comments
            comments: None,
        };

        match &p.kind {
            PostKind::POST_KIND_TEXT => { /* do nothing */ }

            PostKind::POST_KIND_IMAGE(file) | PostKind::POST_KIND_PDF(file) => {
                post.file_size = Option::from(file.size);
                post.file_type = file.file_type.clone();
                post.file_path = Some(file.path.clone());
                post.file_path_url = Some(user_data_url(file.path.as_ref()))
            }

            PostKind::POST_KIND_WEBLINK(link) => {
                post.link_url = Some(link.url.clone());
                post.link_description = link.description.clone();
                post.link_title = link.title.clone();
                post.link_image = link.image.clone();
            }

            PostKind::POST_KIND_COUNTDOWN(time_end) => post.time_end = Some(*time_end),

            PostKind::POST_KIND_SURVEY => {
                post.data_survey = Some(SurveyAPI::new(&survey_helper::get_info(p.id)?, *user)?)
            }

            PostKind::POST_KIND_YOUTUBE(id) => {
                post.file_path = Some(id.clone());
                post.file_type = Some("youtube".to_string());
            }
        }

        if posts_helper::allow_comments_on_post(p)? {
            post.comments = Some(CommentAPI::for_list(&comments_helper::get(p.id)?, user)?);
        }

        Ok(post)
    }

    /// Turn a list of posts into an API entry
    pub fn for_list(l: &[Post], user_id: Option<UserID>) -> ResultBoxError<Vec<PostAPI>> {
        l.iter().map(|p| Self::new(p, &user_id)).collect()
    }
}
