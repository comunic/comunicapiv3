//! # Count unread notifications & conversation
//!
//! Optionally includes friendship requests too
//!
//! @author Pierre Hubert
use serde::Serialize;

#[derive(Serialize)]
struct CountFriendsRequests {
    friends_requests: u64,
}

#[derive(Serialize)]
pub struct ResCountAllUnread {
    notifications: u64,
    conversations: u64,

    #[serde(flatten)]
    friends: Option<CountFriendsRequests>,
}

impl ResCountAllUnread {
    pub fn new(
        notifications: u64,
        conversations: u64,
        friends_requests: Option<u64>,
    ) -> ResCountAllUnread {
        ResCountAllUnread {
            notifications,
            conversations,
            friends: friends_requests.map(|n| CountFriendsRequests {
                friends_requests: n,
            }),
        }
    }
}
