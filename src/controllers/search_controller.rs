//! # Search controller
//!
//! @author Pierre Hubert

use crate::api_data::global_search_result_api::GlobalSearchResultAPI;
use crate::data::base_request_handler::BaseRequestHandler;
use crate::data::global_search_result::GlobalSearchResult;
use crate::data::http_request_handler::HttpRequestHandler;
use crate::helpers::{groups_helper, user_helper};
use crate::routes::RequestResult;

/// Search for user
pub async fn search_user(r: &mut HttpRequestHandler) -> RequestResult {
    let query = r.post_string_opt("query", 1, true)?;
    let limit = r.post_u64_opt("searchLimit", 5)?;

    let list = user_helper::search_user(&query, limit)?
        .iter()
        .map(|f| f.id())
        .collect::<Vec<u64>>();

    r.set_response(list)
}

/// Perform a global search
pub async fn search_global(r: &mut HttpRequestHandler) -> RequestResult {
    let query = r.post_string("query")?;
    let limit = 10;

    let mut list = user_helper::search_user(&query, limit)?
        .iter()
        .map(|f| GlobalSearchResult::User(f.clone()))
        .collect::<Vec<GlobalSearchResult>>();

    list.append(
        &mut groups_helper::search_group(&query, limit)?
            .iter()
            .map(|f| GlobalSearchResult::Group(f.clone()))
            .collect::<Vec<GlobalSearchResult>>(),
    );

    r.set_response(GlobalSearchResultAPI::for_list(&list))
}
