//! # Admin information API
//!
//! @author Pierre Hubert

use serde::Serialize;

use crate::data::admin::Admin;

#[derive(Serialize)]
pub struct AdminInfoAPI {
    id: u64,
    name: String,
    email: String,
    time_create: u64,
    roles: Vec<&'static str>,
}

impl AdminInfoAPI {
    pub fn new(a: &Admin) -> Self {
        Self {
            id: a.id.id(),
            name: a.name.clone(),
            email: a.email.clone(),
            time_create: a.time_create,
            roles: a.roles.iter().map(|r| r.to_id()).collect(),
        }
    }
}
