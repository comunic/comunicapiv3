//! # WebSocket routes
//!
//! @author Pierre Hubert

use crate::controllers::{
    calls_controller, conversations_controller, forez_controller, likes_controller, user_ws_actions,
};
use crate::data::error::Res;
use crate::data::user_ws_request_handler::UserWsRequestHandler;
use crate::routes::RequestResult;

pub type WsRequestProcess = Box<dyn Fn(&mut UserWsRequestHandler) -> Res>;

/// Search for a route
pub async fn exec_user_ws_route(
    uri: &str,
    handler: &mut UserWsRequestHandler,
) -> Option<RequestResult> {
    match uri {
        // Main controller
        "$main/set_incognito" => Some(user_ws_actions::set_incognito(handler)),
        "$main/register_conv" => Some(user_ws_actions::register_conv(handler)),
        "$main/unregister_conv" => Some(user_ws_actions::unregister_conv(handler)),
        "$main/register_post" => Some(user_ws_actions::register_post(handler)),
        "$main/unregister_post" => Some(user_ws_actions::unregister_post(handler)),

        // Likes controller
        "likes/update" => Some(likes_controller::update(handler).await),

        // Conversations controller
        "conversations/is_writing" => {
            Some(conversations_controller::member_is_writing(handler).await)
        }

        // Calls controller
        "calls/config" => Some(calls_controller::get_config(handler).await),
        "calls/join" => Some(calls_controller::join_call(handler).await),
        "calls/leave" => Some(calls_controller::leave_call(handler).await),
        "calls/members" => Some(calls_controller::get_members_list(handler).await),
        "calls/signal" => Some(calls_controller::on_client_signal(handler).await),
        "calls/mark_ready" => Some(calls_controller::mark_user_ready(handler).await),
        "calls/request_offer" => Some(calls_controller::request_offer(handler).await),
        "calls/stop_streaming" => Some(calls_controller::stop_streaming(handler).await),

        // Presence controller
        "forez_presence/list" => Some(forez_controller::get_list(handler)),
        "forez_presence/add_day" => Some(forez_controller::add_day(handler)),
        "forez_presence/del_day" => Some(forez_controller::del_day(handler)),

        _ => None,
    }
}
