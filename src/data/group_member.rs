//! # Group membership information
//!
//! @author Pierre Hubert

use crate::data::group_id::GroupID;
use crate::data::user::UserID;

#[derive(PartialEq, Eq, PartialOrd, Debug)]
pub enum GroupMembershipLevel {
    ADMINISTRATOR = 0,
    MODERATOR = 1,
    MEMBER = 2,
    INVITED = 3,

    // When the group membership has not been approved yet
    PENDING = 4,

    // Simple visit
    VISITOR = 5,
}

impl GroupMembershipLevel {
    pub fn is_at_least_member(&self) -> bool {
        matches!(
            &self,
            GroupMembershipLevel::ADMINISTRATOR
                | GroupMembershipLevel::MODERATOR
                | GroupMembershipLevel::MEMBER
        )
    }

    pub fn to_api(&self) -> String {
        match self {
            GroupMembershipLevel::ADMINISTRATOR => "administrator",
            GroupMembershipLevel::MODERATOR => "moderator",
            GroupMembershipLevel::MEMBER => "member",
            GroupMembershipLevel::INVITED => "invited",
            GroupMembershipLevel::PENDING => "pending",
            GroupMembershipLevel::VISITOR => "visitor",
        }
        .to_string()
    }

    pub fn from_api(level: &str) -> GroupMembershipLevel {
        match level {
            "administrator" => GroupMembershipLevel::ADMINISTRATOR,
            "moderator" => GroupMembershipLevel::MODERATOR,
            "member" => GroupMembershipLevel::MEMBER,
            "invited" => GroupMembershipLevel::INVITED,
            "pending" => GroupMembershipLevel::PENDING,
            "visitor" => GroupMembershipLevel::VISITOR,
            _ => GroupMembershipLevel::VISITOR,
        }
    }
}

pub struct GroupMember {
    pub id: u64,
    pub user_id: UserID,
    pub group_id: GroupID,
    pub time_create: u64,
    pub level: GroupMembershipLevel,
    pub following: bool,
}

impl GroupMember {
    pub fn is_admin(&self) -> bool {
        self.level == GroupMembershipLevel::ADMINISTRATOR
    }

    /// Check if a member of a group is a least a moderator of this group
    pub fn is_moderator(&self) -> bool {
        self.level <= GroupMembershipLevel::MODERATOR
    }

    pub fn is_at_least_member(&self) -> bool {
        self.level <= GroupMembershipLevel::MEMBER
    }
}

#[cfg(test)]
mod tests {
    use crate::data::group_member::GroupMembershipLevel;

    #[test]
    fn membership_level_coherence() {
        assert!(GroupMembershipLevel::ADMINISTRATOR < GroupMembershipLevel::MODERATOR);
        assert!(GroupMembershipLevel::MODERATOR < GroupMembershipLevel::MEMBER);
        assert!(GroupMembershipLevel::MEMBER < GroupMembershipLevel::INVITED);
        assert!(GroupMembershipLevel::INVITED < GroupMembershipLevel::PENDING);
        assert!(GroupMembershipLevel::PENDING < GroupMembershipLevel::VISITOR);
    }
}
