//! # User like API entry
//!
//! @author Pierre Hubert

use serde::Serialize;

use crate::data::user_like::UserLike;

#[derive(Serialize)]
#[allow(non_snake_case)]
pub struct UserLikeAPI {
    id: u64,
    userID: u64,
    time_sent: u64,
    elem_type: String,
    elem_id: u64,
}

impl UserLikeAPI {
    pub fn new(l: &UserLike) -> UserLikeAPI {
        UserLikeAPI {
            id: l.id,
            userID: l.user_id.id(),
            time_sent: l.time_sent,
            elem_type: l.elem_type.clone(),
            elem_id: l.elem_id,
        }
    }

    pub fn for_list(l: &[UserLike]) -> Vec<Self> {
        l.iter().map(Self::new).collect()
    }
}
